/*
 * algorithms.h
 *
 *  Created on: Sep 24, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_ALGORITHMS_H_
#define CALIBRATION_COMMON_ALGORITHMS_H_

#include <calibration_common/algorithms/automatic_checkerboard_finder.h>
#include <calibration_common/algorithms/checkerboard_finder.h>
#include <calibration_common/algorithms/plane_to_plane_calibration.h>
#include <calibration_common/algorithms/point_on_plane_calibration.h>

#endif /* CALIBRATION_COMMON_ALGORITHMS_H_ */
