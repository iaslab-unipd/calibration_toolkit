/*
 * automatic_checkerboard_finder.h
 *
 *  Created on: Sep 25, 2012
 *      Author: Filippo Basso
 */

#ifndef AUTOMATIC_CHECKERBOARD_FINDER_H_
#define AUTOMATIC_CHECKERBOARD_FINDER_H_

#include <calibration_common/algorithms/checkerboard_finder.h>

namespace calibration
{

/**
 *
 */
class AutomaticCheckerboardFinder : public CheckerboardFinder
{
public:

  /* methods */

  /**
   * @brief AutomaticCheckerboardFinder
   */
  AutomaticCheckerboardFinder();

  /**
   * @brief AutomaticCheckerboardFinder
   * @param image
   */
  AutomaticCheckerboardFinder(const cv::Mat & image);

  /**
   * @brief ~AutomaticCheckerboardFinder
   */
  virtual
  ~AutomaticCheckerboardFinder();

  using CheckerboardFinder::find;

  /**
   * @brief find
   * @param checkerboard
   * @param corners
   * @return
   */
  virtual bool
  find(const Checkerboard & checkerboard,
       std::vector<cv::Point2d> & corners) const;

};

} /* namespace calibration */
#endif /* AUTOMATIC_CHECKERBOARD_FINDER_H_ */
