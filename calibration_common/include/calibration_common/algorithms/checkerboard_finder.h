/*
 * checkerboard_finder.h
 *
 *  Created on: Jun 11, 2013
 *      Author: Filippo Basso
 */

#ifndef CHECKERBOARD_FINDER_H_
#define CHECKERBOARD_FINDER_H_

#include <opencv2/opencv.hpp>
#include <calibration_common/base/checkerboard.h>

using calibration::Checkerboard;
using calibration::PointSet2d;

namespace calibration
{

/**
 *
 */
class CheckerboardFinder
{
public:

  /* typedefs */

  // Smart pointers
  typedef boost::shared_ptr<CheckerboardFinder> Ptr;
  typedef boost::shared_ptr<const CheckerboardFinder> ConstPtr;

  /* methods */

  CheckerboardFinder();

  /**
   * @brief CheckerboardFinder
   * @param image
   */
  CheckerboardFinder(const cv::Mat &image);

  /**
   * @brief ~CheckerboardFinder
   */
  virtual
  ~CheckerboardFinder();

  /**
   * @brief setImage
   * @param image
   */
  void
  setImage(const cv::Mat & image);

  /**
   * @brief find
   * @param checkerboard
   * @param corners
   * @return
   */
  virtual bool
  find(const Checkerboard & checkerboard,
       std::vector<cv::Point2d> & corners) const = 0;

  /**
   * @brief find
   * @param checkerboard
   * @param corners
   * @return
   */
  virtual bool
  find(const Checkerboard & checkerboard,
       PointSet2d & corners) const;

protected:

  /* variables */

  cv::Mat image_;
  cv::Mat gray_;

};

inline CheckerboardFinder::CheckerboardFinder()
{
  // Do nothing
}

inline CheckerboardFinder::CheckerboardFinder(const cv::Mat &image)
{
  setImage(image);
}

inline CheckerboardFinder::~CheckerboardFinder()
{
  // Do nothing
}

inline void CheckerboardFinder::setImage(const cv::Mat & image)
{
  image_ = image;
  cv::cvtColor(image_, gray_, CV_BGR2GRAY);
}

inline bool CheckerboardFinder::find(const Checkerboard & checkerboard,
                                     PointSet2d & corners) const
{
  std::vector<cv::Point2d> cv_corners;
  bool pattern_found = find(checkerboard, cv_corners);
  corners = PointSet2d(cv_corners);
  return pattern_found;
}

} /* namespace calibration */
#endif /* CHECKERBOARD_FINDER_H_ */
