/*
 * plane_to_plane_calibration.h
 *
 *  Created on: Apr 2, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_PLANE_TO_PLANE_CALIBRATION_H_
#define CALIBRATION_COMMON_PLANE_TO_PLANE_CALIBRATION_H_

#include <calibration_common/base/typedefs.h>

#include <vector>

namespace calibration
{

struct PlanePair
{
  PlanePair()
  {
  }

  PlanePair(Plane3d plane_1,
            Plane3d plane_2)
    : plane_1_(plane_1), plane_2_(plane_2)
  {
  }

  Plane3d plane_1_;
  Plane3d plane_2_;
};

class PlaneToPlaneCalibration
{
public:

  PlaneToPlaneCalibration();

  virtual
  ~PlaneToPlaneCalibration();

  void
  addPair(const PlanePair & pair);

  void
  addPair(const Plane3d & plane_1,
          const Plane3d & plane_2);

  int
  getPairNumber();

  Transform3d
  estimateTransform();

  static Transform3d
  estimateTransform(const std::vector<PlanePair> & plane_pair_vec);

protected:

  std::vector<PlanePair> plane_pair_vec_;

};

inline void PlaneToPlaneCalibration::addPair(const PlanePair & pair)
{
  plane_pair_vec_.push_back(pair);
}

inline void PlaneToPlaneCalibration::addPair(const Plane3d & plane_1,
                                             const Plane3d & plane_2)
{
  addPair(PlanePair(plane_1, plane_2));
}

inline int PlaneToPlaneCalibration::getPairNumber()
{
  return plane_pair_vec_.size();
}

inline Transform3d PlaneToPlaneCalibration::estimateTransform()
{
  return estimateTransform(plane_pair_vec_);
}

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_PLANE_TO_PLANE_CALIBRATION_H_ */
