/*
 * plane_to_plane_calibration.h
 *
 *  Created on: Apr 2, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_POINT_ON_PLANE_CALIBRATION_H_
#define CALIBRATION_COMMON_POINT_ON_PLANE_CALIBRATION_H_

#include <calibration_common/base/typedefs.h>

#include <vector>

namespace calibration
{

struct PointPlanePair
{
  PointPlanePair()
  {
  }

  PointPlanePair(const Point3d & point,
                 const Plane3d & plane)
    : point_(point), plane_(plane)
  {
  }

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  Point3d point_;
  Plane3d plane_;
};

class PointOnPlaneCalibration
{
public:

  PointOnPlaneCalibration();

  virtual
  ~PointOnPlaneCalibration();

  void
  addPair(const PointPlanePair & pair);

  void
  addPair(const Point3d & point,
          const Plane3d & plane);

  int
  getPairNumber();

  Transform3d
  estimateTransform();

  static Transform3d
  estimateTransform(const std::vector<PointPlanePair> & pair_vec);

protected:

  std::vector<PointPlanePair> pair_vec_;

};

inline void PointOnPlaneCalibration::addPair(const PointPlanePair & pair)
{
  pair_vec_.push_back(pair);
}

inline void PointOnPlaneCalibration::addPair(const Point3d & point,
                                             const Plane3d & plane)
{
  addPair(PointPlanePair(point, plane));
}

inline int PointOnPlaneCalibration::getPairNumber()
{
  return pair_vec_.size();
}

inline Transform3d PointOnPlaneCalibration::estimateTransform()
{
  return estimateTransform(pair_vec_);
}

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_POINT_ON_PLANE_CALIBRATION_H_ */
