/*
 * base.h
 *
 *  Created on: Sep 24, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_BASE_H_
#define CALIBRATION_COMMON_BASE_H_

#include <calibration_common/base/typedefs.h>
#include <calibration_common/base/math.h>
#include <calibration_common/base/point_set.h>

#include <calibration_common/base/polynomial_multifit.h>

#include <calibration_common/base/base_object.h>
#include <calibration_common/base/planar_object.h>
#include <calibration_common/base/pattern.h>
#include <calibration_common/base/checkerboard.h>

#include <calibration_common/base/pose_estimator.h>
#include <calibration_common/base/sensor.h>

#endif /* CALIBRATION_COMMON_BASE_H_ */
