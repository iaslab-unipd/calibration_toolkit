/*
 * base_object.h
 *
 *  Created on: Feb 1, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_BASE_OBJECT_H_
#define CALIBRATION_COMMON_BASE_OBJECT_H_

#include <calibration_common/base/typedefs.h>

#include <geometry_msgs/TransformStamped.h>

namespace calibration
{

/* ****************** Class definition ****************** */

class BaseObject
{
public:

  /* typedefs */

  // Smart pointers
  typedef boost::shared_ptr<BaseObject> Ptr;
  typedef boost::shared_ptr<const BaseObject> ConstPtr;

  /* methods */

  /**
   * @brief BaseObject
   * @param frame_id
   * @param parent
   */
  BaseObject(const std::string & frame_id = "",
             const ConstPtr & parent = ConstPtr());


  /**
   * @brief BaseObject
   * @param other
   */
  BaseObject(const BaseObject & other);

  /**
   * @brief BaseObject
   * @param other
   * @param transform
   * @param new_frame_id
   * @param new_parent
   */
  BaseObject(const BaseObject & other,
             const Transform3d & transform,
             const std::string & new_frame_id = "",
             const ConstPtr & new_parent = ConstPtr());

  /**
   * @brief ~BaseObject
   */
  virtual
  ~BaseObject();

  /**
   * @brief transform
   * @param transform
   */
  virtual void
  transform(const Transform3d & transform);

  /**
   * @brief pose
   * @return
   */
  const Pose3d &
  pose() const;

  /**
   * @brief getFrameId
   * @return
   */
  const std::string &
  frameId() const;

  /**
   * @brief parent
   * @return
   */
  const ConstPtr &
  parent() const;

  /**
   * @brief toTF
   * @param transform_msg
   * @return
   */
  bool
  toTF(geometry_msgs::TransformStamped & transform_msg) const;

protected:

  Pose3d pose_;

  std::string frame_id_;
  ConstPtr parent_;

};

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_BASE_OBJECT_H_ */
