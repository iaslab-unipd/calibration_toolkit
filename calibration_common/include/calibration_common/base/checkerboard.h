/*
 * checkerboard.h
 *
 *  Created on: Sep 13, 2012
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_CHECKERBOARD_H_
#define CALIBRATION_COMMON_CHECKERBOARD_H_

#include <calibration_common/base/planar_object.h>
#include <calibration_common/base/pattern.h>
#include <calibration_common/color/view.h>

#include <visualization_msgs/Marker.h>

namespace calibration
{

/* ****************** Class definition ****************** */

class Checkerboard : public PlanarObject, public Pattern
{
public:

  /* typedefs */

  // Smart pointers
  typedef boost::shared_ptr<Checkerboard> Ptr;
  typedef boost::shared_ptr<const Checkerboard> ConstPtr;

  /* methods */

  /**
   *
   * @param rows
   * @param cols
   * @param cell_width
   * @param cell_height
   * @param corners
   * @param transform
   * @param frame_id
   * @param parent
   */
  Checkerboard(int rows,
               int cols,
               double cell_width,
               double cell_height,
               const std::string & frame_id = "",
               const BaseObject::ConstPtr & parent = BaseObject::ConstPtr());

  /**
   *
   * @param other
   */
  Checkerboard(const Checkerboard & other);

  /**
   *
   * @param other
   * @param transform
   * @param new_frame_id
   * @param new_parent
   */
  Checkerboard(const Checkerboard & other,
               const Transform3d & transform,
               const std::string & new_frame_id = "",
               const BaseObject::ConstPtr & new_parent = BaseObject::ConstPtr());

  /**
   * @brief Checkerboard
   * @param view
   */
  Checkerboard(const ColorView<Checkerboard> & view);

  /**
   * @brief ~Checkerboard
   */
  virtual
  ~Checkerboard();

  virtual void
  transform(const Transform3d & transform);

  /**
   * @brief operator ()
   * @param row
   * @param col
   * @return
   */
  const PointSet3d::ConstElement
  operator ()(int row,
              int col) const;

  /**
   * @brief at
   * @param row
   * @param col
   * @return
   */
  const PointSet3d::ConstElement
  at(int row,
     int col) const;

  /**
   * @brief center
   * @return
   */
  Point3d
  center() const;

  /**
   * @brief rows
   * @return
   */
  int
  rows() const;

  /**
   * @brief cols
   * @return
   */
  int
  cols() const;

  /**
   * @brief cellWidth
   * @return
   */
  double
  cellWidth() const;

  /**
   * @brief cellHeight
   * @return
   */
  double
  cellHeight() const;

  /**
   * @brief corners
   * @return
   */
  const PointSet3d &
  corners() const;

  /**
   * @brief area
   * @return
   */
  double
  area() const;

  /**
   * @brief width
   * @return
   */
  virtual double
  width() const;

  /**
   * @brief height
   * @return
   */
  virtual double
  height() const;

  /**
   * @brief toMarker
   * @param marker
   */
  virtual void
  toMarker(visualization_msgs::Marker & marker) const;

  static PointSet3d corners(int rows,
                            int cols,
                            double cell_width,
                            double cell_height);

private:

  /* variables */

  int rows_;
  int cols_;

  double cell_width_;
  double cell_height_;

};

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_CHECKERBOARD_H_ */
