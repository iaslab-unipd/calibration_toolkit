/*
 * math.h
 *
 *  Created on: Sep 22, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_MATH_H_
#define CALIBRATION_COMMON_MATH_H_

#include <boost/smart_ptr/shared_ptr.hpp>
#include <Eigen/Dense>

namespace calibration
{

template <typename MathFunctionT>
  struct MathFunctionTraits
  {

  };

template <typename Derived>
  class MathFunction
  {
  public:

    typedef boost::shared_ptr<MathFunction> Ptr;
    typedef boost::shared_ptr<const MathFunction> ConstPtr;

    typedef typename MathFunctionTraits<Derived>::Scalar Scalar;

    virtual Scalar
    operator ()(const Scalar & x) const;

    virtual Scalar
    evaluate(const Scalar & x) const = 0;

  };

template <typename Derived>
  class MathFunctionMap
  {
  public:

    typedef boost::shared_ptr<MathFunctionMap> Ptr;
    typedef boost::shared_ptr<const MathFunctionMap> ConstPtr;

    typedef Eigen::DenseIndex Index;

    virtual const Derived &
    operator ()(const Index & row,
                const Index & col) const = 0;

    virtual Derived &
    operator ()(const Index & row,
                const Index & col) = 0;

    virtual const Derived &
    at(const Index & row,
       const Index & col) const
    {
      assert(row >= 0 and row < rows_ and col >= 0 and col < cols_);
      return operator ()(row, col);
    }

    virtual Derived &
    at(const Index & row,
       const Index & col)
    {
      assert(row >= 0 and row < rows_ and col >= 0 and col < cols_);
      return operator ()(row, col);
    }

  protected:

    Index rows_;
    Index cols_;

  };

template <typename Derived>
  class MathFunctionWithCoefficients : public MathFunction<Derived>
  {
  public:

    typedef boost::shared_ptr<MathFunctionWithCoefficients> Ptr;
    typedef boost::shared_ptr<const MathFunctionWithCoefficients> ConstPtr;

    typedef typename MathFunctionTraits<Derived>::Scalar Scalar;
    enum
    {
      SIZE = MathFunctionTraits<Derived>::SIZE
    };

    typedef Eigen::Matrix<Scalar, SIZE, 1> Coefficients;

    MathFunctionWithCoefficients(const Coefficients & coefficients);

    virtual
    ~MathFunctionWithCoefficients();

    virtual Coefficients &
    coefficients();

    virtual const Coefficients &
    coefficients() const;

    virtual int
    size() const;

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  protected:

    Coefficients coefficients_;

  };

template <typename Derived>
  class MathFunctionWithCoefficientsMap : public MathFunctionMap<Derived>
  {
  public:

    typedef boost::shared_ptr<MathFunctionWithCoefficientsMap> Ptr;
    typedef boost::shared_ptr<const MathFunctionWithCoefficientsMap> ConstPtr;

    typedef typename MathFunctionMap<Derived>::Index Index;

    typedef typename MathFunctionTraits<Derived>::Scalar Scalar;
    enum
    {
      SIZE = MathFunctionTraits<Derived>::SIZE
    };

    typedef Eigen::Matrix<Scalar, SIZE, Eigen::Dynamic> Map;
    typedef typename Map::ColXpr Element;
    typedef typename Map::ConstColXpr ConstElement;

    template <typename OtherDerived>
    MathFunctionWithCoefficientsMap(const Index & rows,
                                    const Index & cols,
                                    const Eigen::MatrixBase<OtherDerived> & value);

    virtual
    ~MathFunctionWithCoefficientsMap();

    virtual const Derived &
    operator ()(const Index & row,
                const Index & col) const = 0;

    virtual Derived &
    operator ()(const Index & row,
                const Index & col) = 0;

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  protected:

    Map map_;

  };

template <typename ScalarT, int Degree, int MinDegree = 0>
  class PolynomialFunction;

template <typename ScalarT, int Degree, int MinDegree>
  struct MathFunctionTraits<PolynomialFunction<ScalarT, Degree, MinDegree> >
  {
    typedef ScalarT Scalar;
    enum
    {
      SIZE = Degree + 1 - MinDegree,
      MIN_DEGREE = MinDegree,
      DEGREE = Degree
    };
  };

template <typename ScalarT, int Degree, int MinDegree>
  class PolynomialFunction : public MathFunctionWithCoefficients<PolynomialFunction<ScalarT, Degree, MinDegree> >
  {
  public:

    typedef boost::shared_ptr<PolynomialFunction> Ptr;
    typedef boost::shared_ptr<const PolynomialFunction> ConstPtr;

    typedef MathFunctionWithCoefficients<PolynomialFunction> Base;
    typedef MathFunctionTraits<PolynomialFunction> Info;

    typedef typename Base::Coefficients Polynomial;
    typedef typename Info::Scalar Scalar;

    typedef Eigen::DenseIndex DegreeIndex;

    PolynomialFunction();

    template <typename OtherDerived>
      PolynomialFunction(const Eigen::MatrixBase<OtherDerived> & other);

    Scalar
    evaluate(const Scalar & x) const;

    const Scalar &
    coefficient(const DegreeIndex & degree) const;

    Scalar &
    coefficient(const DegreeIndex & degree);

    static const Polynomial
    Identity();

  };

} /* namespace calibration */

#include <impl/calibration_common/base/math.hpp>

#endif /* CALIBRATION_COMMON_MATH_H_ */
