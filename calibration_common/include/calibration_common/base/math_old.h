/*
 * math.h
 *
 *  Created on: Sep 22, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_MATH_H_
#define CALIBRATION_COMMON_MATH_H_

#include <boost/smart_ptr/shared_ptr.hpp>
#include <Eigen/Dense>

namespace calibration
{

template <typename ScalarT>
  class MathFunction
  {
  public:

    typedef boost::shared_ptr<MathFunction> Ptr;
    typedef boost::shared_ptr<const MathFunction> ConstPtr;

    virtual ScalarT
    operator ()(const ScalarT & x) const;

    virtual ScalarT
    evaluate(const ScalarT & x) const = 0;

  };

//template <typename ScalarT>
//  class MathFunctionMap
//  {
//  public:
//
//    typedef boost::shared_ptr<MathFunctionMap> Ptr;
//    typedef boost::shared_ptr<const MathFunctionMap> ConstPtr;
//
//    virtual const MathFunction<ScalarT> &
//    operator ()(const Eigen::DenseIndex & row,
//                const Eigen::DenseIndex & col) const = 0;
//
//    virtual MathFunction<ScalarT> &
//    operator ()(const Eigen::DenseIndex & row,
//                const Eigen::DenseIndex & col) = 0;
//
//    virtual const MathFunction<ScalarT> &
//    at(const Eigen::DenseIndex & row,
//       const Eigen::DenseIndex & col) const
//    {
//      assert(row >= 0 and row < rows_ and col >= 0 and col < cols_);
//      return operator ()(row, col);
//    }
//
//    virtual MathFunction<ScalarT> &
//    at(const Eigen::DenseIndex & row,
//       const Eigen::DenseIndex & col)
//    {
//      assert(row >= 0 and row < rows_ and col >= 0 and col < cols_);
//      return operator ()(row, col);
//    }
//
//  protected:
//
//    Eigen::DenseIndex rows_;
//    Eigen::DenseIndex cols_;
//
//  };

template <typename ScalarT, int Size>
  class MathFunctionWithCoefficients : public MathFunction<ScalarT>
  {
  public:

    enum Info
    {
      SIZE = Size
    };

    typedef boost::shared_ptr<MathFunctionWithCoefficients> Ptr;
    typedef boost::shared_ptr<const MathFunctionWithCoefficients> ConstPtr;

    typedef Eigen::Matrix<ScalarT, Size, 1> CoefficientsT;

    MathFunctionWithCoefficients(const CoefficientsT & coefficients);

    virtual
    ~MathFunctionWithCoefficients();

    virtual CoefficientsT &
    coefficients();

    virtual const CoefficientsT &
    coefficients() const;

    virtual int
    size() const;

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  protected:

    CoefficientsT coefficients_;

  };

template <typename ScalarT, int Degree, int MinDegree = 0>
  class PolynomialFunction : public MathFunctionWithCoefficients<ScalarT, Degree + 1 - MinDegree>
  {
  public:

    typedef boost::shared_ptr<PolynomialFunction> Ptr;
    typedef boost::shared_ptr<const PolynomialFunction> ConstPtr;

    typedef MathFunctionWithCoefficients<ScalarT, Degree + 1 - MinDegree> Base;
    typedef typename Base::CoefficientsT PolynomialT;

    enum Info
    {
      SIZE = Degree + 1 - MinDegree,
      MIN_DEGREE = MinDegree,
      DEGREE = Degree
    };

    PolynomialFunction();

    template <typename OtherDerived>
      PolynomialFunction(const Eigen::MatrixBase<OtherDerived> & other);

    ScalarT
    evaluate(const ScalarT & x) const;

    const ScalarT &
    coefficient(Eigen::DenseIndex degree) const;

    ScalarT &
    coefficient(Eigen::DenseIndex degree);

    static const PolynomialT
    IdentityPolynomial();

  };

} /* namespace calibration */

#include <impl/calibration_common/base/math_old.hpp>

#endif /* CALIBRATION_COMMON_MATH_H_ */
