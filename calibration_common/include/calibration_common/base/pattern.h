/*
 * pattern.h
 *
 *  Created on: Sep 3, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_PATTERN_H_
#define CALIBRATION_COMMON_PATTERN_H_

#include <calibration_common/base/point_set.h>

namespace calibration
{

class Pattern
{
public:

  // Smart pointers
  typedef boost::shared_ptr<Pattern> Ptr;
  typedef boost::shared_ptr<const Pattern> ConstPtr;

  /**
   * @brief Pattern
   * @param points
   */
  Pattern(const PointSet3d & points);

  /**
   * @brief Pattern
   * @param other
   */
  Pattern(const Pattern & other);

  /**
   * @brief Pattern
   * @param other
   * @param transform
   */
  Pattern(const Pattern & other,
          const Transform3d & transform);

  /**
   * @brief ~Pattern
   */
  virtual
  ~Pattern();

  /**
   * @brief size
   * @return
   */
  int
  size() const;

  /**
   * @brief operator []
   * @param index
   * @return
   */
  const PointSet3d::ConstElement
  operator [](int index) const;

  /**
   * @brief points
   * @return
   */
  const PointSet3d &
  points() const;

  /**
   * @brief transform
   * @param transform
   */
  virtual void
  transform(const Transform3d & transform);

protected:

  PointSet3d points_;

};

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_PATTERN_H_ */
