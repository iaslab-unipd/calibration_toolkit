/*
 * planar_object.h
 *
 *  Created on: Dec 10, 2012
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_PLANAR_OBJECT_H_
#define CALIBRATION_COMMON_PLANAR_OBJECT_H_

#include <calibration_common/base/base_object.h>
#include <calibration_common/depth/view.h>
#include <visualization_msgs/Marker.h>

namespace calibration
{

/* ****************** Class definition ****************** */

/**
 *
 */
class PlanarObject : public BaseObject
{
public:

  /* typedefs */

  // Smart pointers
  typedef boost::shared_ptr<PlanarObject> Ptr;
  typedef boost::shared_ptr<const PlanarObject> ConstPtr;

  /* methods */

  /**
   * @brief PlanarObject
   * @param plane
   * @param frame_id
   * @param parent
   */
  PlanarObject(const Plane3d & plane,
               const std::string & frame_id = "",
               const BaseObject::ConstPtr & parent = ConstPtr());

  /**
   * @brief PlanarObject
   * @param other
   */
  PlanarObject(const PlanarObject & other);

  /**
   * @brief PlanarObject
   * @param other
   * @param transform
   * @param new_frame_id
   * @param new_parent
   */
  PlanarObject(const PlanarObject & other,
               const Transform3d & transform,
               const std::string & new_frame_id = "",
               const BaseObject::ConstPtr & new_parent = ConstPtr());

  /**
   * @brief PlanarObject
   * @param inliers
   * @param frame_id
   * @param parent
   */
  PlanarObject(const PointSet3d & inliers,
               const std::string & frame_id = "",
               const BaseObject::ConstPtr & parent = ConstPtr());

  /**
   * @brief PlanarObject
   * @param view
   */
  template <typename PointT>
    PlanarObject(const DepthView<PlanarObject, PointT> & view);

  /**
   * Default destructor
   */
  virtual
  ~PlanarObject();

  /**
   * @brief transform
   * @param transform
   */
  virtual void
  transform(const Transform3d & transform);

  /**
   * @brief fitPlane
   * @param points
   * @return
   */
  static Plane3d
  fitPlane(const PointSet3d & points);

  /**
   * @brief plane
   * @return
   */
  const Plane3d &
  plane() const;

  /**
   * @brief toMarker
   * @param marker
   */
  void
  toMarker(visualization_msgs::Marker & marker) const;

protected:

  static void
  computeMeanAndCovarianceMatrix(const PointSet3d & points,
                                 Eigen::Matrix3d & covariance_matrix,
                                 Point3d & centroid);

  Plane3d plane_;

};

//template <typename Derived>
//PlanarObject::PlanarObject(const ColorView<Derived> & view)
//  : BaseObject(view), plane_(view.object()->plane())
//{
//  plane_.transform(pose());
//}

template <typename PointT>
  PlanarObject::PlanarObject(const DepthView<PlanarObject, PointT> & view)
    : BaseObject("", view.sensor())
  {
    std::stringstream ss;
    ss << view.object()->frameId() << "_" << view.id();
    BaseObject::frame_id_ = ss.str();

    plane_ = fitPlane(view.interestPoints());
    BaseObject::transform(Util::plane3dTransform(view.object()->plane(), plane_));
  }

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_PLANAR_OBJECT_H_ */
