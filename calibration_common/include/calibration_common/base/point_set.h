/*
 * point_set.h
 *
 *  Created on: Mar 27, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_POINT_SET_H_
#define CALIBRATION_COMMON_POINT_SET_H_

#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/smart_ptr/make_shared.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <opencv2/core/core.hpp>

#include <visualization_msgs/Marker.h>

#include <calibration_common/base/typedefs.h>

namespace calibration
{

template <typename ScalarT, int Dimension>
  class PointSet
  {
  public:

    typedef Eigen::Matrix<ScalarT, Dimension, 1, (Dimension > 1) ? Eigen::ColMajor : Eigen::RowMajor, Dimension> PointT;
    typedef Eigen::Matrix<ScalarT, Dimension, Eigen::Dynamic, (Dimension > 1) ? Eigen::ColMajor : Eigen::RowMajor,
      Dimension> MatrixT;

    typedef boost::shared_ptr<PointSet> Ptr;
    typedef boost::shared_ptr<const PointSet> ConstPtr;

    typedef typename MatrixT::ColXpr Element;
    typedef typename MatrixT::ConstColXpr ConstElement;

    PointSet();

    PointSet(const PointSet<ScalarT, Dimension> & other);

    PointSet(const MatrixT & points);

    PointSet(const PointSet & other,
             const std::vector<int> & indices);

    PointSet(size_t size,
             const PointT & value = PointT::Zero());

    PointSet(size_t width_or_cols, // TODO swap height and width (cols and rows???)
             size_t height_or_rows,
             const PointT & value = PointT::Zero());

    virtual
    ~PointSet();

    PointSet &
    operator +=(const PointSet & rhs);

    const PointSet
    operator +(const PointSet & rhs);

    const size_t
    width() const;

    const size_t
    height() const;

    const size_t
    rows() const;

    const size_t
    cols() const;

    const size_t
    size() const;

    const ConstElement
    at(int row,
       int column) const;

    Element
    at(int row,
       int column);

    const ConstElement
    operator ()(size_t row,
                size_t column) const;

    Element
    operator ()(size_t row,
                size_t column);

    bool
    isOrganized() const;

    Element
    operator [](size_t index);

    const ConstElement
    operator [](size_t index) const;

    const MatrixT &
    matrix() const;

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  protected:

    size_t width_;  // = cols_
    size_t height_; // = rows_

    MatrixT points_;

  };

template <typename ScalarT>
  class PointSet2 : public PointSet<ScalarT, 2>
  {
  public:

    typedef boost::shared_ptr<PointSet2> Ptr;
    typedef boost::shared_ptr<const PointSet2> ConstPtr;

    typedef PointSet<ScalarT, 2> Base;

    PointSet2();

    PointSet2(const PointSet2<ScalarT> & other);

    PointSet2(const typename Base::MatrixT & points);

    PointSet2(const PointSet2 & other,
              const std::vector<int> & indices);

    PointSet2(size_t size,
              const typename Base::PointT & value = Base::PointT::Zero());

    PointSet2(size_t width_or_cols, // TODO swap height and width (cols and rows???)
              size_t height_or_rows,
              const typename Base::PointT & value = Base::PointT::Zero());

    PointSet2(const std::vector<cv::Point_<ScalarT> > & cv_points);

    typedef pcl::PointCloud<pcl::PointXY> PointCloudXY;
    typedef typename PointCloudXY::Ptr PointCloudXYPtr;

    static const PointCloudXYPtr
    toPointCloud(const PointSet2<ScalarT> & point_set);

    const PointCloudXYPtr
    toPointCloud();

  };

template <typename ScalarT>
  class PointSet3 : public PointSet<ScalarT, 3>
  {
  public:

    typedef boost::shared_ptr<PointSet3> Ptr;
    typedef boost::shared_ptr<const PointSet3> ConstPtr;

    typedef PointSet<ScalarT, 3> Base;

    PointSet3();

    PointSet3(const PointSet3<ScalarT> & other);

    PointSet3(const typename Base::MatrixT & points);

    PointSet3(const PointSet3 & other,
              const std::vector<int> & indices);

    PointSet3(size_t size,
              const typename Base::PointT & value = Base::PointT::Zero());

    PointSet3(size_t width_or_cols, // TODO swap height and width (cols and rows???)
              size_t height_or_rows,
              const typename Base::PointT & value = Base::PointT::Zero());

    PointSet3(const std::vector<cv::Point3_<ScalarT> > & cv_points);

    template <typename PointT>
      PointSet3(const pcl::PointCloud<PointT> & cloud);

    template <typename PointT>
      PointSet3(const pcl::PointCloud<PointT> & cloud,
                const std::vector<int> & indices,
                const Transform3d & transform = Transform3d::Identity());

    void
    transform(const Transform3d & transform);

    typedef pcl::PointCloud<pcl::PointXYZ> PointCloudXYZ;
    typedef typename PointCloudXYZ::Ptr PointCloudXYZPtr;

    static const PointCloudXYZPtr
    toPointCloud(const PointSet3<ScalarT> & point_set);

    const PointCloudXYZPtr
    toPointCloud();

    void
    toMarker(visualization_msgs::Marker & marker) const;

  };

typedef PointSet2<float> PointSet2f;
typedef PointSet3<float> PointSet3f;

typedef PointSet2<double> PointSet2d;
typedef PointSet3<double> PointSet3d;

} /* namespace calibration */

#include <impl/calibration_common/base/point_set.hpp>

#endif /* CALIBRATION_COMMON_POINT_SET_H_ */
