/*
 * polynomial_multifit.h
 *
 *  Created on: Sep 8, 2013
 *      Author: Mauro Antonello
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_POLYNOMIAL_MULTIFIT_H_
#define CALIBRATION_COMMON_POLYNOMIAL_MULTIFIT_H_

#include <gsl/gsl_multifit.h>
#include <calibration_common/base/typedefs.h>

namespace calibration
{

class MathFunctionMultifit
{
public:

  typedef boost::shared_ptr<MathFunctionMultifit> Ptr;
  typedef boost::shared_ptr<const MathFunctionMultifit> ConstPtr;

  virtual void
  addData(const double & x,
          const double & y) = 0;

  virtual bool
  update() = 0;

};

template <typename PolynomialFunctionT>
  class PolynomialMultifit : public MathFunctionMultifit
  {
  public:

    typedef boost::shared_ptr<PolynomialMultifit> Ptr;
    typedef boost::shared_ptr<const PolynomialMultifit> ConstPtr;

    typedef typename PolynomialFunctionT::Ptr PolynomialFunctionPtr;

  protected:

    typedef MathFunctionTraits<PolynomialFunctionT> Info;

    enum
    {
      SIZE = Info::SIZE,
      MIN_DEGREE = Info::MIN_DEGREE
    };

    typedef std::vector<std::pair<double, double> > DataBin;

  public:

    PolynomialMultifit(const PolynomialFunctionPtr & polynomial_function);

    virtual
    ~PolynomialMultifit();

    virtual void
    addData(const double & x,
            const double & y);

    virtual bool
    update();

  protected:

    DataBin data_bin_;
    PolynomialFunctionPtr polynomial_function_;

  };

} /* namespace calibration */

#include <impl/calibration_common/base/polynomial_multifit.hpp>

#endif /* POLYNOMIAL_MULTIFIT_H_ */
