/*
 * pose_estimator.h
 *
 *  Created on: Dec 17, 2012
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_POSE_ESTIMATOR_H_
#define CALIBRATION_COMMON_POSE_ESTIMATOR_H_

#include <calibration_common/base/point_set.h>

namespace calibration
{

/* ****************** Class definition ****************** */

class PoseEstimator
{
public:

  /* typedefs */

  // Smart pointers
  typedef boost::shared_ptr<PoseEstimator> Ptr;
  typedef boost::shared_ptr<const PoseEstimator> ConstPtr;

  /* methods */

  /**
   * @brief ~PoseEstimator
   */
  virtual
  ~PoseEstimator();

  /**
   * @brief estimatePose
   * @param points_image
   * @param points_object
   * @return
   */
  virtual Pose3d
  estimatePose(const PointSet2d & points_image,
               const PointSet3d & points_object) const = 0;

};

/* */

inline
PoseEstimator::~PoseEstimator()
{
  // Do nothing
}

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_POSE_ESTIMATOR_H_ */
