/*
 * sensor.h
 *
 *  Created on: Jun 14, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_SENSOR_H_
#define CALIBRATION_COMMON_SENSOR_H_

#include <calibration_common/base/base_object.h>

namespace calibration
{

class Sensor : public BaseObject
{
public:

  // Smart pointers
  typedef boost::shared_ptr<Sensor> Ptr;
  typedef boost::shared_ptr<const Sensor> ConstPtr;

  /* methods */

  /**
   * @brief Sensor
   * @param frame_id
   * @param parent
   */
  Sensor(const std::string & frame_id,
         const BaseObject::ConstPtr & parent = BaseObject::ConstPtr());

  /**
   * @brief Sensor
   * @param other
   */
  Sensor(const Sensor & other);

  /**
   * @brief Sensor
   * @param other
   * @param transform
   * @param new_frame_id
   * @param new_parent
   */
  Sensor(const Sensor & other,
         const Transform3d & transform,
         const std::string & new_frame_id,
         const BaseObject::ConstPtr & new_parent = BaseObject::ConstPtr());

  /**
   * @brief ~Sensor
   */
  virtual
  ~Sensor();

  /**
   * @brief setPose
   * @param pose
   */
  virtual void
  setPose(const Pose3d & pose);

};

}
/* namespace calibration */
#endif /* CALIBRATION_COMMON_SENSOR_H_ */
