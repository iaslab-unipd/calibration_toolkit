/*
 * calibration_typedefs.h
 *
 *  Created on: Mar 27, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_TYPEDEFS_H_
#define CALIBRATION_COMMON_TYPEDEFS_H_

#include <boost/smart_ptr/shared_ptr.hpp>
#include <Eigen/Geometry>
#include <unsupported/Eigen/Polynomials>

namespace calibration
{

typedef Eigen::Vector2d Point2d;
typedef Eigen::Vector3d Point3d;

typedef Eigen::Hyperplane<double, 3> Plane3d;
typedef Eigen::ParametrizedLine<double, 3> Line3d;

typedef Eigen::Affine3d Transform3d;
typedef Eigen::Affine3d Pose3d;

/* ******************************************************************* */

const Plane3d PLANE_XY = Plane3d(Eigen::Vector3d::UnitZ(), 0);

class Util
{
public:

  inline static const Transform3d plane3dTransform(const Plane3d & from,
                                                   const Plane3d & to)
  {
    Eigen::Quaterniond rotation;
    rotation.setFromTwoVectors(from.normal(), to.normal());
    Eigen::Translation3d translation(-to.normal() * to.offset());
    return translation * rotation;
  }

};

template <typename ObjectT>
  struct Constraint
  {
    virtual bool isValid(const ObjectT & object) const = 0;
  };

template <typename ObjectT>
  struct NoConstraint : public Constraint<ObjectT>
  {
    bool isValid(const ObjectT & object) const
    {
      return true;
    }
  };

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_TYPEDEFS_H_ */
