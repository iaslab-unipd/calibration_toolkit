/*
 * calibration_common.h
 *
 *  Created on: Feb 5, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_H_
#define CALIBRATION_COMMON_H_

#include <calibration_common/base/base.h>
#include <calibration_common/algorithms/algorithms.h>

//#include <calibration_common/objects/planar_object.h>
//#include <calibration_common/objects/checkerboard.h>
//
//#include <calibration_common/objects/sensor.h>
//#include <calibration_common/objects/color_sensor.h>
//#include <calibration_common/objects/depth_sensor.h>
//
//#include <calibration_common/objects/color_view.h>
//#include <calibration_common/objects/depth_view.h>
//
//#include <calibration_common/objects/camera_model.h>
//#include <calibration_common/objects/pinhole_camera_model.h>
//
//#include <calibration_common/algorithms/pose_estimator.h>
//#include <calibration_common/algorithms/pinhole_pose_estimator.h>
//
//#include <calibration_common/algorithms/checkerboard_finder.h>
//#include <calibration_common/algorithms/automatic_checkerboard_finder.h>
//
//#include <calibration_common/algorithms/plane_to_plane_calibration.h>
//#include <calibration_common/algorithms/point_on_plane_calibration.h>
//
//#include <calibration_common/errors/image_reprojection_error.h>
//#include <calibration_common/errors/fixed_number_points_error.h>
//#include <calibration_common/errors/euclidean_distance_error.h>
//#include <calibration_common/errors/angular_error.h>
//#include <calibration_common/errors/plane_distance_error.h>

#endif /* CALIBRATION_COMMON_H_ */
