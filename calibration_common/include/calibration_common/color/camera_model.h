/*
 * color/camera_model.h
 *
 *  Created on: Oct 20, 2012
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_COLOR_CAMERA_MODEL_H_
#define CALIBRATION_COMMON_COLOR_CAMERA_MODEL_H_

#include <calibration_common/base/point_set.h>

namespace calibration
{

/* ****************** Class definition ****************** */

/**
 *
 */
class ColorCameraModel
{
public:

  /* typedefs */

  // Smart pointers
  typedef boost::shared_ptr<ColorCameraModel> Ptr;
  typedef boost::shared_ptr<const ColorCameraModel> ConstPtr;

  /* methods */

  /**
   *
   */
  virtual
  ~ColorCameraModel()
  {
    // Do nothing
  }

  /**
   *
   * @param pixel_point
   * @return
   */
  virtual Point3d
  projectPixelTo3dRay(const Point2d & pixel_point) const = 0;

  /**
   *
   * @param world_point
   * @return
   */
  virtual Point2d
  project3dToPixel(const Point3d & world_point) const = 0;

  /**
   *
   * @param pixel_points
   * @return
   */
  virtual PointSet3d
  projectPixelTo3dRay(const PointSet2d & pixel_points) const = 0; // TODO implement here!!

  /**
   *
   * @param world_points
   * @return
   */
  virtual PointSet2d
  project3dToPixel(const PointSet3d & world_points) const = 0; // TODO implement here!!

};

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_COLOR_CAMERA_MODEL_H_ */
