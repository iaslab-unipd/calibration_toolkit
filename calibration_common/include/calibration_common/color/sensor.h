/*
 * color/sensor.h
 *
 *  Created on: Jun 14, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_COLOR_SENSOR_H_
#define CALIBRATION_COMMON_COLOR_SENSOR_H_

#include <calibration_common/base/sensor.h>
#include <calibration_common/color/camera_model.h>
#include <calibration_common/base/pose_estimator.h>

namespace calibration
{

class ColorSensor : public Sensor
{
public:

  enum Type
  {
    PINHOLE,
    OMNIDIRECTIONAL
  };

  // Smart pointers
  typedef boost::shared_ptr<ColorSensor> Ptr;
  typedef boost::shared_ptr<const ColorSensor> ConstPtr;

  /* methods */

  /**
   * @brief ColorSensor
   * @param camera_model
   * @param pose_estimator
   * @param frame_id
   * @param parent
   */
  ColorSensor(const Type & type,
              const ColorCameraModel::ConstPtr & camera_model,
              const PoseEstimator::ConstPtr & pose_estimator,
              const std::string & frame_id,
              const BaseObject::ConstPtr & parent = BaseObject::ConstPtr());

  /**
   * @brief ColorSensor
   * @param other
   */
  ColorSensor(const ColorSensor & other);

  /**
   * @brief ColorSensor
   * @param other
   * @param transform
   * @param new_frame_id
   * @param new_parent
   */
  ColorSensor(const ColorSensor & other,
              const Transform3d & transform,
              const std::string & new_frame_id,
              const BaseObject::ConstPtr & new_parent = BaseObject::ConstPtr());

  /**
   * @brief ~Sensor
   */
  virtual
  ~ColorSensor();

  Pose3d
  estimatePose(const PointSet2d & points_image,
               const PointSet3d & points_object) const;

  const ColorCameraModel::ConstPtr &
  cameraModel() const;

  const PoseEstimator::ConstPtr &
  poseEstimator() const;

protected:

  const Type type_;
  const ColorCameraModel::ConstPtr camera_model_;
  const PoseEstimator::ConstPtr pose_estimator_;

};

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_COLOR_SENSOR_H_ */
