/*
 * color/view.h
 *
 *  Created on: Aug 22, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATON_COMMON_COLOR_VIEW_H_
#define CALIBRATON_COMMON_COLOR_VIEW_H_

#include <calibration_common/color/sensor.h>

namespace calibration
{

template <typename ObjectT>
  class ColorView
  {
  public:

    // Smart pointers
    typedef boost::shared_ptr<ColorView> Ptr;
    typedef boost::shared_ptr<const ColorView> ConstPtr;

    ColorView(int id,
              const PointSet2d & interest_points,
              const ColorSensor::ConstPtr & sensor,
              const typename ObjectT::ConstPtr & object);

    virtual
    ~ColorView();

    const int &
    id() const;

    const PointSet2d &
    interestPoints() const;

    const typename ObjectT::ConstPtr &
    object() const;

    const ColorSensor::ConstPtr &
    sensor() const;

  protected:

    int id_;
    PointSet2d interest_points_;
    ColorSensor::ConstPtr sensor_;
    typename ObjectT::ConstPtr object_;

  };

} /* namespace calibration */

#include <impl/calibration_common/color/view.hpp>

#endif /* CALIBRATON_COMMON_COLOR_VIEW_H_ */
