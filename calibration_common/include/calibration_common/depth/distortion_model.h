/*
 * depth/distortion_model.h
 *
 *  Created on: Sep 8, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_DEPTH_DISTORTION_MODEL_H_
#define CALIBRATION_COMMON_DEPTH_DISTORTION_MODEL_H_

#include <pcl/point_cloud.h>

namespace calibration
{

template <typename PointT>
  class DepthDistortionModel
  {
  public:

    /* typedefs */

    // Smart pointers
    typedef boost::shared_ptr<DepthDistortionModel> Ptr;
    typedef boost::shared_ptr<const DepthDistortionModel> ConstPtr;

    /* methods */

    virtual void
    undistort(pcl::PointCloud<PointT> & cloud) const = 0;

  };

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_DEPTH_DISTORTION_MODEL_H_ */
