/*
 * depth/sensor.h
 *
 *  Created on: Aug 21, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_DEPTH_SENSOR_H_
#define CALIBRATION_COMMON_DEPTH_SENSOR_H_

#include <calibration_common/depth/distortion_model.h>
#include <calibration_common/base/sensor.h>

namespace calibration
{

template <typename PointT>
  class DepthSensor : public Sensor
  {
  public:

    enum Type
    {
      KINECT,
      LASER
    };

    // Smart pointers
    typedef boost::shared_ptr<DepthSensor> Ptr;
    typedef boost::shared_ptr<const DepthSensor> ConstPtr;

    typedef DepthDistortionModel<PointT> DistortionModelT;
    typedef typename DistortionModelT::ConstPtr DistortionModelConstPtr;

    /* methods */

    /**
     * @brief DepthSensor
     * @param frame_id
     * @param parent
     */
    DepthSensor(const Type & type,
                const DistortionModelConstPtr & distortion_model,
                const std::string & frame_id,
                const BaseObject::ConstPtr & parent = BaseObject::ConstPtr());

    /**
     * @brief DepthSensor
     * @param other
     */
    DepthSensor(const DepthSensor & other);

    /**
     * @brief DepthSensor
     * @param other
     * @param transform
     * @param new_frame_id
     * @param new_parent
     */
    DepthSensor(const DepthSensor & other,
                const Transform3d & transform,
                const std::string & new_frame_id,
                const BaseObject::ConstPtr & new_parent = BaseObject::ConstPtr());

    /**
     * @brief ~DepthSensor
     */
    virtual
    ~DepthSensor();

    void
    undistort(PointT & point) const;

    void
    undistort(pcl::PointCloud<PointT> & cloud) const;

    const DistortionModelConstPtr
    distortionModel() const;

    void
    setDistortionModel(const DistortionModelConstPtr & distortion_model);

  protected:

    const Type type_;
    DistortionModelConstPtr distortion_model_;

  };

} /* namespace calibration */

#include <impl/calibration_common/depth/sensor.hpp>

#endif /* CALIBRATION_COMMON_SENSOR_H_ */
