/*
 * depth/undistortion_map.h
 *
 *  Created on: Aug 26, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_DEPTH_UNDISTORTION_MAP_H_
#define CALIBRATION_COMMON_DEPTH_UNDISTORTION_MAP_H_

#include <calibration_common/base/point_set.h>
#include <opencv2/core/core.hpp>
#include <pcl/point_cloud.h>

namespace calibration
{

template <typename ScalarT, typename PointT>
  class UndistortionMap
  {
  public:

    typedef boost::shared_ptr<UndistortionMap> Ptr;
    typedef boost::shared_ptr<const UndistortionMap> ConstPtr;

    virtual void undistort(PointT & point) const = 0;
    virtual void undistort(pcl::PointCloud<PointT> & cloud) const = 0;

    virtual void draw(cv::Mat & image,
                      const PointT & point) const = 0;

    friend std::ostream & operator <<(std::ostream & os,
                                      const UndistortionMap & b)
    {
      return b.print(os);
    }

  protected:

    virtual std::ostream & print(std::ostream & os) const = 0;

  };

template <typename ScalarT, typename PointT>
  class NoDistortionMap : public UndistortionMap<ScalarT, PointT>
  {
  public:

    typedef boost::shared_ptr<NoDistortionMap> Ptr;
    typedef boost::shared_ptr<const NoDistortionMap> ConstPtr;

    typedef UndistortionMap<ScalarT, PointT> Base;

    NoDistortionMap()
    {
      // Do nothing
    }

    virtual void undistort(PointT & point) const
    {
      // Do nothing
    }

    virtual void undistort(pcl::PointCloud<PointT> & cloud) const
    {
      // Do nothing
    }

    virtual void draw(cv::Mat & image,
                      const PointT & point) const
    {
      // Do nothing
    }

  };

template <typename ScalarT, typename PointT, int Dimension>
  class UndistortionMap_ : public PointSet<ScalarT, Dimension>,
                           virtual public UndistortionMap<ScalarT, PointT>
  {
  protected:

    typedef typename PointSet2<ScalarT>::PointT PointSphere;

  public:

    typedef boost::shared_ptr<UndistortionMap_> Ptr;
    typedef boost::shared_ptr<const UndistortionMap_> ConstPtr;

    typedef PointSet<ScalarT, Dimension> Base;

    typedef typename Base::PointT Data;
    typedef typename Base::Element Element;
    typedef typename Base::ConstElement ConstElement;

    UndistortionMap_(const size_t & n_bin_long,
                   const size_t & n_bin_lat,
                   const ScalarT & width,
                   const ScalarT & height,
                   const PointSphere & zero,
                   const Data & value)
      : Base(n_bin_long, n_bin_lat, value), bin_width_(width / n_bin_long), bin_height_(height / n_bin_lat), zero_(zero)
    {
      // Do nothing
    }

    UndistortionMap_(const size_t & n_bin_long,
                   const size_t & n_bin_lat,
                   const UndistortionMap_ & other)
      : Base(n_bin_long, n_bin_lat), bin_width_(other.bin_width_ * other.width() / n_bin_long),
        bin_height_(other.bin_height_ * other.height() / n_bin_lat), zero_(other.zero_)
    {
      for (int r = 0; r < Base::height(); ++r)
      {
        for (int c = 0; c < Base::width(); ++c)
        {
          Base::operator ()(r, c) = other(static_cast<int>(std::floor(r * bin_height_ / other.bin_height_)),
                                          static_cast<int>(std::floor(c * bin_width_ / other.bin_width_)));
        }
      }
    }

    virtual PointT toPolar(const PointT & ray) const = 0;

    virtual PointSphere toSphere(const PointT & ray) const = 0;

    using Base::at;

    const ConstElement at(const PointT & point,
                          int & row,
                          int & col) const
    {
      return at(toSphere(point), row, col);
    }

    const ConstElement at(const PointSphere & polar,
                          int & row,
                          int & col) const
    {
      PointSphere r = polar - zero_;
      col = static_cast<int>(std::floor(r[0] / bin_width_));
      row = static_cast<int>(std::floor(r[1] / bin_height_));

      col = std::max(0, col);
      col = std::min(static_cast<int>(Base::width()) - 1, col);

      row = std::max(0, row);
      row = std::min(static_cast<int>(Base::height()) - 1, row);

      return Base::operator ()(row, col);
    }

    virtual void undistort(PointT & point) const
    {
      int r, c;
      undistort(point, at(point, r, c));
    }

    virtual void undistort(pcl::PointCloud<PointT> & cloud) const
    {
      int r, c;
      for (int i = 0; i < cloud.size(); ++i)
        undistort(cloud.points[i], at(cloud.points[i], r, c));
    }

    virtual void undistort(PointT & point,
                           const ConstElement data) const = 0;

    virtual void draw(cv::Mat & image,
                      const PointT & point) const
    {
//      int d_x = image.cols / Base::width();
//      int d_y = image.rows / Base::height();
//      for (int r = 0; r < Base::height(); ++r)
//      {
//        for (int c = 0; c < Base::width(); ++c)
//        {
//          Base::operator ()(r, c) = other(static_cast<int>(std::floor(r * bin_height_ / other.bin_height_)),
//                                          static_cast<int>(std::floor(c * bin_width_ / other.bin_width_)));
//        }
//      }
    }

    virtual std::ostream & print(std::ostream & os) const
    {
      os << this->matrix() << std::endl;
      return os;
    }

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  protected:

    std::vector<cv::Vec3b> color_map_;

    ScalarT bin_width_;
    ScalarT bin_height_;
    PointSphere zero_;

  };

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_DEPTH_UNDISTORTION_MAP_H_ */
