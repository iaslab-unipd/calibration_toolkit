/*
 * depth/undistortion_map_multifit.h
 *
 *  Created on: Sep 5, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_DEPTH_UNDISTORTION_MAP_MULTIFIT_H_
#define CALIBRATION_COMMON_DEPTH_UNDISTORTION_MAP_MULTIFIT_H_

#include <calibration_common/depth/undistortion_map.h>

namespace calibration
{

template <typename PointT>
  class UndistortionMapMultifit
  {
  public:

    typedef boost::shared_ptr<UndistortionMapMultifit> Ptr;
    typedef boost::shared_ptr<const UndistortionMapMultifit> ConstPtr;

    typedef typename pcl::PointCloud<PointT>::ConstPtr PointCloudConstPtr;

    virtual void
    addPoint(const PointT & point,
             const Plane3d & plane) = 0;

    virtual void
    accumulateCloud(const PointCloudConstPtr & cloud,
                    const std::vector<int> & indices) = 0;

    virtual void
    accumulatePoint(const PointT & point) = 0;

    virtual void
    addAccumulatedPoints(const Plane3d & plane) = 0;

    virtual void
    update() = 0;

    virtual const typename UndistortionMap<double, PointT>::ConstPtr
    undistortionMap() const = 0;

  };

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_DEPTH_UNDISTORTION_MAP_MULTIFIT_H_ */
