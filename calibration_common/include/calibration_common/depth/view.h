/*
 * depth/view.h
 *
 *  Created on: Aug 22, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATON_COMMON_DEPTH_VIEW_H_
#define CALIBRATON_COMMON_DEPTH_VIEW_H_

#include <calibration_common/base/point_set.h>
#include <calibration_common/depth/sensor.h>
#include <visualization_msgs/Marker.h>

namespace calibration
{

template <typename ObjectT, typename PointT>
  class DepthView
  {
  public:

    // Smart pointers
    typedef boost::shared_ptr<DepthView> Ptr;
    typedef boost::shared_ptr<const DepthView> ConstPtr;

    typedef typename DepthSensor<PointT>::ConstPtr DepthSensorConstPtr;

    DepthView(int id,
              const pcl::PointCloud<PointT> & data,
              const std::vector<int> & indices,
              const DepthSensorConstPtr & sensor,
              const typename ObjectT::ConstPtr & object);

    DepthView(int id,
              const PointSet3d & points,
              const DepthSensorConstPtr & sensor,
              const typename ObjectT::ConstPtr & object);

    virtual
    ~DepthView();

    const int &
    id() const;

    const PointSet3d &
    interestPoints() const;

    const typename ObjectT::ConstPtr &
    object() const;

    const DepthSensorConstPtr &
    sensor() const;

    void
    toMarker(visualization_msgs::Marker & marker) const;

  protected:

    int id_;
    PointSet3d interest_points_;
    DepthSensorConstPtr sensor_;
    typename ObjectT::ConstPtr object_;

  };

} /* namespace calibration */

#include <impl/calibration_common/depth/view.hpp>

#endif /* CALIBRATON_COMMON_DEPTH_VIEW_H_ */
