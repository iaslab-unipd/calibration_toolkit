/*
 * errors/angular.h
 *
 *  Created on: Dec 12, 2012
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_ERRORS_ANGULAR_H_
#define CALIBRATION_COMMON_ERRORS_ANGULAR_H_

#include <calibration_common/errors/fixed_number_points.h>

namespace calibration
{

/* ****************** Class definition ****************** */

/**
 * Given a set of points \f$ p_i \in \mathbb{R}^D \f$, error \f$ \theta_{err} \f$ is calculated as
 * \f[
 *      \theta_{err} = \sum_i^N \arccos \left( \frac{r_i^T \, p_i}{\| r_i \| \| p_i \|} \right),
 * \f]
 * where \f$ r_i \in \mathbb{R}^D \f$ are the real locations of the points, \f$ N \f$ is the number
 * of such points and \f$ D \f$ is the dimension of the space.
 */
template <int Dimension>
class AngularError : public FixedNumberPointsError<AngularError<Dimension> >
{

public:

  /* typedefs */

  // Smart pointer
  typedef boost::shared_ptr<AngularError<Dimension> > Ptr;
  typedef boost::shared_ptr<const AngularError<Dimension> > ConstPtr;

  // Eigen
  typedef typename ErrorInfo<AngularError<Dimension> >::MatrixT MatrixT;

  typedef FixedNumberPointsError<AngularError<Dimension> > Base;
  using Base::num_points_;

  /* methods */

  /**
   *
   */
  AngularError();

  /**
   * @see setRealPoints
   */
  AngularError(const MatrixT& points_real);

  /**
   *
   */
  virtual
  ~AngularError();

  /**
   * Set the real points \f$ r_i \in \mathbb{R}^D, \, i = 1, \dots, N \f$
   * @param points_real a \f$ D \times N \f$ matrix
   */
  virtual void
  setRealPoints(const MatrixT& points_real);

  /**
   *
   * @param points_estimated
   * @param errors
   * @return
   */
  template <typename Derived>
  double
  computeNoCheck(const MatrixT& points, const Eigen::MatrixBase<Derived>& errors) const;

protected:

  /* variables */

  MatrixT points_real_normalized_;     ///

};

template <int Dimension>
struct ErrorInfo<AngularError<Dimension> >
{
  typedef Eigen::Matrix<double, Dimension, Eigen::Dynamic> MatrixT;
  enum
  {
    Dim = Dimension
  };
};

/* ****************** Methods implementation ****************** */

template <int Dimension>
AngularError<Dimension>::AngularError()
: Base()
{
  // Do nothing
}

template <int Dimension>
AngularError<Dimension>::AngularError(const MatrixT & points_real)
: Base()
{
  setRealPoints(points_real);
}

template <int Dimension>
AngularError<Dimension>::~AngularError()
{
  // Do nothing
}

template <int Dimension>
inline void
AngularError<Dimension>::setRealPoints(const MatrixT & points_real)
{
  Base::setRealPoints(points_real);
  points_real_normalized_.resize(Dimension, num_points_);
  for(int i = 0; i < num_points_; ++i)
    points_real_normalized_.col(i) = points_real.col(i).normalized();
}

template <int Dimension>
template <typename Derived>
inline double
AngularError<Dimension>::computeNoCheck(const MatrixT & points,
                                        const Eigen::MatrixBase<Derived>& errors) const
{
  Eigen::MatrixBase<Derived>& non_const_errors = const_cast<Eigen::MatrixBase<Derived>&>(errors);
  for(int i = 0; i < num_points_; ++i)
    non_const_errors[i] = std::acos(points_real_normalized_.col(i).dot(points.col(i).normalized()));
  return errors.sum();
}

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_ERRORS_ANGULAR_H_ */
