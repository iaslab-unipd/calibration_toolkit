/*
 * errors/euclidean_distance.h
 *
 *  Created on: Dec 11, 2012
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_ERRORS_EUCLIDEAN_DISTANCE_H_
#define CALIBRATION_COMMON_ERRORS_EUCLIDEAN_DISTANCE_H_

#include <calibration_common/errors/fixed_number_points.h>

namespace calibration
{

/* ****************** Class definition ****************** */

/**
 *
 */
template <int Dimension>
  class EuclideanDistanceError : public FixedNumberPointsError<EuclideanDistanceError<Dimension> >
  {

  public:

    /* typedefs */

    // Smart pointers
    typedef boost::shared_ptr<EuclideanDistanceError<Dimension> > Ptr;
    typedef boost::shared_ptr<const EuclideanDistanceError<Dimension> > ConstPtr;

    // Eigen
    typedef typename ErrorInfo<EuclideanDistanceError<Dimension> >::PointSetT PointSetT;

    typedef FixedNumberPointsError<EuclideanDistanceError<Dimension> > Base;
    using Base::num_points_;
    using Base::points_real_;

    /* methods */

    /**
     * @brief EuclideanDistanceError
     */
    EuclideanDistanceError();

    /**
     * @brief EuclideanDistanceError
     * @param points_real
     */
    EuclideanDistanceError(const PointSetT & points_real);

    /**
     * @brief ~EuclideanDistanceError
     */
    virtual
    ~EuclideanDistanceError();

    /**
     * @brief computeNoCheck
     * @param points_estimated
     * @param errors
     * @return
     */
    template <typename Derived>
      double
      computeNoCheck(const PointSetT & points_estimated,
                     const Eigen::MatrixBase<Derived> & errors) const;

  };

template <int Dimension>
  struct ErrorInfo<EuclideanDistanceError<Dimension> >
  {
    //typedef Eigen::Matrix<double, Dimension, Eigen::Dynamic, Eigen::ColMajor, Dimension> EigenMatrix;
    typedef PointSet<double, Dimension> PointSetT;
    enum
    {
      Dim = Dimension
    };
  };

/* ****************** Methods implementation ****************** */

template <int Dimension>
  EuclideanDistanceError<Dimension>::EuclideanDistanceError()
    : Base()
  {
    // Do nothing
  }

template <int Dimension>
  EuclideanDistanceError<Dimension>::EuclideanDistanceError(const PointSetT & points_real)
    : Base(points_real)
  {
    // Do nothing
  }

template <int Dimension>
  EuclideanDistanceError<Dimension>::~EuclideanDistanceError()
  {
    // Do nothing
  }

template <int Dimension>
  template <typename Derived>
    inline double EuclideanDistanceError<Dimension>::computeNoCheck(const PointSetT & points,
                                                                    const Eigen::MatrixBase<Derived> & errors) const
    {
      Eigen::MatrixBase<Derived> & non_const_errors = const_cast<Eigen::MatrixBase<Derived> &>(errors);
      for (int i = 0; i < num_points_; ++i)
        non_const_errors[i] = (points_real_[i] - points[i]).squaredNorm();
      return errors.sum();
    }

} /* namespace calibration */

#endif /* CALIBRATION_COMMON_ERRORS_EUCLIDEAN_DISTANCE_H_ */
