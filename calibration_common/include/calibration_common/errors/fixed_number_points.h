/*
 * errors/fixed_number_points.h
 *
 *  Created on: Jan 22, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_ERRORS_FIXED_NUMBER_POINTS_H_
#define CALIBRATION_COMMON_ERRORS_FIXED_NUMBER_POINTS_H_

#include <boost/smart_ptr.hpp>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <calibration_common/base/typedefs.h>

namespace calibration
{

template <typename ErrorType>
  struct ErrorInfo
  {

  };

/* ****************** Class definition ****************** */

/**
 *
 */
template <typename Derived>
  class FixedNumberPointsError
  {

  public:

    /* typedefs */

    // Smart pointers
    typedef boost::shared_ptr<FixedNumberPointsError<Derived> > Ptr;
    typedef boost::shared_ptr<const FixedNumberPointsError<Derived> > ConstPtr;

    // Derived
    typedef typename ErrorInfo<Derived>::PointSetT PointSetT;

    /* methods */

    /**
     *
     */
    FixedNumberPointsError();

    /**
     * @see setRealPoints
     */
    FixedNumberPointsError(const PointSetT & points_real);

    /**
     *
     */
    virtual
    ~FixedNumberPointsError();

    /**
     *
     * @return
     */
    virtual int
    size() const;

    /**
     *
     * @param points_real
     */
    virtual void
    setRealPoints(const PointSetT & points_real);

//  /**
//   * @brief compute
//   * @param points
//   * @param errors
//   * @return
//   */
//  template <typename OtherDerived>
//  double
//  compute(const EigenMatrix& points, const Eigen::MatrixBase<OtherDerived>& errors) const;

  protected:

    /* variables */

    PointSetT points_real_;
    int num_points_;

  public:

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  };

/* ****************** Methods implementation ****************** */

template <typename Derived>
  FixedNumberPointsError<Derived>::FixedNumberPointsError()
    : num_points_(0)
  {
    // Do nothing
  }

template <typename Derived>
  FixedNumberPointsError<Derived>::FixedNumberPointsError(const PointSetT & points_real)
    : points_real_(points_real), num_points_(points_real.size())
  {
    // Do nothing
  }

template <typename Derived>
  FixedNumberPointsError<Derived>::~FixedNumberPointsError()
  {
    // Do nothing
  }

template <typename Derived>
  inline int FixedNumberPointsError<Derived>::size() const
  {
    return num_points_;
  }

template <typename Derived>
  inline void FixedNumberPointsError<Derived>::setRealPoints(const PointSetT & points_real)
  {
    points_real_ = points_real;
    num_points_ = points_real_.size();
  }

//template <typename Derived>
//template <typename OtherDerived>
//inline double
//FixedNumberPointsError<Derived>::compute(const EigenMatrix& points_estimated,
//                                         const Eigen::MatrixBase<OtherDerived>& errors) const
//{
//  assert(points_estimated.cols() == num_points_);
//  Eigen::MatrixBase<OtherDerived>& non_const_errors = const_cast<Eigen::MatrixBase<OtherDerived>&>(errors);
//  return static_cast<const Derived*>(this)->computeNoCheck(points_estimated, non_const_errors);
//}

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_ERRORS_ */
