/*
 * errors/image_reprojection.h
 *
 *  Created on: Dec 11, 2012
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_ERRORS_IMAGE_REPROJECTION_H_
#define CALIBRATION_COMMON_ERRORS_IMAGE_REPROJECTION_H_

#include <calibration_common/color/camera_model.h>
#include <calibration_common/base/base_object.h>
#include <calibration_common/errors/euclidean_distance.h>

namespace calibration
{

/* ****************** Class definition ****************** */

/**
 *
 */
class ImageReprojectionError : public EuclideanDistanceError<2>
{

public:

  /* typedefs */

  // Smart pointers
  typedef boost::shared_ptr<ImageReprojectionError> Ptr;
  typedef boost::shared_ptr<const ImageReprojectionError> ConstPtr;

  // Derived
  typedef EuclideanDistanceError<2> Base;

  using Base::num_points_;

  /* methods */

  /**
   * @brief ImageReprojectionError
   * @param camera_model
   */
  ImageReprojectionError(const ColorCameraModel::ConstPtr & camera_model);

  ImageReprojectionError(const ColorCameraModel::ConstPtr & camera_model,
                         const PointSet2d & image_points);

  /**
   * @brief ~ImageReprojectionError
   */
  virtual
  ~ImageReprojectionError();

  /**
   * @brief setRealImagePoints
   * @param image_points
   */
  void
  setRealImagePoints(const PointSet2d & image_points);

  /**
   * @brief compute
   * @param points
   * @param errors
   * @return
   */
  template <typename Derived>
    double
    compute(const PointSet3d & points,
            const Eigen::MatrixBase<Derived> & errors) const;

  /**
   * @brief compute
   * @param object
   * @param errors
   * @return
   */
  template <typename Derived>
    double
    compute(const Pattern & object,
            const Eigen::MatrixBase<Derived> & errors) const;

protected:

  /* variables */

  ColorCameraModel::ConstPtr camera_model_;

};

/* ****************** Methods implementation ****************** */

inline ImageReprojectionError::ImageReprojectionError(const ColorCameraModel::ConstPtr & camera_model)
  : EuclideanDistanceError<2>(), camera_model_(camera_model)
{
  // Do nothing
}

inline ImageReprojectionError::ImageReprojectionError(const ColorCameraModel::ConstPtr & camera_model,
                                                      const PointSet2d & image_points)
  : EuclideanDistanceError<2>(image_points), camera_model_(camera_model)
{
  // Do nothing
}

inline ImageReprojectionError::~ImageReprojectionError()
{
  // Do nothing
}

inline void ImageReprojectionError::setRealImagePoints(const PointSet2d & image_points)
{
  Base::setRealPoints(image_points);
}

template <typename Derived>
  inline double ImageReprojectionError::compute(const PointSet3d & world_points,
                                                const Eigen::MatrixBase<Derived> & errors) const
  {
    Eigen::MatrixBase<Derived> & non_const_errors = const_cast<Eigen::MatrixBase<Derived> &>(errors);
    return Base::computeNoCheck(camera_model_->project3dToPixel(world_points), non_const_errors);
  }

template <typename Derived>
  inline double ImageReprojectionError::compute(const Pattern & object,
                                                const Eigen::MatrixBase<Derived> & errors) const
  {
    Eigen::MatrixBase<Derived> & non_const_errors = const_cast<Eigen::MatrixBase<Derived> &>(errors);
    return Base::computeNoCheck(camera_model_->project3dToPixel(object.points()), non_const_errors);
  }

} /* namespace calibration */
#endif /* IMAGE_REPROJECTION_ERROR_H_ */
