/*
 * errors/pcl_plane_distance.h
 *
 *  Created on: Jan 23, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_ERRORS_POINT_CLOUD_PLANE_DISTANCE_H_
#define CALIBRATION_COMMON_ERRORS_POINT_CLOUD_PLANE_DISTANCE_H_

#include <Eigen/Geometry>
#include <pcl/sample_consensus/sac_model_plane.h>

namespace calibration
{

/* ****************** Class definition ****************** */

/**
 *
 */
template <typename PointType>
  class PlaneDistanceError
  {
  public:

    /* typedefs */

    // Smart pointers
    typedef boost::shared_ptr<PlaneDistanceError<PointType> > Ptr;
    typedef boost::shared_ptr<const PlaneDistanceError<PointType> > ConstPtr;

    typedef typename pcl::PointCloud<PointType>::ConstPtr PointCloudConstPtr;
    typedef typename pcl::SampleConsensusModelPlane<PointType>::Ptr SampleConsensusModelPlanePtr;

    /* methods */

    /**
     * @brief PlaneDistanceError
     * @param cloud
     * @param inliers
     */
    PlaneDistanceError(const PointCloudConstPtr & cloud,
                       const pcl::IndicesConstPtr & inliers);

    /**
     * @brief ~PlaneDistanceError
     */
    virtual
    ~PlaneDistanceError();

    /**
     * @brief size
     * @return
     */
    virtual int
    size() const;

    /**
     * @brief compute
     * @param plane
     * @param errors
     * @return
     */
    template <typename Derived>
      double
      compute(const Plane3d & plane,
              const Eigen::MatrixBase<Derived> & errors) const;

  protected:

    /* variables */

    const PointCloudConstPtr cloud_;
    const pcl::IndicesConstPtr inliers_;
    SampleConsensusModelPlanePtr model_;

  };

/* ****************** Methods implementation ****************** */

template <typename PointType>
  PlaneDistanceError<PointType>::PlaneDistanceError(const PointCloudConstPtr & cloud,
                                                    const pcl::IndicesConstPtr & inliers)
    : cloud_(cloud), inliers_(inliers), model_(new pcl::SampleConsensusModelPlane<PointType>(cloud, *inliers))
  {
    // Do nothing
  }

template <typename PointType>
  PlaneDistanceError<PointType>::~PlaneDistanceError()
  {
    // Do nothing
  }

template <typename PointType>
  inline int PlaneDistanceError<PointType>::size() const
  {
    return inliers_->size();
  }

template <typename PointType>
  template <typename Derived>
    double PlaneDistanceError<PointType>::compute(const Plane3d & plane,
                                                  const Eigen::MatrixBase<Derived> & errors) const
    {
      Eigen::VectorXf plane_coefficients(plane.coeffs().template cast<float>());

      std::vector<double> errors_vector;
      model_->getDistancesToModel(plane_coefficients, errors_vector);

      Eigen::MatrixBase<Derived> & non_const_errors = const_cast<Eigen::MatrixBase<Derived> &>(errors);
      for (unsigned int i = 0; i < errors_vector.size(); ++i)
        non_const_errors[i] = errors_vector[i] * errors_vector[i];

      return errors.sum();
    }

} /* namespace calibration */
#endif /* POINT_CLOUD_PLANE_DISTANCE_ERROR_H_ */
