/*
 * errors/plane_distance.h
 *
 *  Created on: Jan 23, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_ERRORS_PLANE_DISTANCE_H_
#define CALIBRATION_COMMON_ERRORS_PLANE_DISTANCE_H_

#include <calibration_common/base/typedefs.h>

namespace calibration
{

/* ****************** Class definition ****************** */

/**
 *
 */
template <typename PointType>
  class PlaneDistanceError
  {
  public:

    /* typedefs */

    // Smart pointers
    typedef boost::shared_ptr<PlaneDistanceError<PointType> > Ptr;
    typedef boost::shared_ptr<const PlaneDistanceError<PointType> > ConstPtr;

    /* methods */

    /**
     * @brief PlaneDistanceError
     * @param cloud
     * @param inliers
     */
    PlaneDistanceError(const PointSet3d & points);

    /**
     * @brief ~PlaneDistanceError
     */
    virtual
    ~PlaneDistanceError();

    /**
     * @brief size
     * @return
     */
    virtual int
    size() const;

    /**
     * @brief compute
     * @param plane
     * @param errors
     * @return
     */
    template <typename Derived>
      double
      compute(const Plane3d & plane,
              const Eigen::MatrixBase<Derived> & errors) const;

  protected:

    /* variables */

    const PointSet3d points_;

  };

/* ****************** Methods implementation ****************** */

template <typename PointType>
  PlaneDistanceError<PointType>::PlaneDistanceError(const PointSet3d & points)
    : points_(points)
  {
    // Do nothing
  }

template <typename PointType>
  PlaneDistanceError<PointType>::~PlaneDistanceError()
  {
    // Do nothing
  }

template <typename PointType>
  inline int PlaneDistanceError<PointType>::size() const
  {
    return points_.size();
  }

template <typename PointType>
  template <typename Derived>
    double PlaneDistanceError<PointType>::compute(const Plane3d & plane,
                                                  const Eigen::MatrixBase<Derived> & errors) const
    {
      Eigen::MatrixBase<Derived> & non_const_errors = const_cast<Eigen::MatrixBase<Derived> &>(errors);
      for (unsigned int i = 0; i < points_.size(); ++i)
      {
        double d = plane.signedDistance(points_[i]);
        non_const_errors[i] = d * d;
      }
      return errors.sum();
    }

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_ERRORS_PLANE_DISTANCE_H_ */
