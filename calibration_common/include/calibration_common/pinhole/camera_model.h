/*
 * pinhole/camera_model.h
 *
 *  Created on: Oct 18, 2012
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_PINHOLE_CAMERA_MODEL_H_
#define CALIBRATION_COMMON_PINHOLE_CAMERA_MODEL_H_

#include <calibration_common/color/camera_model.h>
#include <image_geometry/pinhole_camera_model.h>

namespace calibration
{

class PinholeCameraModel : public ColorCameraModel,
                           public image_geometry::PinholeCameraModel
{
public:

  /* typedefs */

  // Smart pointers
  typedef boost::shared_ptr<PinholeCameraModel> Ptr;
  typedef boost::shared_ptr<const PinholeCameraModel> ConstPtr;

  typedef image_geometry::PinholeCameraModel Base;

  /* methods */

  /**
   *
   */
  PinholeCameraModel();

  /**
   *
   * @param msg
   */
  PinholeCameraModel(const sensor_msgs::CameraInfo & msg);

  /**
   *
   * @param msg
   */
  PinholeCameraModel(const PinholeCameraModel & other);

  /**
   *
   * @param pixel_point
   * @return
   */
  virtual Point3d
  projectPixelTo3dRay(const Point2d & pixel_point) const;

  /**
   *
   * @param world_point
   * @return
   */
  virtual Point2d
  project3dToPixel(const Point3d & world_point) const;

  /**
   *
   * @param pixel_points
   * @return
   */
  virtual PointSet3d
  projectPixelTo3dRay(const PointSet2d & pixel_points) const;

  /**
   *
   * @param world_points
   * @return
   */
  virtual PointSet2d
  project3dToPixel(const PointSet3d & world_points) const;

};

} /* namespace calibration */

#endif /* CALIBRATION_COMMON_PINHOLE_CAMERA_MODEL_H_ */
