/*
 * pinhole/pose_estimator.h
 *
 *  Created on: Dec 17, 2012
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_PINHOLE_POSE_ESTIMATOR_H_
#define CALIBRATION_COMMON_PINHOLE_POSE_ESTIMATOR_H_

#include <calibration_common/base/pose_estimator.h>
#include <calibration_common/pinhole/camera_model.h>
#include <magic_folder/eigen_conversions.h>

namespace calibration
{

/* ****************** Class definition ****************** */

class PinholePoseEstimator : public PoseEstimator
{
public:

  /* typedefs */

  // Smart pointers
  typedef boost::shared_ptr<PoseEstimator> Ptr;
  typedef boost::shared_ptr<const PoseEstimator> ConstPtr;

  // Others
  typedef cv::Vec<double, 2> CvVector2d;
  typedef cv::Vec<double, 3> CvVector3d;
  typedef cv::Mat_<CvVector2d> CvMatrix2Xd;
  typedef cv::Mat_<CvVector3d> CvMatrix3Xd;

  /* methods */

  /**
   * @brief PinholePoseEstimator
   * @param camera_model
   */
  PinholePoseEstimator(const PinholeCameraModel::ConstPtr & camera_model);

  /**
   * @brief ~PinholePoseEstimator
   */
  virtual
  ~PinholePoseEstimator();

  /**
   * @brief estimatePose
   * @param points_image
   * @param points_object
   * @return
   */
  virtual Pose3d
  estimatePose(const PointSet2d & points_image,
               const PointSet3d & points_object) const;

protected:

  PinholeCameraModel::ConstPtr camera_model_;

};

/* ****************** Methods implementation ****************** */

inline PinholePoseEstimator::PinholePoseEstimator(const PinholeCameraModel::ConstPtr & camera_model)
  : PoseEstimator(), camera_model_(camera_model)
{

}

inline PinholePoseEstimator::~PinholePoseEstimator()
{
  // Do nothing
}

inline Pose3d PinholePoseEstimator::estimatePose(const PointSet2d & points_image,
                                                 const PointSet3d & points_object) const
{
  assert(points_image.cols() == points_object.cols());

  magic_folder::EigenConversions<double> converter;
  CvMatrix2Xd cv_points_image;
  CvMatrix3Xd cv_points_object;
  converter.eigen2opencv(points_image.matrix(), cv_points_image);
  converter.eigen2opencv(points_object.matrix(), cv_points_object);

  CvVector3d cv_r, cv_t;
  cv::solvePnP(cv_points_object, cv_points_image, camera_model_->intrinsicMatrix(), camera_model_->distortionCoeffs(),
               cv_r, cv_t);

  Eigen::Vector3d r = converter.opencv2eigen(cv_r);
  Eigen::Vector3d t = converter.opencv2eigen(cv_t);

  Eigen::AngleAxisd rotation(r.norm(), r.normalized());
  Eigen::Translation3d translation(t);

  return translation * rotation;
}

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_PINHOLE_POSE_ESTIMATOR_H_ */
