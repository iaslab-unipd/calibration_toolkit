/*
 * math.hpp
 *
 *  Created on: Sep 22, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_IMPL_MATH_HPP_
#define CALIBRATION_COMMON_IMPL_MATH_HPP_

#include <calibration_common/base/math.h>
#include <unsupported/Eigen/Polynomials>

namespace calibration
{

/* ********** MathFunction ********** */

template <typename Derived>
  typename MathFunction<Derived>::Scalar MathFunction<Derived>::operator ()(const Scalar & x) const
  {
    return evaluate(x);
  }

/* ********** MathFunctionWithCoefficients ********** */

template <typename Derived>
  MathFunctionWithCoefficients<Derived>::MathFunctionWithCoefficients(const Coefficients & coefficients)
    : coefficients_(coefficients)
  {
    // Do nothing
  }

template <typename Derived>
  MathFunctionWithCoefficients<Derived>::~MathFunctionWithCoefficients()
  {
    // Do nothing
  }

template <typename Derived>
  typename MathFunctionWithCoefficients<Derived>::Coefficients & MathFunctionWithCoefficients<Derived>::coefficients()
  {
    return coefficients_;
  }

template <typename Derived>
  const typename MathFunctionWithCoefficients<Derived>::Coefficients & MathFunctionWithCoefficients<Derived>::coefficients() const
  {
    return coefficients_;
  }

template <typename Derived>
  int MathFunctionWithCoefficients<Derived>::size() const
  {
    return coefficients_.size();
  }

/* ********** PolynomialFunction ********** */

template <typename ScalarT, int Degree, int MinDegree>
  PolynomialFunction<ScalarT, Degree, MinDegree>::PolynomialFunction()
    : Base(Identity())
  {
    EIGEN_STATIC_ASSERT((Info::DEGREE >= Info::MIN_DEGREE), INVALID_MATRIX_TEMPLATE_PARAMETERS);
  }

template <typename ScalarT, int Degree, int MinDegree>
  template <typename OtherDerived>
    PolynomialFunction<ScalarT, Degree, MinDegree>::PolynomialFunction(const Eigen::MatrixBase<OtherDerived> & other)
      : Base(other)
    {
      EIGEN_STATIC_ASSERT((Info::DEGREE >= Info::MIN_DEGREE), INVALID_MATRIX_TEMPLATE_PARAMETERS);
    }

template <typename ScalarT, int Degree, int MinDegree>
  typename PolynomialFunction<ScalarT, Degree, MinDegree>::Scalar PolynomialFunction<ScalarT, Degree, MinDegree>::evaluate(const Scalar & x) const
  {
    Scalar y = Eigen::poly_eval(Base::coefficients(), x);
    for (int i = 0; i < Info::MIN_DEGREE; ++i, y *= x);
    return y;
  }

template <typename ScalarT, int Degree, int MinDegree>
  const typename PolynomialFunction<ScalarT, Degree, MinDegree>::Scalar & PolynomialFunction<ScalarT, Degree, MinDegree>::coefficient(const DegreeIndex & degree) const
  {
    assert(degree >= Info::MIN_DEGREE and degree <= Info::DEGREE);
    return Base::operator [](degree - Info::MIN_DEGREE);
  }

template <typename ScalarT, int Degree, int MinDegree>
  typename PolynomialFunction<ScalarT, Degree, MinDegree>::Scalar & PolynomialFunction<ScalarT, Degree, MinDegree>::coefficient(const DegreeIndex & degree)
  {
    assert(degree >= Info::MIN_DEGREE and degree <= Info::DEGREE);
    return Base::operator [](degree - Info::MIN_DEGREE);
  }

template <typename ScalarT, int Degree, int MinDegree>
  const typename PolynomialFunction<ScalarT, Degree, MinDegree>::Polynomial PolynomialFunction<ScalarT, Degree,
    MinDegree>::Identity()
  {
    EIGEN_STATIC_ASSERT((Info::MIN_DEGREE <= 1), INVALID_MATRIX_TEMPLATE_PARAMETERS);
    Polynomial poly(Polynomial::Zero());
    poly[1 - Info::MIN_DEGREE] = Scalar(1);
    return poly;
  }

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_MATH_H_ */
