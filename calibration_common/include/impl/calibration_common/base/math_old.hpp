/*
 * math.hpp
 *
 *  Created on: Sep 22, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_IMPL_MATH_HPP_
#define CALIBRATION_COMMON_IMPL_MATH_HPP_

#include <calibration_common/base/math_old.h>
#include <unsupported/Eigen/Polynomials>

namespace calibration
{

/* ********** MathFunction ********** */

template <typename ScalarT>
  ScalarT MathFunction<ScalarT>::operator ()(const ScalarT & x) const
  {
    return evaluate(x);
  }

/* ********** MathFunctionWithCoefficients ********** */

template <typename ScalarT, int Size>
  MathFunctionWithCoefficients<ScalarT, Size>::MathFunctionWithCoefficients(const Coefficients & coefficients)
    : coefficients_(coefficients)
  {
    // Do nothing
  }

template <typename ScalarT, int Size>
  MathFunctionWithCoefficients<ScalarT, Size>::~MathFunctionWithCoefficients()
  {
    // Do nothing
  }

template <typename ScalarT, int Size>
  Eigen::Matrix<ScalarT, Size, 1> & MathFunctionWithCoefficients<ScalarT, Size>::coefficients()
  {
    return coefficients_;
  }

template <typename ScalarT, int Size>
  const Eigen::Matrix<ScalarT, Size, 1> & MathFunctionWithCoefficients<ScalarT, Size>::coefficients() const
  {
    return coefficients_;
  }

template <typename ScalarT, int Size>
  int MathFunctionWithCoefficients<ScalarT, Size>::size() const
  {
    return coefficients_.size();
  }

/* ********** PolynomialFunction ********** */

template <typename ScalarT, int Degree, int MinDegree>
  PolynomialFunction<ScalarT, Degree, MinDegree>::PolynomialFunction()
    : Base(IdentityPolynomial())
  {
    EIGEN_STATIC_ASSERT((DEGREE >= MIN_DEGREE), INVALID_MATRIX_TEMPLATE_PARAMETERS);
  }

template <typename ScalarT, int Degree, int MinDegree>
  template <typename OtherDerived>
    PolynomialFunction<ScalarT, Degree, MinDegree>::PolynomialFunction(const Eigen::MatrixBase<OtherDerived> & other)
      : Base(other)
    {
      EIGEN_STATIC_ASSERT((DEGREE >= MIN_DEGREE), INVALID_MATRIX_TEMPLATE_PARAMETERS);
    }

template <typename ScalarT, int Degree, int MinDegree>
  ScalarT PolynomialFunction<ScalarT, Degree, MinDegree>::evaluate(const ScalarT & x) const
  {
    ScalarT y = Eigen::poly_eval(Base::coefficients(), x);
    for (int i = 0; i < MIN_DEGREE; ++i, y *= x);
    return y;
  }

template <typename ScalarT, int Degree, int MinDegree>
  const ScalarT & PolynomialFunction<ScalarT, Degree, MinDegree>::coefficient(Eigen::DenseIndex degree) const
  {
    assert(degree >= MIN_DEGREE and degree <= DEGREE);
    return Base::operator [](degree - MIN_DEGREE);
  }

template <typename ScalarT, int Degree, int MinDegree>
  ScalarT & PolynomialFunction<ScalarT, Degree, MinDegree>::coefficient(Eigen::DenseIndex degree)
  {
    assert(degree >= MIN_DEGREE and degree <= DEGREE);
    return Base::operator [](degree - MIN_DEGREE);
  }

template <typename ScalarT, int Degree, int MinDegree>
  const typename PolynomialFunction<ScalarT, Degree, MinDegree>::PolynomialT PolynomialFunction<ScalarT, Degree,
    MinDegree>::IdentityPolynomial()
  {
    EIGEN_STATIC_ASSERT((MIN_DEGREE <= 1), INVALID_MATRIX_TEMPLATE_PARAMETERS);
    PolynomialT poly(PolynomialT::Zero());
    poly[1 - MIN_DEGREE] = ScalarT(1);
    return poly;
  }

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_MATH_H_ */
