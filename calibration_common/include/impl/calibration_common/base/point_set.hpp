/*
 * point_set.hpp
 *
 *  Created on: Mar 27, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_IMPL_POINT_SET_HPP_
#define CALIBRATION_COMMON_IMPL_POINT_SET_HPP_

#include <calibration_common/base/point_set.h>

namespace calibration
{

template <typename ScalarT, int Dimension>
  PointSet<ScalarT, Dimension>::PointSet()
    : points_(), width_(0), height_(0)
  {
  }

template <typename ScalarT, int Dimension>
  PointSet<ScalarT, Dimension>::PointSet(const PointSet<ScalarT, Dimension> & other)
    : points_(other.points_), width_(other.width_), height_(other.height_)
  {
    // Do nothing
  }

template <typename ScalarT, int Dimension>
  PointSet<ScalarT, Dimension>::PointSet(const MatrixT & points)
    : points_(points), width_(points.cols()), height_(points.rows())
  {
  }

template <typename ScalarT, int Dimension>
  PointSet<ScalarT, Dimension>::PointSet(const PointSet & other,
                                         const std::vector<int> & indices)
    : points_(Dimension, indices.size()), width_(indices.size()), height_(1)
  {
    assert(indices.size() <= other.size());
    for (size_t i = 0; i < indices.size(); ++i)
      points_.col(i) = other.points_.col(indices[i]);
  }

template <typename ScalarT, int Dimension>
  PointSet<ScalarT, Dimension>::PointSet(size_t size,
                                         const PointT & value)
    : points_(Dimension, size), width_(size), height_(1)
  {
    for (size_t i = 0; i < points_.cols(); ++i)
      points_.col(i) = value;
  }

template <typename ScalarT, int Dimension>
  PointSet<ScalarT, Dimension>::PointSet(size_t width_or_cols, // TODO swap height and width (cols and rows???)
                                         size_t height_or_rows,
                                         const PointT & value)
    : points_(Dimension, height_or_rows * width_or_cols), width_(width_or_cols), height_(height_or_rows)
  {
    for (size_t i = 0; i < points_.cols(); ++i)
      points_.col(i) = value;
  }

template <typename ScalarT, int Dimension>
  PointSet<ScalarT, Dimension>::~PointSet()
  {
    // Do nothing
  }

template <typename ScalarT, int Dimension>
  inline PointSet<ScalarT, Dimension> & PointSet<ScalarT, Dimension>::operator +=(const PointSet & rhs)
  {
    points_.conservativeResize(Dimension, points_.cols() + rhs.points_.cols());
    points_.rightCols(rhs.points_.cols()) = rhs.points_;
    width_ = static_cast<size_t>(points_.cols());
    height_ = 1;
    return *this;
  }

template <typename ScalarT, int Dimension>
  inline const PointSet<ScalarT, Dimension> PointSet<ScalarT, Dimension>::operator +(const PointSet & rhs)
  {
    return (PointSet(*this) += rhs);
  }

template <typename ScalarT, int Dimension>
  inline const size_t PointSet<ScalarT, Dimension>::width() const
  {
    return width_;
  }

template <typename ScalarT, int Dimension>
  inline const size_t PointSet<ScalarT, Dimension>::height() const
  {
    return height_;
  }

template <typename ScalarT, int Dimension>
  inline const size_t PointSet<ScalarT, Dimension>::rows() const
  {
    return height();
  }

template <typename ScalarT, int Dimension>
  inline const size_t PointSet<ScalarT, Dimension>::cols() const
  {
    return width();
  }

template <typename ScalarT, int Dimension>
  inline const size_t PointSet<ScalarT, Dimension>::size() const
  {
    return width() * height();
  }

template <typename ScalarT, int Dimension>
  inline const typename PointSet<ScalarT, Dimension>::ConstElement PointSet<ScalarT, Dimension>::at(int row,
                                                                                                    int column) const
  {
    if (this->height_ > 1)
      return operator [](row * width() + column);
    else
      throw pcl::IsNotDenseException("Can't use 2D indexing with a unorganized point cloud"); // TODO change exception type
  }

template <typename ScalarT, int Dimension>
  inline typename PointSet<ScalarT, Dimension>::Element PointSet<ScalarT, Dimension>::at(int row,
                                                                                         int column)
  {
    if (this->height_ > 1)
      return operator [](row * width() + column);
    else
      throw pcl::IsNotDenseException("Can't use 2D indexing with a unorganized point cloud"); // TODO change exception type
  }

template <typename ScalarT, int Dimension>
  inline const typename PointSet<ScalarT, Dimension>::ConstElement PointSet<ScalarT, Dimension>::operator ()(size_t row,
                                                                                                             size_t column) const
  {
    return operator [](row * width() + column);
  }

template <typename ScalarT, int Dimension>
  inline typename PointSet<ScalarT, Dimension>::Element PointSet<ScalarT, Dimension>::operator ()(size_t row,
                                                                                                  size_t column)
  {
    return operator [](row * width() + column);
  }

template <typename ScalarT, int Dimension>
  inline bool PointSet<ScalarT, Dimension>::isOrganized() const
  {
    return height() != 1;
  }

template <typename ScalarT, int Dimension>
  inline typename PointSet<ScalarT, Dimension>::Element PointSet<ScalarT, Dimension>::operator [](size_t index)
  {
    return points_.col(index);
  }

template <typename ScalarT, int Dimension>
  inline const typename PointSet<ScalarT, Dimension>::ConstElement PointSet<ScalarT, Dimension>::operator [](size_t index) const
  {
    return points_.col(index);
  }

template <typename ScalarT, int Dimension>
  inline const typename PointSet<ScalarT, Dimension>::MatrixT & PointSet<ScalarT, Dimension>::matrix() const
  {
    return points_;
  }

template <typename ScalarT>
  PointSet2<ScalarT>::PointSet2()
    : Base()
  {
  }

template <typename ScalarT>
  PointSet2<ScalarT>::PointSet2(const PointSet2<ScalarT> & other)
    : Base(other)
  {
    *this = other;
  }

template <typename ScalarT>
  PointSet2<ScalarT>::PointSet2(const typename Base::MatrixT & points)
    : Base(points)
  {
  }

template <typename ScalarT>
  PointSet2<ScalarT>::PointSet2(const PointSet2 & other,
                                const std::vector<int> & indices)
    : Base(other, indices)
  {
  }

template <typename ScalarT>
  PointSet2<ScalarT>::PointSet2(size_t size,
                                const typename Base::PointT & value)
    : Base(size, value)
  {
  }

template <typename ScalarT>
  PointSet2<ScalarT>::PointSet2(size_t width_or_cols, // TODO swap height and width (cols and rows???)
                                size_t height_or_rows,
                                const typename Base::PointT & value)
    : Base(width_or_cols, height_or_rows, value)
  {
  }

template <typename ScalarT>
  PointSet2<ScalarT>::PointSet2(const std::vector<cv::Point_<ScalarT> > & cv_points)
    : Base(cv_points.size())
  {
    for (size_t i = 0; i < cv_points.size(); ++i)
    {
      const cv::Point_<ScalarT> & p = cv_points[i];
      Base::operator [](i) << p.x, p.y;
    }
  }

template <typename ScalarT>
  const typename PointSet2<ScalarT>::PointCloudXYPtr PointSet2<ScalarT>::toPointCloud(const PointSet2<ScalarT> & point_set)
  {
    PointCloudXYPtr cloud = boost::make_shared<PointCloudXY>(point_set.width(), point_set.height());
    for (size_t col = 0; col < point_set.size(); ++col)
    {
      pcl::PointXY & p = cloud->points[col];
      p.x = static_cast<float>(point_set[col][0]);
      p.y = static_cast<float>(point_set[col][1]);
    }
    return cloud;
  }

template <typename ScalarT>
  const typename PointSet2<ScalarT>::PointCloudXYPtr PointSet2<ScalarT>::toPointCloud()
  {
    return toPointCloud(*this);
  }

template <typename ScalarT>
  PointSet3<ScalarT>::PointSet3()
    : Base()
  {
  }

template <typename ScalarT>
  PointSet3<ScalarT>::PointSet3(const PointSet3<ScalarT> & other)
    : Base(other)
  {
    *this = other;
  }

template <typename ScalarT>
  PointSet3<ScalarT>::PointSet3(const typename Base::MatrixT & points)
    : Base(points)
  {
  }

template <typename ScalarT>
  PointSet3<ScalarT>::PointSet3(const PointSet3 & other,
                                const std::vector<int> & indices)
    : Base(other, indices)
  {
  }

template <typename ScalarT>
  PointSet3<ScalarT>::PointSet3(size_t size,
                                const typename Base::PointT & value)
    : Base(size, value)
  {
  }

template <typename ScalarT>
  PointSet3<ScalarT>::PointSet3(size_t width_or_cols, // TODO swap height and width (cols and rows???)
                                size_t height_or_rows,
                                const typename Base::PointT & value)
    : Base(width_or_cols, height_or_rows, value)
  {
  }

template <typename ScalarT>
  PointSet3<ScalarT>::PointSet3(const std::vector<cv::Point3_<ScalarT> > & cv_points)
    : Base(cv_points.size())
  {
    for (size_t i = 0; i < cv_points.size(); ++i)
    {
      const cv::Point3_<ScalarT> & p = cv_points[i];
      Base::operator [](i) << p.x, p.y, p.z;
    }
  }

template <typename ScalarT>
  template <typename PointT>
    PointSet3<ScalarT>::PointSet3(const pcl::PointCloud<PointT> & cloud)
      : Base(cloud.width(), cloud.height())
    {
      for (size_t i = 0; i < cloud->size(); ++i)
      {
        const PointT & p = cloud.points[i];
        Base::operator [](i) << p.x, p.y, p.z;
      }
    }

template <typename ScalarT>
  template <typename PointT>
    PointSet3<ScalarT>::PointSet3(const pcl::PointCloud<PointT> & cloud,
                                  const std::vector<int> & indices,
                                  const Transform3d & transform)
      : Base(indices.size())
    {
      for (size_t i = 0; i < indices.size(); ++i)
      {
        const PointT & p = cloud.points[indices[i]];
        Base::operator [](i) << p.x, p.y, p.z;
      }
      this->transform(transform);
    }

template <typename ScalarT>
  inline void PointSet3<ScalarT>::transform(const Transform3d & transform)
  {
    Base::points_ = transform * Base::points_;
  }

template <typename ScalarT>
  const typename PointSet3<ScalarT>::PointCloudXYZPtr PointSet3<ScalarT>::toPointCloud(const PointSet3<ScalarT> & point_set)
  {
    PointCloudXYZPtr cloud = boost::make_shared<PointCloudXYZ>(point_set.width(), point_set.height());
    for (size_t col = 0; col < point_set.size(); ++col)
    {
      pcl::PointXYZ & p = cloud->points[col];
      p.x = static_cast<float>(point_set[col][0]);
      p.y = static_cast<float>(point_set[col][1]);
      p.z = static_cast<float>(point_set[col][2]);
    }
    return cloud;
  }

template <typename ScalarT>
  const typename PointSet3<ScalarT>::PointCloudXYZPtr PointSet3<ScalarT>::toPointCloud()
  {
    return toPointCloud(*this);
  }

template <typename ScalarT>
  void PointSet3<ScalarT>::toMarker(visualization_msgs::Marker & marker) const
  {
    marker.type = visualization_msgs::Marker::SPHERE_LIST;
    marker.action = visualization_msgs::Marker::ADD;
    marker.scale.x = 0.01;
    marker.scale.y = 0.01;
    marker.scale.z = 0.01;
    marker.color.r = 1.0;
    marker.color.a = 1.0;

    for (int i = 0; i < Base::size(); ++i)
    {
      geometry_msgs::Point p;
      p.x = Base::operator [](i)[0];
      p.y = Base::operator [](i)[1];
      p.z = Base::operator [](i)[2];
      marker.points.push_back(p);
    }

  }

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_IMPL_POINT_SET_HPP_ */
