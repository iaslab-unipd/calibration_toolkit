/*
 * polynomial_multifit.hpp
 *
 *  Created on: Sep 8, 2013
 *      Author: Mauro Antonello
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_IMPL_POLYNOMIAL_MULTIFIT_HPP_
#define CALIBRATION_COMMON_IMPL_POLYNOMIAL_MULTIFIT_HPP_

#include <calibration_common/base/polynomial_multifit.h>

namespace calibration
{

template <typename PolynomialFunctionT>
  PolynomialMultifit<PolynomialFunctionT>::PolynomialMultifit(const PolynomialFunctionPtr & polynomial_function)
    : polynomial_function_(polynomial_function)
  {
    // Do nothing
  }

template <typename PolynomialFunctionT>
  PolynomialMultifit<PolynomialFunctionT>::~PolynomialMultifit()
  {
    // Do nothing
  }

template <typename PolynomialFunctionT>
  void PolynomialMultifit<PolynomialFunctionT>::addData(const double & x,
                                                        const double & y)
  {
    data_bin_.push_back(std::make_pair(x, y));
  }

template <typename PolynomialFunctionT>
  bool PolynomialMultifit<PolynomialFunctionT>::update()
  {
    if (data_bin_.size() < 4 * SIZE)
      return false;

    const size_t bin_size = data_bin_.size();

    gsl_matrix * x = gsl_matrix_alloc(bin_size, SIZE);
    gsl_vector * y = gsl_vector_alloc(bin_size);
    gsl_vector * w = gsl_vector_alloc(bin_size);

    // initialize gsl data matrices.
    for (unsigned int i_pt = 0; i_pt < bin_size; ++i_pt)
    {
      double xi, yi, wi;
      xi = data_bin_[i_pt].first;
      yi = data_bin_[i_pt].second;
      wi = 1.0;

      double xi_tmp = 1.0;
      for (int i = 0; i < MIN_DEGREE; ++i, xi_tmp *= xi);
      for (unsigned int i_dim = 0; i_dim < SIZE; ++i_dim, xi_tmp *= xi)
        gsl_matrix_set(x, i_pt, i_dim, xi_tmp);

      gsl_vector_set(y, i_pt, yi);
      gsl_vector_set(w, i_pt, wi);
    }

    gsl_multifit_linear_workspace * workspace;
    gsl_matrix * cov = gsl_matrix_alloc(SIZE, SIZE);
    gsl_vector * c = gsl_vector_alloc(SIZE);
    double chisq;

    // perform regression
    workspace = gsl_multifit_linear_alloc(bin_size, SIZE);
    gsl_multifit_wlinear(x, w, y, c, cov, &chisq, workspace);
    gsl_multifit_linear_free(workspace);

    // update bin coefficients
    for (unsigned int i_dim = 0; i_dim < SIZE; ++i_dim)
      polynomial_function_->coefficients()[i_dim] = gsl_vector_get(c, i_dim);

    // free resources
    gsl_matrix_free(x);
    gsl_vector_free(y);
    gsl_vector_free(w);
    gsl_vector_free(c);
    gsl_matrix_free(cov);

    return true;
  }

} /* namespace calibration */
#endif /* CALIBRATION_COMMON_IMPL_POLYNOMIAL_MULTIFIT_HPP_ */
