/*
 * color/view.hpp
 *
 *  Created on: Aug 22, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATON_COMMON_IMPL_COLOR_VIEW_HPP_
#define CALIBRATON_COMMON_IMPL_COLOR_VIEW_HPP_

#include <calibration_common/color/view.h>

namespace calibration
{

template <typename ObjectT>
  ColorView<ObjectT>::ColorView(int id,
                                const PointSet2d & interest_points,
                                const ColorSensor::ConstPtr & sensor,
                                const typename ObjectT::ConstPtr & object)
    : id_(id), interest_points_(interest_points), sensor_(sensor), object_(object)
  {
    // Do nothing
  }

template <typename ObjectT>
  ColorView<ObjectT>::~ColorView()
  {
    // Do nothing
  }

template <typename ObjectT>
  const int & ColorView<ObjectT>::id() const
  {
    return id_;
  }

template <typename ObjectT>
  const PointSet2d & ColorView<ObjectT>::interestPoints() const
  {
    return interest_points_;
  }

template <typename ObjectT>
  const typename ObjectT::ConstPtr & ColorView<ObjectT>::object() const
  {
    return object_;
  }

template <typename ObjectT>
  const ColorSensor::ConstPtr & ColorView<ObjectT>::sensor() const
  {
    return sensor_;
  }

} /* namespace calibration_common */
#endif /* CALIBRATON_COMMON_COLOR_VIEW_H_ */
