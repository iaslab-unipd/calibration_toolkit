/*
 * depth/sensor.hpp
 *
 *  Created on: Aug 21, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_COMMON_IMPL_DEPTH_SENSOR_HPP_
#define CALIBRATION_COMMON_IMPL_DEPTH_SENSOR_HPP_

#include <calibration_common/depth/sensor.h>

namespace calibration
{

template <typename PointT>
  DepthSensor<PointT>::DepthSensor(const Type & type,
                                   const DistortionModelConstPtr & distortion_model,
                                   const std::string & frame_id,
                                   const BaseObject::ConstPtr & parent)
    : Sensor(frame_id, parent), type_(type), distortion_model_(distortion_model)
  {
    // Do nothing
  }

template <typename PointT>
  DepthSensor<PointT>::DepthSensor(const DepthSensor & other)
    : Sensor(other), type_(other.type_), distortion_model_(other.distortion_model_)
  {
    // Do nothing
  }

template <typename PointT>
  DepthSensor<PointT>::DepthSensor(const DepthSensor & other,
                                   const Transform3d & transform,
                                   const std::string & new_frame_id,
                                   const BaseObject::ConstPtr & new_parent)
    : Sensor(other, transform, new_frame_id, new_parent), type_(other.type_), distortion_model_(other.distortion_model_)
  {
    // Do nothing
  }

template <typename PointT>
  DepthSensor<PointT>::~DepthSensor()
  {
    // Do nothing
  }

template <typename PointT>
  void DepthSensor<PointT>::undistort(PointT & point) const
  {
    distortion_model_->undistort(point);
  }

template <typename PointT>
  void DepthSensor<PointT>::undistort(pcl::PointCloud<PointT> & cloud) const
  {
    distortion_model_->undistort(cloud);
  }

template <typename PointT>
  const typename DepthSensor<PointT>::DistortionModelConstPtr DepthSensor<PointT>::distortionModel() const
  {
    return distortion_model_;
  }

template <typename PointT>
  void DepthSensor<PointT>::setDistortionModel(const DistortionModelConstPtr & distortion_model)
  {
    distortion_model_ = distortion_model;
  }

}
/* namespace calibration */
#endif /* CALIBRATION_COMMON_SENSOR_H_ */
