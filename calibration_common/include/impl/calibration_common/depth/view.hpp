/*
 * depth/view.hpp
 *
 *  Created on: Aug 22, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATON_COMMON_IMPL_DEPTH_VIEW_HPP_
#define CALIBRATON_COMMON_IMPL_DEPTH_VIEW_HPP_

#include <calibration_common/depth/view.h>

namespace calibration
{

template <typename ObjectT, typename PointT>
  DepthView<ObjectT, PointT>::DepthView(int id,
                                        const pcl::PointCloud<PointT> & data,
                                        const std::vector<int> & indices,
                                        const DepthSensorConstPtr & sensor,
                                        const typename ObjectT::ConstPtr & object)
    : id_(id), interest_points_(data, indices), sensor_(sensor), object_(object)
  {
    // Do nothing
  }

template <typename ObjectT, typename PointT>
  DepthView<ObjectT, PointT>::DepthView(int id,
                                        const PointSet3d & points,
                                        const DepthSensorConstPtr & sensor,
                                        const typename ObjectT::ConstPtr & object)
    : id_(id), interest_points_(points), sensor_(sensor), object_(object)
  {
    // Do nothing
  }

template <typename ObjectT, typename PointT>
  DepthView<ObjectT, PointT>::~DepthView()
  {
    // Do nothing
  }

template <typename ObjectT, typename PointT>
  const int & DepthView<ObjectT, PointT>::id() const
  {
    return id_;
  }

template <typename ObjectT, typename PointT>
  const PointSet3d & DepthView<ObjectT, PointT>::interestPoints() const
  {
    return interest_points_;
  }

template <typename ObjectT, typename PointT>
  const typename ObjectT::ConstPtr & DepthView<ObjectT, PointT>::object() const
  {
    return object_;
  }

template <typename ObjectT, typename PointT>
  const typename DepthSensor<PointT>::ConstPtr & DepthView<ObjectT, PointT>::sensor() const
  {
    return sensor_;
  }

template <typename ObjectT, typename PointT>
  void DepthView<ObjectT, PointT>::toMarker(visualization_msgs::Marker & marker) const
  {
    marker.header.stamp = ros::Time::now();
    marker.header.frame_id = sensor_->frameId();

    marker.type = visualization_msgs::Marker::SPHERE_LIST;
    marker.action = visualization_msgs::Marker::ADD;
    marker.scale.x = 0.01;
    marker.scale.y = 0.01;
    marker.scale.z = 0.01;
    marker.color.r = 1.0;
    marker.color.a = 1.0;

    for (int i = 0; i < interest_points_.size(); ++i)
    {
      geometry_msgs::Point p;
      p.x = interest_points_[i][0];
      p.y = interest_points_[i][1];
      p.z = interest_points_[i][2];
      marker.points.push_back(p);
    }

  }

} /* namespace calibration */
#endif /* CALIBRATON_COMMON_IMPL_DEPTH_VIEW_HPP_ */
