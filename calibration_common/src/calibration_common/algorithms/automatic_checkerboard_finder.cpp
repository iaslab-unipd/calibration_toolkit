/*
 * automatic_checkerboard_finder.cpp
 *
 *  Created on: Mar 28, 2013
 *      Author: Filippo Basso
 */

#include <calibration_common/algorithms/automatic_checkerboard_finder.h>

namespace calibration
{

AutomaticCheckerboardFinder::AutomaticCheckerboardFinder()
  : CheckerboardFinder()
{
  // Do nothing
}

AutomaticCheckerboardFinder::AutomaticCheckerboardFinder(const cv::Mat &image)
  : CheckerboardFinder(image)
{
  // Do nothing
}

AutomaticCheckerboardFinder::~AutomaticCheckerboardFinder()
{
  // Do nothing
}

bool AutomaticCheckerboardFinder::find(const Checkerboard & checkerboard,
                                       std::vector<cv::Point2d> & corners) const
{
  cv::Size pattern_size(checkerboard.cols(), checkerboard.rows());
  std::vector<cv::Point2f> corners_float;

  bool pattern_found = cv::findChessboardCorners(gray_, pattern_size, corners_float);

  if (pattern_found)
    cv::cornerSubPix(gray_, corners_float, cv::Size(2, 2), cv::Size(-1, -1),
                     cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 100, 0.01));

  corners.clear();
  for (unsigned int i = 0; i < corners_float.size(); ++i)
    corners.push_back(cv::Point2d(corners_float[i].x, corners_float[i].y));

  return pattern_found;
}

} /* namespace calibration */
