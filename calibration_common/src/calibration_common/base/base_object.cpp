/*
 * base_object.cpp
 *
 *  Created on: Feb 1, 2013
 *      Author: Filippo Basso
 */

#include <calibration_common/base/base_object.h>
#include <eigen_conversions/eigen_msg.h>

namespace calibration
{

BaseObject::BaseObject(const std::string & frame_id,
                       const ConstPtr & parent)
  : pose_(Pose3d::Identity()), frame_id_(frame_id), parent_(parent)
{
  // Do nothing
}

BaseObject::BaseObject(const BaseObject & other)
  : pose_(other.pose_), frame_id_(other.frame_id_), parent_(other.parent_)
{
  // Do nothing
}

BaseObject::BaseObject(const BaseObject & other,
                       const Transform3d & transform,
                       const std::string & new_frame_id,
                       const ConstPtr & new_parent)
  : pose_(transform * other.pose_), frame_id_(new_frame_id), parent_(new_parent)
{
  // Do nothing
}

BaseObject::~BaseObject()
{
  // Do nothing
}

void BaseObject::transform(const Transform3d & transform)
{
  pose_ = transform * pose_;
}

const Pose3d & BaseObject::pose() const
{
  return pose_;
}

const std::string & BaseObject::frameId() const
{
  return frame_id_;
}

const BaseObject::ConstPtr & BaseObject::parent() const
{
  return parent_;
}

bool BaseObject::toTF(geometry_msgs::TransformStamped & transform_msg) const
{
  if (not parent())
    return false;

  transform_msg.header.frame_id = parent()->frameId();
  transform_msg.header.stamp = ros::Time::now();
  transform_msg.child_frame_id = frameId();

  tf::transformEigenToMsg(pose(), transform_msg.transform);
  return true;
}

} /* namespace calibration */
