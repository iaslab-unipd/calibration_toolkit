/*
 * checkerboard.cpp
 *
 *  Created on: Jul 16, 2013
 *      Author: Filippo Basso
 */

#include <calibration_common/base/checkerboard.h>
#include <calibration_common/color/view.h>
#include <eigen_conversions/eigen_msg.h>

namespace calibration
{

Checkerboard::Checkerboard(int rows,
                           int cols,
                           double cell_width,
                           double cell_height,
                           const std::string & frame_id,
                           const BaseObject::ConstPtr & parent)
  : PlanarObject(PLANE_XY, frame_id, parent), Pattern(corners(rows, cols, cell_width, cell_height)), rows_(rows),
    cols_(cols), cell_width_(cell_width), cell_height_(cell_height)
{
  // Do nothing
}

Checkerboard::Checkerboard(const Checkerboard & other)
  : PlanarObject(other), Pattern(other), rows_(other.rows_), cols_(other.cols_), cell_width_(other.cell_width_),
    cell_height_(other.cell_height_)
{
  // Do nothing
}

Checkerboard::Checkerboard(const Checkerboard & other,
                           const Transform3d & transform,
                           const std::string & new_frame_id,
                           const BaseObject::ConstPtr & new_parent)
  : PlanarObject(other, transform, new_frame_id, new_parent), Pattern(other, transform), rows_(other.rows_),
    cols_(other.cols_), cell_width_(other.cell_width_), cell_height_(other.cell_height_)
{
  // Do nothing
}

Checkerboard::Checkerboard(const ColorView<Checkerboard> & view)
  : PlanarObject(*view.object()), Pattern(*view.object()), rows_(view.object()->rows()), cols_(view.object()->cols()),
    cell_width_(view.object()->cellWidth()), cell_height_(view.object()->cellHeight())
{
  parent_ = view.sensor();
  std::stringstream ss;
  ss << view.object()->frameId() << "_" << view.id();
  frame_id_ = ss.str();
  transform(view.sensor()->estimatePose(view.interestPoints(), view.object()->points()));
}

Checkerboard::~Checkerboard()
{
  // Do nothing
}

void Checkerboard::transform(const Transform3d & transform)
{
  PlanarObject::transform(transform);
  Pattern::transform(transform);
}

const PointSet3d::ConstElement Checkerboard::operator ()(int row,
                                                         int col) const
{
  return operator [](row * cols() + col);
}

const PointSet3d::ConstElement Checkerboard::at(int row,
                                                int col) const
{
  assert(row < rows());
  assert(col < cols());
  return operator ()(row, col);
}

Point3d Checkerboard::center() const
{
  return Point3d(((*this)(0, 0) + (*this)(rows() - 1, cols() - 1)) / 2);
}

const PointSet3d & Checkerboard::corners() const
{
  return points();
}

int Checkerboard::rows() const
{
  return rows_;
}

int Checkerboard::cols() const
{
  return cols_;
}

double Checkerboard::cellWidth() const
{
  return cell_width_;
}

double Checkerboard::cellHeight() const
{
  return cell_height_;
}

double Checkerboard::area() const
{
  return width() * height();
}

double Checkerboard::width() const
{
  return cellWidth() * (cols() + 1);
}

double Checkerboard::height() const
{
  return cellHeight() * (rows() + 1);
}

PointSet3d Checkerboard::corners(int rows,
                                 int cols,
                                 double cell_width,
                                 double cell_height)
{
  assert(rows > 0);
  assert(cols > 0);
  assert(cell_width > 0);
  assert(cell_height > 0);
  PointSet3d corners(rows, cols);
  for (int i = 0; i < rows * cols; ++i)
    corners[i] << (i / cols) * cell_height, (i % cols) * cell_width, 0.0;

  return corners;
}

void Checkerboard::toMarker(visualization_msgs::Marker & marker) const
{
  if (not parent())
    return;

  marker.header.stamp = ros::Time::now();
  marker.header.frame_id = parent()->frameId();

  marker.type = visualization_msgs::Marker::CUBE_LIST;
  marker.action = visualization_msgs::Marker::ADD;
  marker.scale.z = 0.01;

  marker.scale.x = cellHeight();
  marker.scale.y = cellWidth();

  std_msgs::ColorRGBA black;
  black.a = 1.0;
  std_msgs::ColorRGBA white;
  white.b = white.g = white.r = white.a = 1.0;

  Eigen::Vector3d delta = ((*this)(1, 1) - (*this)(0, 0)) / 2.0;
  Eigen::Vector3d delta2 = ((*this)(0, 1) - (*this)(1, 0)) / 2.0;
  PointSet3d centers(rows() + 1, cols() + 1);

  int index = 0;
  for (int i = 0; i < rows(); ++i)
  {
    for (int j = 0; j < cols(); ++j)
      centers[index++] = (*this)(i, j) - delta;
    centers[index++] = (*this)(i, cols() - 1) + delta2;
  }
  centers[index++] = (*this)(rows() - 1, 0) - delta2;
  for (int i = 0; i < cols(); ++i)
    centers[index++] = (*this)(rows() - 1, i) + delta;

  centers.transform(pose().inverse());

  for (int i = 0; i < centers.size(); ++i)
  {
    geometry_msgs::Point p;
    tf::pointEigenToMsg(centers[i], p);
    marker.points.push_back(p);
    if (cols() % 2)
      marker.colors.push_back((i / (cols() + 1)) % 2 ? (i % 2 ? white : black) : (i % 2 ? black : white));
    else
      marker.colors.push_back(i % 2 ? white : black);
  }
  tf::poseEigenToMsg(pose(), marker.pose);
}

} /* namespace calibration */

