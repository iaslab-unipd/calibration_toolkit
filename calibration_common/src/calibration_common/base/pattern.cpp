/*
 * pattern.cpp
 *
 *  Created on: Sep 3, 2013
 *      Author: Filippo Basso
 */

#include <calibration_common/base/pattern.h>

namespace calibration
{

Pattern::Pattern(const PointSet3d & points)
  : points_(points)
{
  // Do nothing
}

Pattern::Pattern(const Pattern & other)
  : points_(other.points_)
{
  // Do nothing
}

Pattern::Pattern(const Pattern & other,
                 const Transform3d & transform)
  : points_(other.points_)
{
  points_.transform(transform);
}

Pattern::~Pattern()
{
  // Do nothing
}

int Pattern::size() const
{
  return points_.size();
}

const PointSet3d::ConstElement Pattern::operator [](int index) const
{
  return points_[index];
}

const PointSet3d &
Pattern::points() const
{
  return points_;
}

void Pattern::transform(const Transform3d & transform)
{
  points_.transform(transform);
}

} /* namespace calibration */
