/*
 * planar_object.cpp
 *
 *  Created on: Aug 22, 2013
 *      Author: Filippo Basso
 */

#include <calibration_common/base/planar_object.h>
#include <pcl/common/eigen.h>
#include <eigen_conversions/eigen_msg.h>


namespace calibration
{

PlanarObject::PlanarObject(const Plane3d & plane,
                           const std::string & frame_id,
                           const BaseObject::ConstPtr & parent)
  : BaseObject(frame_id, parent), plane_(plane)
{
  //BaseObject::pose_ = Util::plane3dTransform(PLANE_XY, plane);
}

PlanarObject::PlanarObject(const PlanarObject & other)
  : BaseObject(other), plane_(other.plane_)
{
  // Do nothing
}

PlanarObject::PlanarObject(const PlanarObject & other,
                           const Transform3d & transform,
                           const std::string & new_frame_id,
                           const BaseObject::ConstPtr & new_parent)
  : BaseObject(other, transform, new_frame_id, new_parent), plane_(other.plane_)
{
  plane_.transform(transform);
}

//PlanarObject::PlanarObject(const ColorView<PlanarObject> & view)
//  : BaseObject(view), plane_(view.object()->plane())
//{
//  plane_.transform(pose());
//}

PlanarObject::PlanarObject(const PointSet3d & inliers,
                           const std::string & frame_id,
                           const BaseObject::ConstPtr & parent)
  : BaseObject(frame_id, parent)
{
  plane_ = fitPlane(inliers);
  BaseObject::transform(Util::plane3dTransform(PLANE_XY, plane_));
}

PlanarObject::~PlanarObject()
{
  // Do nothing
}

const Plane3d & PlanarObject::plane() const
{
  return plane_;
}

void PlanarObject::toMarker(visualization_msgs::Marker & marker) const
{
  marker.header.stamp = ros::Time::now();
  marker.header.frame_id = parent()->frameId();

  marker.type = visualization_msgs::Marker::CUBE;
  marker.action = visualization_msgs::Marker::ADD;
  marker.scale.x = 2;
  marker.scale.y = 2;
  marker.scale.z = 0.005;
  marker.color.b = 1.0;
  marker.color.a = 0.5;

  tf::poseEigenToMsg(pose(), marker.pose);
}

void PlanarObject::transform(const Transform3d & transform)
{
  plane_.transform(transform);
  BaseObject::transform(transform);
}

Plane3d PlanarObject::fitPlane(const PointSet3d & points)
{
  Eigen::Matrix3d covariance_matrix;
  Point3d centroid;

  computeMeanAndCovarianceMatrix(points, covariance_matrix, centroid);

  double eigen_value;
  Point3d eigen_vector;
  pcl::eigen33(covariance_matrix, eigen_value, eigen_vector);

  return Plane3d(eigen_vector, -eigen_vector.dot(centroid));
}

void PlanarObject::computeMeanAndCovarianceMatrix(const PointSet3d & points,
                                                  Eigen::Matrix3d & covariance_matrix,
                                                  Point3d & centroid)
{
  Eigen::Matrix<double, 9, 1> accu = Eigen::Matrix<double, 9, 1>::Zero();
  size_t point_count;
//  if (cloud.is_dense)
//  {
  point_count = points.size();
  for (size_t i = 0; i < point_count; ++i)
  {
    accu[0] += points[i][0] * points[i][0];
    accu[1] += points[i][0] * points[i][1];
    accu[2] += points[i][0] * points[i][2];
    accu[3] += points[i][1] * points[i][1];
    accu[4] += points[i][1] * points[i][2];
    accu[5] += points[i][2] * points[i][2];
    accu[6] += points[i][0];
    accu[7] += points[i][1];
    accu[8] += points[i][2];
  }
//  }

  accu /= static_cast<double>(point_count);
  if (point_count != 0)
  {
    centroid = accu.tail<3>();   // -- does not compile with Clang 3.0
//    centroid[0] = accu[6]; centroid[1] = accu[7]; centroid[2] = accu[8];
//    centroid[3] = 0;
    covariance_matrix.coeffRef(0) = accu[0] - accu[6] * accu[6];
    covariance_matrix.coeffRef(1) = accu[1] - accu[6] * accu[7];
    covariance_matrix.coeffRef(2) = accu[2] - accu[6] * accu[8];
    covariance_matrix.coeffRef(4) = accu[3] - accu[7] * accu[7];
    covariance_matrix.coeffRef(5) = accu[4] - accu[7] * accu[8];
    covariance_matrix.coeffRef(8) = accu[5] - accu[8] * accu[8];
    covariance_matrix.coeffRef(3) = covariance_matrix.coeff(1);
    covariance_matrix.coeffRef(6) = covariance_matrix.coeff(2);
    covariance_matrix.coeffRef(7) = covariance_matrix.coeff(5);
  }

}

}
/* namespace calibration */
