/*
 * sensor.cpp
 *
 *  Created on: Jun 14, 2013
 *      Author: Filippo Basso
 */

#include <calibration_common/base/sensor.h>

namespace calibration
{

Sensor::Sensor(const std::string & frame_id,
               const BaseObject::ConstPtr & parent)
  : BaseObject(frame_id, parent)
{
  // Do nothing
}

Sensor::Sensor(const Sensor & other)
  : BaseObject(other)
{
  // Do nothing
}

Sensor::Sensor(const Sensor & other,
               const Transform3d & transform,
               const std::string & new_frame_id,
               const BaseObject::ConstPtr & new_parent)
  : BaseObject(other, transform, new_frame_id, new_parent)
{
  // Do nothing
}

Sensor::~Sensor()
{
  // Do nothing
}

void Sensor::setPose(const Pose3d & pose)
{
  BaseObject::pose_ = pose;
}

}
/* namespace calibration */
