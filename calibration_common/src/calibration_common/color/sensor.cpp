/*
 * color/sensor.cpp
 *
 *  Created on: Jun 14, 2013
 *      Author: Filippo Basso
 */

#include <calibration_common/color/sensor.h>

namespace calibration
{

ColorSensor::ColorSensor(const Type & type,
                         const ColorCameraModel::ConstPtr & camera_model,
                         const PoseEstimator::ConstPtr & pose_estimator,
                         const std::string & frame_id,
                         const BaseObject::ConstPtr & parent)
  : Sensor(frame_id, parent), type_(type), camera_model_(camera_model), pose_estimator_(pose_estimator)
{
  // Do nothing
}

ColorSensor::ColorSensor(const ColorSensor & other)
  : Sensor(other), type_(other.type_), camera_model_(other.camera_model_), pose_estimator_(other.pose_estimator_)
{
  // Do nothing
}

ColorSensor::ColorSensor(const ColorSensor & other,
                         const Transform3d & transform,
                         const std::string & new_frame_id,
                         const BaseObject::ConstPtr & new_parent)
  : Sensor(other, transform, new_frame_id, new_parent), type_(other.type_), camera_model_(other.camera_model_),
    pose_estimator_(other.pose_estimator_)
{
  // Do nothing
}

ColorSensor::~ColorSensor()
{
  // Do nothing
}

Pose3d ColorSensor::estimatePose(const PointSet2d & points_image,
                                 const PointSet3d & points_object) const
{
  return pose_estimator_->estimatePose(points_image, points_object);
}

const ColorCameraModel::ConstPtr & ColorSensor::cameraModel() const
{
  return camera_model_;
}

const PoseEstimator::ConstPtr & ColorSensor::poseEstimator() const
{
  return pose_estimator_;
}

}
/* namespace calibration */
