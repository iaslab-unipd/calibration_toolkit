/*
 * pinhole/camera_model.cpp
 *
 *  Created on: Oct 18, 2012
 *      Author: Filippo Basso
 */

#include <ros/ros.h>
#include <calibration_common/pinhole/camera_model.h>
#include <image_geometry/pinhole_camera_model.h>
#include <opencv2/core/eigen.hpp>

namespace calibration
{

PinholeCameraModel::PinholeCameraModel()
  : ColorCameraModel(), Base()
{

}

PinholeCameraModel::PinholeCameraModel(const sensor_msgs::CameraInfo& msg)
  : ColorCameraModel(), Base()
{
  Base::fromCameraInfo(msg);
}

PinholeCameraModel::PinholeCameraModel(const PinholeCameraModel& other)
  : ColorCameraModel(), Base(other)
{
  // Do nothing
}

Point3d PinholeCameraModel::projectPixelTo3dRay(const Point2d & pixel_point) const
{
  cv::Point3d cv_point = Base::projectPixelTo3dRay(cv::Point2d(pixel_point[0], pixel_point[1]));
  return Eigen::Vector3d(cv_point.x, cv_point.y, cv_point.z).normalized();
}

Point2d PinholeCameraModel::project3dToPixel(const Point3d & world_point) const
{
  cv::Point2d cv_point = Base::project3dToPixel(cv::Point3d(world_point[0], world_point[1], world_point[2]));
  return Eigen::Vector2d(cv_point.x, cv_point.y);
}

PointSet3d PinholeCameraModel::projectPixelTo3dRay(const PointSet2d & pixel_points) const
{
  PointSet3d ret_matrix(pixel_points.width(), pixel_points.height());
  for (int i = 0; i < pixel_points.size(); ++i)
  {
    ret_matrix[i] = projectPixelTo3dRay(Point2d(pixel_points[i]));
  }
  return ret_matrix;
}

PointSet2d PinholeCameraModel::project3dToPixel(const PointSet3d & world_points) const
{
  PointSet2d ret_matrix(world_points.width(), world_points.height());
  for (int i = 0; i < world_points.size(); ++i)
  {
    ret_matrix[i] = project3dToPixel(Point3d(world_points[i]));
  }
  return ret_matrix;
}

} /* namespace calibration */
