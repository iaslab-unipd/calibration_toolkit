/*
 * calibration_common.cpp
 *
 *  Created on: Feb 5, 2013
 *      Author: Filippo Basso
 */

#include <calibration_common/objects/kinect_distortion_map_multifit.h>
#include <gtest/gtest.h>

#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>

using namespace calibration_common;

TEST(KinectPolynomialDistortionMap, at)
{
  KinectPolynomialDistortionMap<double, pcl::PointXYZ, 2> distorsion_map(10, 10, M_PI / 2.0, M_PI / 4.0);
  int row, col;
  distorsion_map.at(pcl::PointXYZ(0.0, 0.0, 1.0), row, col);
  EXPECT_EQ(row, 5);
  EXPECT_EQ(col, 5);
}

TEST(KinectDistortionMapMultifit, KinectDistortionMapMultifit)
{
  typedef KinectPolynomialDistortionMap<double, pcl::PointXYZ, 2> DistortionMapT;
  DistortionMapT::Ptr distorsion_map(boost::make_shared<DistortionMapT>(10, 10, M_PI / 2.0, M_PI / 4.0));
  KinectDistortionMapMultifit<double, pcl::PointXYZ, 2> multifit(distorsion_map);
  SUCCEED();
}

TEST(KinectDistortionMapMultifit, updateDistortionMap)
{
  typedef KinectPolynomialDistortionMap<double, pcl::PointXYZ, 2> DistortionMapT;
  DistortionMapT::Ptr distorsion_map(boost::make_shared<DistortionMapT>(10, 10, M_PI / 2.0, M_PI / 4.0));
  KinectDistortionMapMultifit<double, pcl::PointXYZ, 2> multifit(distorsion_map);
  // applied distortion: z_d = c3 * z_p*z_p + c2 * z_p + c1
  double c0 = 1.0;
  double c1 = -2.0;
  double c2 = 1.0;
  // gaussian noise on the detected point, linear with respect to the z coordinate.
  boost::mt19937 rnd_gen;
  boost::normal_distribution<double> norm_dist(0.0, 1e-4);
  boost::variate_generator<boost::mt19937&, boost::normal_distribution<double> > normal_gen(rnd_gen, norm_dist);
  // update ignore bins with less than 2*Dimension (6) points.
  for (double z = 1.0; z <= 10.0; z++)
    for (int n = 0; n < 10; n++)
      multifit.addPoint(pcl::PointXYZ(0.0, 0.0, z),
                        Plane3d(-Eigen::Vector3d::UnitZ(), c2 * z * z + (c1 + normal_gen()) * z + c0));
  multifit.updateDistortionMap();

  int row, col;
  distorsion_map->at(pcl::PointXYZ(0.0, 0.0, 1.0), row, col);
  EXPECT_NEAR(distorsion_map->at(row, col)[0], c0, 1e-2);
  EXPECT_NEAR(distorsion_map->at(row, col)[1], c1, 1e-2);
  EXPECT_NEAR(distorsion_map->at(row, col)[2], c2, 1e-2);
}

TEST(KinectDistortionMapMultifit, accumulatePoint)
{
  typedef KinectPolynomialDistortionMap<double, pcl::PointXYZ, 2> DistortionMapT;

  double c0 = 1.0;
  double c1 = -2.0;
  double c2 = 1.0;

  boost::mt19937 rnd_gen;
  boost::normal_distribution<double> norm_dist(0.0, 1e-4);
  boost::variate_generator<boost::mt19937&, boost::normal_distribution<double> > normal_gen(rnd_gen, norm_dist);

  DistortionMapT::Ptr distorsion_map(boost::make_shared<DistortionMapT>(10, 10, M_PI / 2.0, M_PI / 4.0));
  KinectDistortionMapMultifit<double, pcl::PointXYZ, 2> multifit(distorsion_map);

  DistortionMapT::Ptr distorsion_map2(boost::make_shared<DistortionMapT>(10, 10, M_PI / 2.0, M_PI / 4.0));
  KinectDistortionMapMultifit<double, pcl::PointXYZ, 2> multifit2(distorsion_map2);

  for (double z = 1.0; z <= 10.0; z++)
  {
    for (int n = 0; n < 10; n++)
    {
      Plane3d plane(-Eigen::Vector3d::UnitZ(), c2 * z * z + (c1 + normal_gen()) * z + c0);
      multifit.addPoint(pcl::PointXYZ(0.0, 0.0, z), plane);
      multifit2.accumulatePoint(pcl::PointXYZ(0.0, 0.0, z));
      multifit2.addAccumulatedPoints(plane);
    }
  }
  multifit.updateDistortionMap();
  multifit2.updateDistortionMap();

  EXPECT_EQ(distorsion_map->at(0, 0)[0], distorsion_map2->at(0, 0)[0]);
  EXPECT_EQ(distorsion_map->at(0, 0)[1], distorsion_map2->at(0, 0)[1]);
  EXPECT_EQ(distorsion_map->at(0, 0)[2], distorsion_map2->at(0, 0)[2]);
}

int main(int argc,
         char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
