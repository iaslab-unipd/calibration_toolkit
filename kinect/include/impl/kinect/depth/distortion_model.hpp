/*
 * kinect/depth/distortion_model.hpp
 *
 *  Created on: Sep 8, 2013
 *      Author: Filippo Basso
 */

#ifndef KINECT_IMPL_DEPTH_DISTORTION_MODEL_HPP_
#define KINECT_IMPL_DEPTH_DISTORTION_MODEL_HPP_

#include <kinect/depth/distortion_model.h>

namespace calibration
{

template <typename PointT, typename MathFunctionT>
  KinectDepthDistortionModel<PointT, MathFunctionT>::KinectDepthDistortionModel(const UndistortionMapConstPtr & undistortion_map,
                                                                                const MathFunctionConstPtr & error_function)
    : undistortion_map_(undistortion_map), error_function_(error_function)
  {
    // Do nothing
  }

template <typename PointT, typename MathFunctionT>
  KinectDepthDistortionModel<PointT, MathFunctionT>::~KinectDepthDistortionModel()
  {
    // Do nothing
  }

template <typename PointT, typename MathFunctionT>
  void KinectDepthDistortionModel<PointT, MathFunctionT>::undistort(pcl::PointCloud<PointT> & cloud) const
  {
    for (size_t i = 0; i < cloud.size(); ++i)
    {
      undistortion_map_->undistort(cloud.points[i]);
      double k = error_function_->evaluate(cloud.points[i].z) / cloud.points[i].z;

      cloud.points[i].x *= k;
      cloud.points[i].y *= k;
      cloud.points[i].z *= k;
    }
  }

} /* namespace calibration */
#endif /* KINECT_IMPL_DEPTH_DISTORTION_MODEL_HPP_ */
