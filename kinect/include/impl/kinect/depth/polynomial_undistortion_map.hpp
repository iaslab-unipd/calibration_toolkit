/*
 * kinect/depth/polynomial_undistortion_map.h
 *
 *  Created on: Sep 3, 2013
 *      Author: Mauro Antonello
 *      Author: Filippo Basso
 */

#ifndef KINECT_IMPL_DEPTH_POLYNOMIAL_UNDISTORTION_MAP_HPP_
#define KINECT_IMPL_DEPTH_POLYNOMIAL_UNDISTORTION_MAP_HPP_

#include <kinect/depth/polynomial_undistortion_map.h>

namespace calibration
{

template <typename ScalarT, typename PointT, typename PolyFunT>
  KinectPolynomialUndistortionMap<ScalarT, PointT, PolyFunT>::KinectPolynomialUndistortionMap(const size_t & rows,
                                                                                              const size_t & cols,
                                                                                              const ScalarT & fov_width,
                                                                                              const ScalarT & fov_height,
                                                                                              const Eigen::Vector3d & central_ray,
                                                                                              const Polynomial & value)
    : Base(rows, cols, fov_width, fov_height, central_ray, value)
  {
    // Do nothing
  }

template <typename ScalarT, typename PointT, typename PolyFunT>
  void KinectPolynomialUndistortionMap<ScalarT, PointT, PolyFunT>::undistort(PointT & point,
                                                                             const ConstElement data) const
  {
    PolyFunT poly(data);
    double k = poly.evaluate(point.z) / point.z;
    point.x *= k;
    point.y *= k;
    point.z *= k;
  }

} /* namespace calibration */

#endif /* KINECT_IMPL_DEPTH_POLYNOMIAL_UNDISTORTION_MAP_HPP_ */
