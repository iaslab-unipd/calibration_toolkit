/*
 * kinect/depth/polynomial_undistortion_map_multifit.hpp
 *
 *  Created on: Sep 3, 2013
 *      Author: Mauro Antonello
 *      Author: Filippo Basso
 */

#ifndef KINECT_IMPL_DEPTH_POLYNOMIAL_UNDISTORTION_MAP_MULTIFIT_HPP_
#define KINECT_IMPL_DEPTH_POLYNOMIAL_UNDISTORTION_MAP_MULTIFIT_HPP_

#include <kinect/depth/polynomial_undistortion_map_multifit.h>
#include <gsl/gsl_multifit.h>

namespace calibration
{

template <typename ScalarT, typename PointT, typename PolyFunT>
  KinectPolynomialUndistortionMapMultifit<ScalarT, PointT, PolyFunT>::KinectPolynomialUndistortionMapMultifit(const UndistortionMapPtr & distortion_map)
    : distortion_map_(distortion_map), distorsion_bins_(distortion_map->size()),
      accumulation_bins_(distortion_map->size())
  {
    // Do nothing
  }

template <typename ScalarT, typename PointT, typename PolyFunT>
  KinectPolynomialUndistortionMapMultifit<ScalarT, PointT, PolyFunT>::~KinectPolynomialUndistortionMapMultifit()
  {
    // Do nothing
  }

template <typename ScalarT, typename PointT, typename PolyFunT>
  const typename UndistortionMap<double, PointT>::ConstPtr KinectPolynomialUndistortionMapMultifit<ScalarT, PointT,
    PolyFunT>::undistortionMap() const
  {
    return typename UndistortionMap<double, PointT>::ConstPtr(distortion_map_);
  }

template <typename ScalarT, typename PointT, typename PolyFunT>
  void KinectPolynomialUndistortionMapMultifit<ScalarT, PointT, PolyFunT>::accumulateCloud(const PointCloudConstPtr & cloud,
                                                                                           const std::vector<int> & indices)
  {
    for (size_t i = 0; i < indices.size(); ++i)
      accumulatePoint(cloud->points[indices[i]]);
  }

template <typename ScalarT, typename PointT, typename PolyFunT>
  void KinectPolynomialUndistortionMapMultifit<ScalarT, PointT, PolyFunT>::accumulatePoint(const PointT & point)
  {
    if (not pcl::isFinite(point))
      return;

    int row, col;
    distortion_map_->at(point, row, col);
    accumulation_bins_[row * distortion_map_->width() + col] += Point3d(point.x, point.y, point.z);

  }

template <typename ScalarT, typename PointT, typename PolyFunT>
  void KinectPolynomialUndistortionMapMultifit<ScalarT, PointT, PolyFunT>::addAccumulatedPoints(const Plane3d & plane)
  {
    for (size_t i = 0; i < accumulation_bins_.size(); ++i)
    {
      if (accumulation_bins_[i].isEmpty())
        continue;

      Point3d pt = accumulation_bins_[i].average();
      Line3d ray(pt, Point3d::UnitZ());
      Point3d plane_pt = pt + ray.direction() * ray.intersection(plane); // FIXME use intersectionPoint when available

//      if (i % 960 == 0)
//      {
//        std::cerr << i << ", " << double(pt[2]) << ", " << double(plane_pt[2]) << ";" << std::endl;
//      }

      distorsion_bins_[i].push_back(std::make_pair(double(pt[2]), double(plane_pt[2])));
      accumulation_bins_[i].reset();
    }
  }

template <typename ScalarT, typename PointT, typename PolyFunT>
  void KinectPolynomialUndistortionMapMultifit<ScalarT, PointT, PolyFunT>::addPoint(const PointT & point,
                                                                                    const Plane3d & plane)
  {
    if (not pcl::isFinite(point))
      return;

    Point3d pt(point.x, point.y, point.z);
    Line3d ray(pt, Point3d::UnitZ());
    Point3d plane_pt = pt + ray.direction() * ray.intersection(plane); // FIXME use intersectionPoint when available

    int row, col;
    distortion_map_->at(point, row, col);
    distorsion_bins_[row * distortion_map_->width() + col].push_back(std::make_pair(pt[2], plane_pt[2]));
  }

template <typename ScalarT, typename PointT, typename PolyFunT>
  void KinectPolynomialUndistortionMapMultifit<ScalarT, PointT, PolyFunT>::update()
  {
    for (unsigned int i_row = 0, i_bin = 0; i_row < distortion_map_->height(); ++i_row)
    {
      for (unsigned int i_col = 0; i_col < distortion_map_->width(); ++i_col, ++i_bin)
      {
        // skip the bin if there are not enough point for a "good" fitting.
        if (distorsion_bins_[i_bin].size() < 5 * DEGREE)
          continue;

        const size_t bin_size = distorsion_bins_[i_bin].size();
        const PointDistorsionBin & distorsion_bin = distorsion_bins_[i_bin];

        gsl_matrix * x = gsl_matrix_alloc(bin_size, SIZE);
        gsl_vector * y = gsl_vector_alloc(bin_size);
        gsl_vector * w = gsl_vector_alloc(bin_size);

        // initialize gsl data matrices.
        for (unsigned int i_pt = 0; i_pt < bin_size; ++i_pt)
        {
          double xi, yi, wi;
          xi = distorsion_bin[i_pt].first;
          yi = distorsion_bin[i_pt].second;
          wi = 1.0;

          double xi_tmp = 1.0;
          for (int i = 0; i < MIN_DEGREE; ++i, xi_tmp *= xi)
            ;
          for (unsigned int i_dim = 0; i_dim < SIZE; ++i_dim, xi_tmp *= xi)
            gsl_matrix_set(x, i_pt, i_dim, xi_tmp);

          gsl_vector_set(y, i_pt, yi);
          gsl_vector_set(w, i_pt, wi);
        }

        gsl_multifit_linear_workspace * workspace;
        gsl_matrix * cov = gsl_matrix_alloc(SIZE, SIZE);
        gsl_vector * c = gsl_vector_alloc(SIZE);
        double chisq;

        // perform regression
        workspace = gsl_multifit_linear_alloc(bin_size, SIZE);
        gsl_multifit_wlinear(x, w, y, c, cov, &chisq, workspace);
        gsl_multifit_linear_free(workspace);

        // update bin coefficients
        for (unsigned int i_dim = 0; i_dim < SIZE; ++i_dim)
          (*distortion_map_)(i_row, i_col)[i_dim] = gsl_vector_get(c, i_dim);

        // free resources
        gsl_matrix_free(x);
        gsl_vector_free(y);
        gsl_vector_free(w);
        gsl_vector_free(c);
        gsl_matrix_free(cov);
      }
    }
  }

} /* namespace calibration */
#endif /* KINECT_IMPL_DEPTH_POLYNOMIAL_UNDISTORTION_MAP_MULTIFIT_HPP_ */
