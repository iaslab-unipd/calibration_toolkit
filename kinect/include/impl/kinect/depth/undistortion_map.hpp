/*
 * kinect/depth/undistortion_map.hpp
 *
 *  Created on: Sep 3, 2013
 *      Author: Filippo Basso
 */

#ifndef KINECT_IMPL_DEPTH_UNDISTORTION_MAP_HPP_
#define KINECT_IMPL_DEPTH_UNDISTORTION_MAP_HPP_

#include <kinect/depth/undistortion_map.h>

namespace calibration
{

template <typename ScalarT, typename PointT, int Size>
  KinectUndistortionMap<ScalarT, PointT, Size>::KinectUndistortionMap(const size_t & rows,
                                                                      const size_t & cols,
                                                                      const ScalarT & fov_width,
                                                                      const ScalarT & fov_height,
                                                                      const Eigen::Vector3d & central_ray,
                                                                      const Data & value)
    : Base(cols, rows, fov_width, fov_height, toSphere(central_ray) - PointSphere(fov_width / 2, fov_height / 2), value)
  {
    // Do nothing
  }
template <typename ScalarT, typename PointT, int Size>
  KinectUndistortionMap<ScalarT, PointT, Size>::KinectUndistortionMap(const size_t & rows,
                                                                      const size_t & cols,
                                                                      const KinectUndistortionMap & other)
    : Base(cols, rows, other)
  {
    // Do nothing
  }

template <typename ScalarT, typename PointT, int Size>
  KinectUndistortionMap<ScalarT, PointT, Size>::~KinectUndistortionMap()
  {
    // Do nothing
  }

template <typename ScalarT, typename PointT, int Size>
  PointT KinectUndistortionMap<ScalarT, PointT, Size>::toPolar(const PointT & ray) const
  {
    ScalarT p(std::sqrt(ray.x * ray.x + ray.y * ray.y + ray.z * ray.z));
    PointT r;
    r.x = p;
    r.y = M_PI + std::atan2(ray.x, ray.z);
    r.z = M_PI_2 - std::sin(ray.y / p);
    return r;
  }

template <typename ScalarT, typename PointT, int Size>
  typename KinectUndistortionMap<ScalarT, PointT, Size>::PointSphere KinectUndistortionMap<ScalarT, PointT, Size>::toSphere(const PointT & ray) const
  {
    ScalarT p(std::sqrt(ray.x * ray.x + ray.y * ray.y + ray.z * ray.z));
    PointSphere r(M_PI + std::atan2(ray.x, ray.z), M_PI_2 - std::asin(ray.y / p));
    return r;
  }

template <typename ScalarT, typename PointT, int Size>
  typename KinectUndistortionMap<ScalarT, PointT, Size>::PointSphere KinectUndistortionMap<ScalarT, PointT, Size>::toSphere(const Eigen::Vector3d & ray) const
  {
    ScalarT p(ray.norm());
    PointSphere r(M_PI + std::atan2(ray.x(), ray.z()), M_PI_2 - std::asin(ray.y() / p));
    return r;
  }

} /* namespace calibration */
#endif /* KINECT_IMPL_DEPTH_UNDISTORTION_MAP_HPP_ */
