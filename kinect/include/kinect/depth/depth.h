/*
 * kinect/depth/depth.h
 *
 *  Created on: Sep 8, 2013
 *      Author: Filippo Basso
 */

#ifndef KINECT_DEPTH_DEPTH_H_
#define KINECT_DEPTH_DEPTH_H_

#include <kinect/depth/distortion_model.h>
#include <kinect/depth/polynomial_undistortion_map.h>
#include <kinect/depth/polynomial_undistortion_map_multifit.h>
#include <kinect/depth/undistortion_map.h>

#endif /* KINECT_DEPTH_DEPTH_H_ */
