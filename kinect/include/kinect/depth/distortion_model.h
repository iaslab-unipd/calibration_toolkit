/*
 * kinect/depth/distortion_model.h
 *
 *  Created on: Sep 8, 2013
 *      Author: Filippo Basso
 */

#ifndef KINECT_DEPTH_DISTORTION_MODEL_H_
#define KINECT_DEPTH_DISTORTION_MODEL_H_

#include <calibration_common/base/typedefs.h>
#include <calibration_common/depth/distortion_model.h>
#include <calibration_common/depth/undistortion_map.h>

namespace calibration
{

template <typename PointT, typename MathFunctionT>
  class KinectDepthDistortionModel : public DepthDistortionModel<PointT>
  {
  public:

    /* typedefs */

    // Smart pointers
    typedef boost::shared_ptr<KinectDepthDistortionModel> Ptr;
    typedef boost::shared_ptr<const KinectDepthDistortionModel> ConstPtr;

    typedef UndistortionMap<double, PointT> UndistortionMapT; // XXX should use KinectUndistortionMap but need to change hierarchy
    typedef typename UndistortionMapT::ConstPtr UndistortionMapConstPtr;

    typedef typename MathFunctionT::ConstPtr MathFunctionConstPtr;

    /* methods */

    KinectDepthDistortionModel(const UndistortionMapConstPtr & undistortion_map,
                               const MathFunctionConstPtr & error_function);

    virtual
    ~KinectDepthDistortionModel();

    virtual void
    undistort(pcl::PointCloud<PointT> & cloud) const;

  protected:

    const UndistortionMapConstPtr undistortion_map_;
    const MathFunctionConstPtr error_function_;

  };

} /* namespace calibration */

#include <impl/kinect/depth/distortion_model.hpp>

#endif /* KINECT_DEPTH_DISTORTION_MODEL_H_ */
