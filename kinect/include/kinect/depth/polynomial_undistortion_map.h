/*
 * kinect/depth/polynomial_undistortion_map.h
 *
 *  Created on: Sep 3, 2013
 *      Author: Mauro Antonello
 *      Author: Filippo Basso
 */

#ifndef KINECT_DEPTH_POLYNOMIAL_UNDISTORTION_MAP_H_
#define KINECT_DEPTH_POLYNOMIAL_UNDISTORTION_MAP_H_

#include <kinect/depth/undistortion_map.h>

namespace calibration
{

template <typename ScalarT, typename PointT, typename PolyFunT>
  class KinectPolynomialUndistortionMap : public KinectUndistortionMap<ScalarT, PointT, PolyFunT::SIZE>
  {

  public:

    typedef boost::shared_ptr<KinectPolynomialUndistortionMap> Ptr;
    typedef boost::shared_ptr<const KinectPolynomialUndistortionMap> ConstPtr;

    typedef typename PolyFunT::Polynomial Polynomial;

    typedef MathFunctionTraits<PolyFunT> Info;

    enum
    {
      SIZE = Info::SIZE,
      MIN_DEGREE = Info::MIN_DEGREE,
      DEGREE = Info::DEGREE
    };

    typedef KinectUndistortionMap<ScalarT, PointT, Info::SIZE> Base;

    typedef typename Base::Element Element;
    typedef typename Base::ConstElement ConstElement;

    KinectPolynomialUndistortionMap(const size_t & rows,
                                    const size_t & cols,
                                    const ScalarT & fov_width = KinectInfo::FOV_WIDTH,
                                    const ScalarT & fov_height = KinectInfo::FOV_HEIGHT,
                                    const Eigen::Vector3d & central_ray = Eigen::Vector3d::UnitZ(),
                                    const Polynomial & value = PolyFunT::Identity());

    using Base::undistort;

    virtual void undistort(PointT & point,
                           const ConstElement data) const;

  };

} /* namespace calibration */

#include <impl/kinect/depth/polynomial_undistortion_map.hpp>

#endif /* KINECT_DEPTH_POLYNOMIAL_UNDISTORTION_MAP_H_ */
