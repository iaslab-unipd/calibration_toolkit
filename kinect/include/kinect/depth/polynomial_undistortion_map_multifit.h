/*
 * kinect/depth/polynomial_undistortion_map_multifit.h
 *
 *  Created on: Sep 3, 2013
 *      Author: Mauro Antonello
 *      Author: Filippo Basso
 */

#ifndef KINECT_DEPTH_POLYNOMIAL_UNDISTORTION_MAP_MULTIFIT_H_
#define KINECT_DEPTH_POLYNOMIAL_UNDISTORTION_MAP_MULTIFIT_H_

#include <Eigen/Dense>
#include <calibration_common/depth/undistortion_map_multifit.h>
#include <kinect/depth/polynomial_undistortion_map.h>

namespace calibration
{

template <typename ScalarT, typename PointT, typename PolyFunT>
  class KinectPolynomialUndistortionMapMultifit : virtual public UndistortionMapMultifit<PointT>
  {
  public:

    typedef boost::shared_ptr<KinectPolynomialUndistortionMapMultifit> Ptr;
    typedef boost::shared_ptr<const KinectPolynomialUndistortionMapMultifit> ConstPtr;

    typedef KinectPolynomialUndistortionMap<ScalarT, PointT, PolyFunT> UndistortionMapT;
    typedef typename UndistortionMapT::Ptr UndistortionMapPtr;

    typedef typename UndistortionMapMultifit<PointT>::PointCloudConstPtr PointCloudConstPtr;

  protected:

    typedef MathFunctionTraits<PolyFunT> Info;

    enum
    {
      SIZE = Info::SIZE,
      MIN_DEGREE = Info::MIN_DEGREE,
      DEGREE = Info::DEGREE
    };

    typedef std::vector<std::pair<double, double> > PointDistorsionBin;

    class AccumulationBin
    {
    public:

      AccumulationBin()
        : sum_(Point3d::Zero()), n_(0)
      {
        // Do nothing
      }

      void reset()
      {
        sum_ = Point3d::Zero();
        n_ = 0;
      }

      AccumulationBin & operator +=(const Point3d & point)
      {
        sum_ += point;
        ++n_;
        return *this;
      }

      bool isEmpty()
      {
        return n_ == 0;
      }

      Point3d average()
      {
        return sum_ / n_;
      }

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    protected:

      Point3d sum_;
      int n_;

    };

  public:

    KinectPolynomialUndistortionMapMultifit(const UndistortionMapPtr & distortion_map);

    virtual
    ~KinectPolynomialUndistortionMapMultifit();

    virtual void
    accumulateCloud(const PointCloudConstPtr & cloud,
                    const std::vector<int> & indices);

    virtual void
    accumulatePoint(const PointT & point);

    virtual void
    addAccumulatedPoints(const Plane3d & plane);

    virtual void
    addPoint(const PointT & point,
             const Plane3d & plane);

    virtual void
    update();

    virtual const typename UndistortionMap<double, PointT>::ConstPtr
    undistortionMap() const;

  protected:

    UndistortionMapPtr distortion_map_;
    std::vector<PointDistorsionBin> distorsion_bins_;
    std::vector<AccumulationBin> accumulation_bins_;

  };

} /* namespace calibration */

#include <impl/kinect/depth/polynomial_undistortion_map_multifit.hpp>

#endif /* KINECT_DEPTH_POLYNOMIAL_UNDISTORTION_MAP_MULTIFIT_H_ */
