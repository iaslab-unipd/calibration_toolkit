/*
 * kinect/depth/undistortion_map.h
 *
 *  Created on: Sep 3, 2013
 *      Author: Filippo Basso
 */

#ifndef KINECT_DEPTH_UNDISTORTION_MAP_H_
#define KINECT_DEPTH_UNDISTORTION_MAP_H_

#include <calibration_common/depth/undistortion_map.h>

namespace calibration
{

struct KinectInfo
{
  static const double FOV_WIDTH;
  static const double FOV_HEIGHT;
};

template <typename ScalarT, typename PointT, int Size>
  class KinectUndistortionMap : public UndistortionMap_<ScalarT, PointT, Size>
  {
  public:

    typedef boost::shared_ptr<KinectUndistortionMap> Ptr;
    typedef boost::shared_ptr<const KinectUndistortionMap> ConstPtr;

    typedef UndistortionMap_<ScalarT, PointT, Size> Base;

    typedef typename Base::Data Data;
    typedef typename Base::Element Element;
    typedef typename Base::ConstElement ConstElement;
    typedef typename Base::PointSphere PointSphere;

    KinectUndistortionMap(const size_t & rows,
                          const size_t & cols,
                          const ScalarT & fov_width = KinectInfo::FOV_WIDTH,
                          const ScalarT & fov_height = KinectInfo::FOV_HEIGHT,
                          const Eigen::Vector3d & central_ray = Eigen::Vector3d::UnitZ(),
                          const Data & value = Data::Zero());

    KinectUndistortionMap(const size_t & rows,
                          const size_t & cols,
                          const KinectUndistortionMap & other);

    virtual
    ~KinectUndistortionMap();

    PointT
    toPolar(const PointT & ray) const;

    PointSphere
    toSphere(const PointT & ray) const;

    PointSphere
    toSphere(const Eigen::Vector3d & ray) const;

  };

} /* namespace calibration */

#include <impl/kinect/depth/undistortion_map.hpp>

#endif /* KINECT_DEPTH_UNDISTORTION_MAP_H_ */
