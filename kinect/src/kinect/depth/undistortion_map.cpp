/*
 * kinect/depth/undistortion_map.cpp
 *
 *  Created on: Sep 5, 2013
 *      Author: Mauro Antonello
 */

#include <kinect/depth/undistortion_map.h>

namespace calibration
{

  const double KinectInfo::FOV_WIDTH = DEG2RAD(57.0);
  const double KinectInfo::FOV_HEIGHT = DEG2RAD(43.5);

} /* namespace calibration */
