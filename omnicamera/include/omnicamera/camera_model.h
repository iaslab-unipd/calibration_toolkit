/*
 * omnicamera/camera_model.h
 *
 *  Created on: Oct 18, 2012
 *      Author: Filippo Basso
 */

#ifndef OMNICAMERA_CAMERA_MODEL_H_
#define OMNICAMERA_CAMERA_MODEL_H_

#include <ros/ros.h>
#include <calibration_common/color/camera_model.h>
#include <calibration_msgs/OmnicameraInfo.h>

using calibration_msgs::OmnicameraInfo;

namespace calibration
{

/* ****************** Class definition ****************** */

/**
 *
 */
class OmnicameraModel : public ColorCameraModel
{
public:

  /* typedefs */

  // Smart pointers
  typedef boost::shared_ptr<OmnicameraModel> Ptr;
  typedef boost::shared_ptr<const OmnicameraModel> ConstPtr;

  /* methods */

  /**
   *
   */
  OmnicameraModel();

  /**
   *
   * @param polynomial
   * @param inverse_polynomial
   * @param xc
   * @param yc
   * @param c
   * @param d
   * @param e
   */
  OmnicameraModel(const Eigen::VectorXd & polynomial,
                  const Eigen::VectorXd & inverse_polynomial,
                  double xc,
                  double yc,
                  double c,
                  double d,
                  double e,
                  int width,
                  int height);

  /**
   *
   * @param polynomial
   * @param inverse_polynomial
   * @param xc
   * @param yc
   * @param affine_parameters
   */
  OmnicameraModel(const std::vector<double> & polynomial,
                  const std::vector<double> & inverse_polynomial,
                  double xc,
                  double yc,
                  const boost::array<double, 3> & affine_parameters,
                  int width,
                  int height);

  /**
   *
   * @param msg
   */
  OmnicameraModel(const OmnicameraInfo & msg);

  /**
   *
   */
  virtual
  ~OmnicameraModel();

  /**
   * \brief Set the camera parameters from the sensor_msgs/CameraInfo message.
   */
  void
  fromOmnicameraInfo(const OmnicameraInfo & msg);

  /**
   * \brief Set the camera parameters from the sensor_msgs/CameraInfo message.
   */
  void
  fromOmnicameraInfo(const OmnicameraInfo::ConstPtr & msg);

  /**
   * \brief Get the name of the camera coordinate frame in tf.
   */
  std::string
  tfFrame() const;

  /**
   * \brief Get the time stamp associated with this camera model.
   */
  ros::Time
  stamp() const;

  int
  calibrationWidth() const;

  int
  calibrationHeight() const;

  /**
   * \brief Returns true if the camera has been initialized
   */
  bool
  initialized() const;

  /**
   *
   * @return
   */
  const Eigen::Vector2d &
  getCenter() const;

  /**
   *
   * @return
   */
  const Eigen::VectorXd &
  getInversePolynomial() const;

  /**
   *
   * @return
   */
  int
  getInversePolynomialSize() const;

  /**
   *
   * @return
   */
  const Eigen::VectorXd &
  getPolynomial() const;

  /**
   *
   * @return
   */
  int
  getPolynomialSize() const;

  /**
   *
   * @param pixel_point
   * @return
   */
  Point3d
  projectPixelTo3dRay(const Point2d & pixel_point) const;

  /**
   *
   * @param world_point
   * @return
   */
  Point2d
  project3dToPixel(const Point3d & world_point) const;

  /**
   *
   * @param pixel_points
   * @return
   */
  PointSet3d
  projectPixelTo3dRay(const PointSet2d & pixel_points) const;

  /**
   *
   * @param world_points
   * @return
   */
  PointSet2d
  project3dToPixel(const PointSet3d & world_points) const;

protected:

  /**
   *
   * @param center
   */
  void
  setCenter(const Point2d & center);

  /**
   *
   * @param x
   * @param y
   */
  void
  setCenter(const double & x,
            const double & y);

  /**
   *
   * @param c
   * @param d
   * @param e
   */
  void
  setAffineParameters(const double & c,
                      const double & d,
                      const double & e);

  /**
   *
   * @param inv_poly
   */
  void
  setInversePolynomial(const Eigen::VectorXd & inverse_polynomial);

  /**
   *
   * @param inv_poly
   */
  void
  setInversePolynomial(const std::vector<double> & inverse_polynomial);

  /**
   *
   * @param poly
   */
  void
  setPolynomial(const Eigen::VectorXd & polynomial);

  /**
   *
   * @param poly
   */
  void
  setPolynomial(const std::vector<double> & polynomial);

  /* variables */

  Eigen::VectorXd polynomial_;                  ///
  int polynomial_size_;                         ///

  Eigen::VectorXd inverse_polynomial_;          ///
  int inverse_polynomial_size_;                 ///

  Eigen::Vector2d center_;                      ///
  Eigen::Matrix2d coefficients_;                ///
  Eigen::Matrix2d inverse_coefficients_;        ///

  bool initialized_;
  OmnicameraInfo omnicamera_info_;

public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};

/* ****************** Methods implementation ****************** */

inline std::string OmnicameraModel::tfFrame() const
{
  return omnicamera_info_.header.frame_id;
}

inline ros::Time OmnicameraModel::stamp() const
{
  return omnicamera_info_.header.stamp;
}

inline int OmnicameraModel::calibrationWidth() const
{
  return omnicamera_info_.width;
}

inline int OmnicameraModel::calibrationHeight() const
{
  return omnicamera_info_.height;
}

inline bool OmnicameraModel::initialized() const
{
  return initialized_;
}

inline const Eigen::Vector2d&
OmnicameraModel::getCenter() const
{
  return center_;
}

inline void OmnicameraModel::setCenter(const Point2d & center)
{
  center_ = center;
}

inline void OmnicameraModel::setCenter(const double & x,
                                       const double & y)
{
  center_ << x, y;
}

inline void OmnicameraModel::setAffineParameters(const double & c,
                                                 const double & d,
                                                 const double & e)
{
  coefficients_ << c, d, e, 1;
  inverse_coefficients_ << 1, -d, -e, c;
  inverse_coefficients_ /= (c - d * e);
}

inline const Eigen::VectorXd&
OmnicameraModel::getInversePolynomial() const
{
  return inverse_polynomial_;
}

inline void OmnicameraModel::setInversePolynomial(const Eigen::VectorXd & inverse_polynomial)
{
  inverse_polynomial_ = inverse_polynomial;
  inverse_polynomial_size_ = inverse_polynomial.rows();
}

inline void OmnicameraModel::setInversePolynomial(const std::vector<double> & inverse_polynomial)
{
  inverse_polynomial_ = Eigen::Map<const Eigen::VectorXd>(inverse_polynomial.data(), inverse_polynomial.size(), 1);
  inverse_polynomial_size_ = inverse_polynomial.size();
}

inline int OmnicameraModel::getInversePolynomialSize() const
{
  return inverse_polynomial_size_;
}

inline const Eigen::VectorXd &
OmnicameraModel::getPolynomial() const
{
  return polynomial_;
}

inline void OmnicameraModel::setPolynomial(const Eigen::VectorXd & polynomial)
{
  polynomial_ = polynomial;
  polynomial_size_ = polynomial.rows();
}

inline void OmnicameraModel::setPolynomial(const std::vector<double> & polynomial)
{
  setPolynomial(Eigen::Map<const Eigen::VectorXd>(polynomial.data(), polynomial.size(), 1));
}

inline int OmnicameraModel::getPolynomialSize() const
{
  return polynomial_size_;
}

} /* namespace calibration */
#endif /* OMNICAMERA_CAMERA_MODEL_H_ */
