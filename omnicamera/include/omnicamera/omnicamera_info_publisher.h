
#ifndef OMNICAMERA_INFO_PUBLISHER_H_
#define OMNICAMERA_INFO_PUBLISHER_H_

#include <nodelet/nodelet.h>

#include <omnicamera/omnicamera_info_manager.h>
#include <calibration_msgs/OmnicameraInfo.h>

#include <sensor_msgs/CameraInfo.h>

using calibration_msgs::OmnicameraInfo;

namespace calibration
{

class OmnicameraInfoPublisher : public nodelet::Nodelet
{
public:

  virtual void
  onInit();

protected:

  virtual void
  cameraInfoCallback(const sensor_msgs::CameraInfo::ConstPtr& msg);

  ros::Subscriber camera_info_sub_;
  ros::Publisher omnicamera_info_pub_;

  OmnicameraInfo omnicamera_info_msg_;

};

} /* namespace calibration */
#endif /* OMNICAMERA_INFO_PUBLISHER_H_ */
