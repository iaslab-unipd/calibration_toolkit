/*
 * omnicamera/pose_estimator.h
 *
 *  Created on: Sep 24, 2012
 *      Author: Filippo Basso
 */

#ifndef OMNICAMERA_POSE_ESTIMATOR_H_
#define OMNICAMERA_POSE_ESTIMATOR_H_

#include <calibration_common/base/pose_estimator.h>

#include <omnicamera/camera_model.h>

#include <opencv2/opencv.hpp>
#include <Eigen/Geometry>
#include <magic_folder/eigen_conversions.h>

namespace calibration
{

/**
 *
 */
class OmnicameraPoseEstimator : public PoseEstimator
{
public:

  /* typedefs */

  // Smart pointers
  typedef boost::shared_ptr<OmnicameraPoseEstimator> Ptr;
  typedef boost::shared_ptr<const OmnicameraPoseEstimator> ConstPtr;

  // Others
  typedef cv::Vec<double, 2> CvVector2d;
  typedef cv::Vec<double, 3> CvVector3d;
  typedef cv::Mat_<CvVector2d> CvMatrix2Xd;
  typedef cv::Mat_<CvVector3d> CvMatrix3Xd;

  /* methods */

  /**
   * @brief OmnicameraPoseEstimator
   * @param camera_model
   */
  OmnicameraPoseEstimator(const OmnicameraModel::ConstPtr & camera_model);

  /**
   * @brief ~OmnicameraPoseEstimator
   */
  virtual
  ~OmnicameraPoseEstimator();

  /**
   * @brief estimatePose
   * @param points_image
   * @param points_object
   * @param camera_model
   * @return
   */
  virtual Pose3d
  estimatePose(const PointSet2d & points_image,
               const PointSet3d & points_object) const;

protected:

  OmnicameraModel::ConstPtr camera_model_;

};

} /* namespace calibration */
#endif /* OMNICAMERA_POSE_ESTIMATOR_H_ */
