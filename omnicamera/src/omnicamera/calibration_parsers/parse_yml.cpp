/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2009, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Willow Garage nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <omnicamera/calibration_parsers/parse_yml.h>

#include <yaml-cpp/yaml.h>
#include <fstream>
#include <ctime>
#include <cassert>
#include <cstring>
#include <ros/console.h>

namespace calibration
{
namespace omnicamera_calibration_parsers
{

static const char CAM_YML_NAME[] = "camera_name";
static const char POLY_YML_NAME[] = "polynomial";
static const char INV_POLY_YML_NAME[] = "inverse_polynomial";
static const char WIDTH_YML_NAME[] = "width";
static const char HEIGHT_YML_NAME[] = "height";
static const char CX_YML_NAME[] = "center_x";
static const char CY_YML_NAME[] = "center_y";
static const char AFFINE_YML_NAME[] = "affine_parameters";

bool writeCalibrationYml(std::ostream & out,
                         const std::string & camera_name,
                         const OmnicameraInfo & camera_info)
{
  YAML::Emitter emitter;
  emitter << YAML::BeginMap;

  // Camera name and intrinsics
  emitter << YAML::Key << CAM_YML_NAME << YAML::Value << camera_name;

  emitter << YAML::Key << POLY_YML_NAME << YAML::Value << camera_info.polynomial;
  emitter << YAML::Key << INV_POLY_YML_NAME << YAML::Value << camera_info.inverse_polynomial;

  std::vector<double> tmp;
  for (unsigned int i = 0; i < camera_info.affine_parameters.size(); ++i)
    tmp.push_back(camera_info.affine_parameters[i]);

  emitter << YAML::Key << AFFINE_YML_NAME << YAML::Value << tmp;

  emitter << YAML::Key << CX_YML_NAME << YAML::Value << camera_info.center_x;
  emitter << YAML::Key << CY_YML_NAME << YAML::Value << camera_info.center_y;

  // Image dimensions
  emitter << YAML::Key << WIDTH_YML_NAME << YAML::Value << camera_info.width;
  emitter << YAML::Key << HEIGHT_YML_NAME << YAML::Value << camera_info.height;

  emitter << YAML::EndMap;
  out << emitter.c_str();
  return true;
}

bool writeCalibrationYml(const std::string & file_name,
                         const std::string & camera_name,
                         const OmnicameraInfo & camera_info)
{
  std::ofstream out(file_name.c_str());
  if (not out.is_open())
  {
    ROS_ERROR("Unable to open camera calibration file [%s] for writing", file_name.c_str());
    return false;
  }
  return writeCalibrationYml(out, camera_name, camera_info);
}

bool readCalibrationYml(std::istream & in,
                        std::string & camera_name,
                        OmnicameraInfo & camera_info)
{
  try
  {
    YAML::Parser parser(in);
    if (not parser)
    {
      ROS_ERROR("Unable to create YAML parser for camera calibration");
      return false;
    }
    YAML::Node doc;
    parser.GetNextDocument(doc);

    if (const YAML::Node* name_node = doc.FindValue(CAM_YML_NAME))
      *name_node >> camera_name;
    else
      camera_name = "unknown";

    doc[POLY_YML_NAME] >> camera_info.polynomial;
    doc[INV_POLY_YML_NAME] >> camera_info.inverse_polynomial;
    std::vector<double> tmp;
    doc[AFFINE_YML_NAME] >> tmp;
    for (unsigned int i = 0; i < camera_info.affine_parameters.size(); ++i)
      camera_info.affine_parameters[i] = tmp[i];

    doc[CX_YML_NAME] >> camera_info.center_x;
    doc[CY_YML_NAME] >> camera_info.center_y;

    doc[WIDTH_YML_NAME] >> camera_info.width;
    doc[HEIGHT_YML_NAME] >> camera_info.height;

    return true;
  }
  catch (YAML::Exception& e)
  {
    ROS_ERROR("Exception parsing YAML camera calibration:\n%s", e.what());
    return false;
  }
}

bool readCalibrationYml(const std::string & file_name,
                        std::string & camera_name,
                        OmnicameraInfo & cam_info)
{
  std::ifstream fin(file_name.c_str());
  if (not fin.good())
  {
    ROS_ERROR("Unable to open camera calibration file [%s]", file_name.c_str());
    return false;
  }
  bool success = readCalibrationYml(fin, camera_name, cam_info);
  if (not success)
    ROS_ERROR("Failed to parse camera calibration from file [%s]", file_name.c_str());
  return success;
}

} /* namespace omnicamera_calibration_parsers */
} /* namespace calibration */
