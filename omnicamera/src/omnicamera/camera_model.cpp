/*
 * omnicamera/camera_model.cpp
 *
 *  Created on: Oct 18, 2012
 *      Author: Filippo Basso
 */

#include <omnicamera/camera_model.h>

namespace calibration
{

OmnicameraModel::OmnicameraModel()
  : polynomial_size_(0), inverse_polynomial_size_(0), initialized_(false)
{
  // Do nothing
}

OmnicameraModel::OmnicameraModel(const Eigen::VectorXd& polynomial,
                                 const Eigen::VectorXd& inverse_polynomial,
                                 double xc,
                                 double yc,
                                 double c,
                                 double d,
                                 double e,
                                 int width,
                                 int height)
  : center_(xc, yc), initialized_(true)
{
  setPolynomial(polynomial);
  setInversePolynomial(inverse_polynomial);
  setAffineParameters(c, d, e);
}

OmnicameraModel::OmnicameraModel(const std::vector<double> & polynomial,
                                 const std::vector<double> & inverse_polynomial,
                                 double xc,
                                 double yc,
                                 const boost::array<double, 3> & affine_parameters,
                                 int width,
                                 int height)
  : center_(xc, yc), initialized_(true)
{
  setPolynomial(polynomial);
  setInversePolynomial(inverse_polynomial);
  setAffineParameters(affine_parameters[0], affine_parameters[1], affine_parameters[2]);
}

OmnicameraModel::OmnicameraModel(const OmnicameraInfo & msg)
{
  fromOmnicameraInfo(msg);
}

OmnicameraModel::~OmnicameraModel()
{
  // Do nothing
}

void OmnicameraModel::fromOmnicameraInfo(const OmnicameraInfo & msg)
{
  setPolynomial(msg.polynomial);
  setInversePolynomial(msg.inverse_polynomial);
  setCenter(msg.center_x, msg.center_y);
  setAffineParameters(msg.affine_parameters[0], msg.affine_parameters[1], msg.affine_parameters[2]);

  omnicamera_info_ = msg;

  initialized_ = true;
}

void OmnicameraModel::fromOmnicameraInfo(const OmnicameraInfo::ConstPtr & msg)
{
  fromOmnicameraInfo(*msg);
}

Point3d OmnicameraModel::projectPixelTo3dRay(const Point2d & pixel_point) const
{
  Eigen::Vector3d world_point;
  world_point.head<2>() = inverse_coefficients_ * (pixel_point - center_);
  double r_coeff = world_point.head<2>().norm();

  Eigen::VectorXd r(polynomial_size_, 1);
  r[0] = 1.0;
  for (int i = 1; i < r.rows(); ++i)
    r[i] = r[i - 1] * r_coeff;

  world_point[2] = r.dot(polynomial_);

  return world_point.normalized();
}

Point2d OmnicameraModel::project3dToPixel(const Point3d & world_point) const
{
  double norm = world_point.head<2>().norm();
  double t_coeff = std::atan(world_point[2] / norm);

  Eigen::VectorXd t(inverse_polynomial_size_, 1);
  t[0] = 1.0;
  if (norm != 0)
  {
    for (int i = 1; i < inverse_polynomial_size_; ++i)
      t[i] = t[i - 1] * t_coeff;

    return coefficients_ * world_point.head<2>() * t.dot(inverse_polynomial_) / norm + center_;
  }
  else
  {
    return center_;
  }
}

PointSet3d OmnicameraModel::projectPixelTo3dRay(const PointSet2d & pixel_points) const
{
  PointSet3d world_points(pixel_points.width(), pixel_points.height());
  for (int i = 0; i < pixel_points.size(); ++i)
    world_points[i] = projectPixelTo3dRay(Point2d(pixel_points[i]));
  return world_points;
}

PointSet2d OmnicameraModel::project3dToPixel(const PointSet3d & world_points) const
{
  PointSet2d pixel_points(world_points.width(), world_points.height());
  for (int i = 0; i < world_points.size(); ++i)
    pixel_points[i] = project3dToPixel(Point3d(world_points[i]));
  return pixel_points;
}

} /* namespace omnicamera */
