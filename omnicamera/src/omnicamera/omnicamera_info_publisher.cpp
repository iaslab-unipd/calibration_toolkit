#include <omnicamera/omnicamera_info_publisher.h>
#include <pluginlib/class_list_macros.h>

using calibration_msgs::OmnicameraInfo;

namespace calibration
{

void OmnicameraInfoPublisher::cameraInfoCallback(const sensor_msgs::CameraInfo::ConstPtr & msg)
{
  NODELET_INFO("A");
  omnicamera_info_msg_.header = msg->header;
  omnicamera_info_pub_.publish(omnicamera_info_msg_);
}

void OmnicameraInfoPublisher::onInit()
{
  NODELET_DEBUG("Initializing OmnicameraInfoPublisher nodelet...");

  ros::NodeHandle node_handle(getNodeHandle());
  ros::NodeHandle priv_node_handle(getPrivateNodeHandle());

  std::string camera_node, camera_info_topic, omnicamera_info_topic;
  std::stringstream ss;

  priv_node_handle.param("camera", camera_node, std::string("camera"));
  if (camera_node[camera_node.size() - 1] != '/')
    camera_node.append("/");

  ss << camera_node << "camera_info";
  camera_info_topic = ss.str();

  ss.str(""); // Reset ss

  ss << camera_node << "omnicamera_info";
  omnicamera_info_topic = ss.str();

  camera_info_sub_ = node_handle.subscribe(camera_info_topic, 10, &OmnicameraInfoPublisher::cameraInfoCallback, this);
  omnicamera_info_pub_ = node_handle.advertise<OmnicameraInfo>(omnicamera_info_topic, 10);

  std::string url, camera_name;
  priv_node_handle.param("url", url, std::string(""));
  priv_node_handle.param("camera_name", camera_name, std::string("camera"));

  OmnicameraInfoManager manager(priv_node_handle, camera_name, url);
  omnicamera_info_msg_ = manager.getOmnicameraInfo();

}

} /* namespace calibration */

PLUGINLIB_EXPORT_CLASS(calibration::OmnicameraInfoPublisher, nodelet::Nodelet)
