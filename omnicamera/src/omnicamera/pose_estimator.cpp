/*
 * omnicamera/pose_estimator.cpp
 *
 *  Created on: Sep 24, 2012
 *      Author: Filippo Basso
 */

#include <omnicamera/pose_estimator.h>

namespace calibration
{

OmnicameraPoseEstimator::OmnicameraPoseEstimator(const OmnicameraModel::ConstPtr &camera_model)
  : PoseEstimator(), camera_model_(camera_model)
{
  // Do nothing
}

OmnicameraPoseEstimator::~OmnicameraPoseEstimator()
{
  // Do nothing
}

Pose3d OmnicameraPoseEstimator::estimatePose(const PointSet2d & points_image,
                                             const PointSet3d & points_object) const
{
  assert(points_image.size() == points_object.size());

  const int points = points_image.size();

  PointSet3d points_unit_sphere = camera_model_->projectPixelTo3dRay(points_image);

  Point3d plane_normal = points_unit_sphere[points / 2].normalized();
  Point3d plane_point = points_unit_sphere[points / 2];

  Eigen::Matrix3d new_base = Eigen::Matrix3d::Identity();
  new_base.col(1) = new_base.col(2) - new_base.col(2).dot(plane_normal) * plane_normal;
  new_base.col(1).normalize();
  new_base.col(2) = plane_normal;
  new_base.col(0) = new_base.col(1).cross(new_base.col(2));
  new_base.col(0).normalize();

  magic_folder::EigenConversions<double> converter;
  CvMatrix2Xd cv_points_image;
  CvMatrix3Xd cv_points_object;
  converter.eigen2opencv(points_object.matrix(), cv_points_object);

  PointSet3d points_plane(3, points);
  for (int i = 0; i < points; ++i)
  {
    double d = plane_point.dot(plane_normal) / points_unit_sphere[i].dot(plane_normal);
    points_plane[i] = d * points_unit_sphere[i];
    Point3d image_pos = new_base.inverse() * points_plane[i];
    image_pos /= image_pos[2];
    cv_points_image.push_back(converter.eigen2opencv(Point2d(image_pos.head<2>())));
  }

  CvVector3d cv_r, cv_t;
  cv::solvePnP(cv_points_object, cv_points_image, cv::Mat_<double>::eye(3, 3), cv::Mat(), cv_r, cv_t);

  Point3d r = converter.opencv2eigen(cv_r);
  Point3d t = converter.opencv2eigen(cv_t);

  Eigen::AngleAxisd rotation(r.norm(), r.normalized());
  Eigen::Translation3d translation(t);
  return new_base * translation * rotation;

}

} /* namespace calibration */
