/*
 * omnicamera_info_manager_node.cpp
 *
 *  Created on: Mar 1, 2013
 *      Author: Filippo Basso
 */

#include <ros/ros.h>
#include <omnicamera/omnicamera_info_manager.h>

using namespace calibration;
using calibration_msgs::OmnicameraInfo;


int
main(int argc, char** argv)
{
  ros::init(argc, argv, "omnicamera_info_manager_node");
  ros::NodeHandle nh("~");

  std::string url, camera_name;

  nh.param("url", url, std::string(""));
  nh.param("camera_name", camera_name, std::string("camera"));

  OmnicameraInfoManager manager(nh, camera_name, url);

  ros::spin();
}


