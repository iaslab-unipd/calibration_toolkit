/*
 * test_omnicamera_info.cpp
 *
 *  Created on: Mar 1, 2013
 *      Author: Filippo Basso
 */

#include <ros/ros.h>
#include <omnicamera/omnicamera_info_manager.h>

using namespace calibration;
using calibration_msgs::OmnicameraInfo;

int main(int argc,
         char ** argv)
{
  ros::init(argc, argv, "omnicamera_info_test");
  ros::NodeHandle nh("~");

  std::string url, camera_name;
  nh.param("url", url, std::string(""));
  nh.param("camera_name", camera_name, std::string("camera"));

  OmnicameraInfoManager manager(nh, camera_name, url);
  OmnicameraInfo info = manager.getOmnicameraInfo();

  SetOmnicameraInfo service;
  service.request.camera_info = info;

  ROS_INFO_STREAM("Calibration message:\n" << info);

  ros::service::exists("/omnicamera_info_manager_node/set_omnicamera_info", true);

  if (not ros::service::call("/omnicamera_info_manager_node/set_omnicamera_info", service))
  {
    ROS_ERROR_STREAM(service.response.status_message);
  }

  //ros::spin();
}

