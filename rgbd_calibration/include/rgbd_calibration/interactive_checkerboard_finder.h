/*
 * interactive_checkerboard_finder.h
 *
 *  Created on: Sep 25, 2012
 *      Author: Filippo Basso
 */

#ifndef INTERACTIVE_CHECKERBOARD_FINDER_H_
#define INTERACTIVE_CHECKERBOARD_FINDER_H_

#include <ros/ros.h>
#include <opencv2/opencv.hpp>

#include <calibration_common/base/checkerboard.h>

namespace calibration
{

/**
 *
 */
class InteractiveCheckerboardFinder // TODO create a superclass CheckerboardFinder
{
public:

  /* methods */

  /**
   *
   * @param image
   */
  InteractiveCheckerboardFinder(const cv::Mat & image);

  /**
   *
   */
  virtual
  ~InteractiveCheckerboardFinder();

  /**
   *
   * @param checkerboard
   * @param corners
   * @return
   */
  bool
  find(const Checkerboard & checkerboard,
       std::vector<cv::Point2d> & corners,
       bool try_automatically = true);

  /**
   *
   * @param checkerboard
   * @param corners
   * @return
   */
  bool
  find(const Checkerboard & checkerboard,
       PointSet2d & corners,
       bool try_automatically = true);

  /**
   *
   * @param event
   * @param x
   * @param y
   * @param flags
   * @param param
   */
  static void
  selectCornersCallback(int event,
                        int x,
                        int y,
                        int flags,
                        void *param);

  /**
   *
   * @param event
   * @param x
   * @param y
   * @param flags
   * @param param
   */
  static void
  selectSubImageCallback(int event,
                         int x,
                         int y,
                         int flags,
                         void *param);

protected:

  /* variables */

  cv::Mat image_;
  cv::Rect sub_image_rect_;
  std::vector<cv::Point2f> corners_float_;

};

} /* namespace calibration */
#endif /* INTERACTIVE_CHECKERBOARD_FINDER_H_ */
