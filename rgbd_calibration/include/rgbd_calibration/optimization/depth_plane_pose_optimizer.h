/*
 * depth_plane_pose_optimizer.h
 *
 *  Created on: Jun 26, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_DEPTH_PLANE_POSE_OPTIMIZER_H_
#define CALIBRATION_DEPTH_PLANE_POSE_OPTIMIZER_H_

#include <calibration_common/depth/view.h>
#include <calibration_common/base/planar_object.h>
#include <calibration_common/errors/plane_distance.h>
#include <rgbd_calibration/optimization/rgbd_error_functor.h>
#include <unsupported/Eigen/NonLinearOptimization>

namespace calibration
{
namespace optimization
{

template <typename PointT>
  class DepthPlanePoseOptimizer
  {
  public:

    typedef typename PlaneDistanceError<PointT>::Ptr PlaneDistanceErrorPtr;

    typedef RGBDErrorFunctorQ RGBDErrorFunctor;

    class ErrorFunctor : public RGBDErrorFunctor
    {

    public:

      /* typedefs */

      // Derived
      typedef typename RGBDErrorFunctor::ValueType ValueType;
      typedef typename RGBDErrorFunctor::InputType InputType;

      /* methods */

      ErrorFunctor(const PlanarObject::Ptr & depth_plane,
                   const PlaneDistanceErrorPtr & depth_error);

      virtual
      ~ErrorFunctor();

      Eigen::VectorXd
      initialize();

      void
      convertResult(const InputType & x);

      int
      operator()(InputType & x,
                 ValueType & v) const;

    protected:

      PlanarObject::Ptr plane_;
      PlaneDistanceErrorPtr error_;

    };

    DepthPlanePoseOptimizer(const PlanarObject::Ptr & depth_plane,
                            const PointSet3d & inliers);

    double
    error() const;

    bool
    optimize() const;

  protected:

    PlanarObject::Ptr plane_;
    PlaneDistanceErrorPtr error_;

  };

template <typename PointT>
  DepthPlanePoseOptimizer<PointT>::ErrorFunctor::ErrorFunctor(const PlanarObject::Ptr & depth_plane,
                                                              const PlaneDistanceErrorPtr & depth_error)
    : RGBDErrorFunctor(RGBDErrorFunctor::TRANSFORM_SIZE, depth_error->size()), plane_(depth_plane), error_(depth_error)
  {
    // Do nothing
  }

template <typename PointT>
  DepthPlanePoseOptimizer<PointT>::ErrorFunctor::~ErrorFunctor()
  {
    // Do nothing
  }

template <typename PointT>
  Eigen::VectorXd DepthPlanePoseOptimizer<PointT>::ErrorFunctor::initialize()
  {
    Eigen::VectorXd x(inputs_);
    setTransform(x, 0, Transform3d::Identity());
    return x;
  }

template <typename PointT>
  void DepthPlanePoseOptimizer<PointT>::ErrorFunctor::convertResult(const InputType & x)
  {
    plane_->transform(RGBDErrorFunctor::getTransform(x, 0));
  }

template <typename PointT>
  int DepthPlanePoseOptimizer<PointT>::ErrorFunctor::operator()(InputType & x,
                                                                ValueType & v) const
  {
    PlanarObject depth_plane(*plane_, RGBDErrorFunctor::getTransform(x, 0));
    error_->compute(depth_plane.plane(), v);
    return 0;
  }

template <typename PointT>
  DepthPlanePoseOptimizer<PointT>::DepthPlanePoseOptimizer(const PlanarObject::Ptr & depth_plane,
                                                           const PointSet3d & inliers)
    : plane_(depth_plane), error_(new PlaneDistanceError<PointT>(inliers))
  {
    // Do nothing
  }

template <typename PointT>
  double DepthPlanePoseOptimizer<PointT>::error() const
  {
    ErrorFunctor error_functor(plane_, error_);
    Eigen::VectorXd x = error_functor.initialize();
    Eigen::VectorXd v(error_functor.values());
    error_functor(x, v);
    return v.sum();
  }

template <typename PointT>
  bool DepthPlanePoseOptimizer<PointT>::optimize() const
  {
    ErrorFunctor error_functor(plane_, error_);
    Eigen::VectorXd x = error_functor.initialize();
    Eigen::VectorXd v(error_functor.values());

    Eigen::DenseIndex index;

    int tries = 1;
    int info;
    do
    {
      info = Eigen::LevenbergMarquardt<ErrorFunctor>::lmdif1(error_functor, x, &index);
    }
    while (info == 5 and --tries > 0);

    if (info == 0)
      throw std::runtime_error("[DepthPlanePoseOptimizer] Improper input parameters.");

    bool optimized = (info >= 1 and info <= 4);

    if (optimized)
      error_functor.convertResult(x);

    return optimized;
  }

} /* namespace optimization */
} /* namespace calibration */
#endif /* CALIBRATION_DEPTH_PLANE_POSE_OPTIMIZER_H_ */
