/*
 * error_functor.h
 *
 *  Created on: Sep 13, 2012
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_ERROR_FUNCTOR_H_
#define CALIBRATION_ERROR_FUNCTOR_H_

#include <Eigen/Dense>

namespace calibration
{
namespace optimization
{

template<typename ScalarT, int NX = Eigen::Dynamic, int NY = Eigen::Dynamic>
  class ErrorFunctor
  {

  public:

    typedef ScalarT Scalar;

    enum
    {
      InputsAtCompileTime = NX,
      ValuesAtCompileTime = NY
    };

    typedef Eigen::Matrix<ScalarT, InputsAtCompileTime, 1> InputType;
    typedef Eigen::Matrix<ScalarT, ValuesAtCompileTime, 1> ValueType;
    typedef Eigen::Matrix<ScalarT, ValuesAtCompileTime, InputsAtCompileTime> JacobianType;

    ErrorFunctor()
      : inputs_(InputsAtCompileTime), values_(ValuesAtCompileTime)
    {
      // Do nothing
    }

    ErrorFunctor(int inputs,
                 int values)
      : inputs_(inputs), values_(values)
    {
      // Do nothing
    }

    virtual ~ErrorFunctor()
    {
      // Do nothing
    }

    int inputs() const
    {
      return inputs_;
    }

    int values() const
    {
      return values_;
    }

    void setInputs(const int & inputs)
    {
      inputs_ = inputs;
    }

    void setValues(const int & values)
    {
      values_ = values;
    }

    virtual int
    operator()(InputType & x,
               ValueType & v) const = 0;

  protected:

    int inputs_;
    int values_;

  };

} /* namespace optimization */
} /* namespace calibration */
#endif /* CALIBRATION_ERROR_FUNCTOR_H_ */
