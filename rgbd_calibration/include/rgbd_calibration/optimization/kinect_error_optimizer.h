/*
 *  kinect_error_optimizer.h
 *
 *  Created on: Oct 19, 2012
 *      Author: Filippo Basso
 */

#ifndef KINECT_ERROR_OPTIMIZER_H_
#define KINECT_ERROR_OPTIMIZER_H_

#include <calibration_common/errors/angular.h>
#include <calibration_common/color/sensor.h>

#include <rgbd_calibration/optimization/rgbd_error_functor.h>
#include <rgbd_calibration/rgbd/rgbd_checkerboard.h>

#include <kinect/depth/undistortion_map.h>
#include <unsupported/Eigen/NonLinearOptimization>

//#define ALBERTO

namespace calibration
{
namespace optimization
{

template <typename PointT, typename FunctionT>
  class KinectErrorOptimizer
  {
  public:

    typedef boost::shared_ptr<KinectErrorOptimizer> Ptr;
    typedef boost::shared_ptr<const KinectErrorOptimizer> ConstPtr;

    typedef typename FunctionT::Ptr FunctionPtr;

    struct OptimizationData
    {
      typedef boost::shared_ptr<OptimizationData> Ptr;
      typedef boost::shared_ptr<const OptimizationData> ConstPtr;

      OptimizationData(const RGBDCheckerboard<PointT> & rgbd_checkerboard)
        : plane_points_(rgbd_checkerboard.depthView()->interestPoints()),
          color_plane_(rgbd_checkerboard.colorCheckerboard()->plane())
      {
        // Do nothing
      }

      const PointSet3d plane_points_;
      const Plane3d color_plane_;

      Plane3d depth_plane_;

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    };

    typedef typename RGBDCheckerboard<PointT>::ConstPtr RGBDCheckerboardConstPtr;

    typedef typename OptimizationData::Ptr OptimizationDataPtr;
    typedef std::vector<OptimizationDataPtr> DataVector;

    class FunctionErrorFunctor : public ErrorFunctor<double>
    {
    protected:

    public:

      /* typedefs */

      // Derived
      typedef typename ErrorFunctor<double>::ValueType ValueType;
      typedef typename ErrorFunctor<double>::InputType InputType;

      /* methods */

      FunctionErrorFunctor(const DataVector & data_vec,
                           size_t function_coefficients);

      virtual
      ~FunctionErrorFunctor();

      Eigen::VectorXd
      initialize(const FunctionT & distance_function);

      void
      convertResult(const InputType & x,
                    FunctionT & distance_function) const;

      int
      operator()(InputType & x,
                 ValueType & v) const;

    protected:

      DataVector data_vec_;

    };

    KinectErrorOptimizer(const ColorSensor::Ptr & color_sensor,
                         const FunctionPtr & distance_function);

    int
    addData(const RGBDCheckerboardConstPtr & rgbd_checkerboard);

    double
    error() const;

    bool
    optimize();

  protected:

    ColorSensor::Ptr color_sensor_;
    FunctionPtr distance_function_;

    DataVector data_vec_;

  };

template <typename PointT, typename FunctionT>
  KinectErrorOptimizer<PointT, FunctionT>::FunctionErrorFunctor::FunctionErrorFunctor(const DataVector & data_vec,
                                                                                      size_t function_coefficients)
    : ErrorFunctor<double>(), data_vec_(data_vec)
  {
    inputs_ = function_coefficients;
    values_ = 0;

    for (size_t i = 1; i < data_vec_.size(); ++i)
      values_ += i;
  }

template <typename PointT, typename FunctionT>
  KinectErrorOptimizer<PointT, FunctionT>::FunctionErrorFunctor::~FunctionErrorFunctor()
  {
    // Do nothing
  }

template <typename PointT, typename FunctionT>
  Eigen::VectorXd KinectErrorOptimizer<PointT, FunctionT>::FunctionErrorFunctor::initialize(const FunctionT & distance_function)
  {
    Eigen::VectorXd x = distance_function.coefficients();

    PointSet3d points = data_vec_[0]->plane_points_;
    data_vec_[0]->depth_plane_ = PlanarObject::fitPlane(points);

    for (size_t i = 1; i < data_vec_.size(); ++i)
    {
      PointSet3d points = data_vec_[i]->plane_points_;
      data_vec_[i]->depth_plane_ = PlanarObject::fitPlane(points);

      double depth_theta = std::acos(data_vec_[i]->depth_plane_.normal().dot(data_vec_[0]->depth_plane_.normal()));
      double color_theta = std::acos(data_vec_[i]->color_plane_.normal().dot(data_vec_[0]->color_plane_.normal()));
      ROS_WARN_STREAM(depth_theta << " ** " << color_theta);
    }

    return x;
  }

template <typename PointT, typename FunctionT>
  void KinectErrorOptimizer<PointT, FunctionT>::FunctionErrorFunctor::convertResult(const InputType & x,
                                                                                    FunctionT & distance_function) const
  {
    distance_function.coefficients() = x;
  }

template <typename PointT, typename FunctionT>
  int KinectErrorOptimizer<PointT, FunctionT>::FunctionErrorFunctor::operator ()(InputType & x,
                                                                                 ValueType & v) const
  {
    FunctionT function;
    convertResult(x, function);

    int v_index = 0;

    for (size_t i = 0; i < data_vec_.size(); ++i)
    {
      PointSet3d points = data_vec_[i]->plane_points_;

      for (unsigned int p = 0; p < points.size(); ++p)
        points[p] *= function.evaluate(points[p][2]) / points[p][2];

      data_vec_[i]->depth_plane_ = PlanarObject::fitPlane(points);

      for (size_t j = 0; j < i; ++j)
      {
        double depth_theta = std::acos(data_vec_[i]->depth_plane_.normal().dot(data_vec_[j]->depth_plane_.normal()));
        if (depth_theta > M_PI_2)
          depth_theta = M_PI - depth_theta;

        double color_theta = std::acos(data_vec_[i]->color_plane_.normal().dot(data_vec_[j]->color_plane_.normal()));
        if (color_theta > M_PI_2)
          color_theta = M_PI - color_theta;

        v[v_index++] = (depth_theta - color_theta) * (depth_theta - color_theta);
      }

    }

    return 0;
  }

template <typename PointT, typename FunctionT>
  KinectErrorOptimizer<PointT, FunctionT>::KinectErrorOptimizer(const ColorSensor::Ptr & color_sensor,
                                                                const FunctionPtr & distance_function)
    : distance_function_(distance_function)
  {
    // Do nothing
  }

template <typename PointT, typename FunctionT>
  int KinectErrorOptimizer<PointT, FunctionT>::addData(const RGBDCheckerboardConstPtr & rgbd_checkerboard)
  {
    OptimizationDataPtr data(new OptimizationData(*rgbd_checkerboard));
    data_vec_.push_back(data);
    return data_vec_.size();
  }

template <typename PointT, typename FunctionT>
  double KinectErrorOptimizer<PointT, FunctionT>::error() const
  {
    FunctionErrorFunctor error_functor(data_vec_, distance_function_->size());
    Eigen::VectorXd x = error_functor.initialize(*distance_function_);
    Eigen::VectorXd v(error_functor.values());
    error_functor(x, v);
    return v.sum() / v.size();
  }

template <typename PointT, typename FunctionT>
  bool KinectErrorOptimizer<PointT, FunctionT>::optimize()
  {
    bool optimized;

    FunctionErrorFunctor error_functor(data_vec_, distance_function_->size());
    Eigen::VectorXd x = error_functor.initialize(*distance_function_);

    Eigen::VectorXd v(error_functor.values());
    error_functor(x, v);

    Eigen::DenseIndex index;

    ROS_INFO_STREAM("Angular Error");
    ROS_INFO_STREAM("x = " << x.transpose() << ": error = " << v.sum() / v.size());

    int tries = 10;
    int info;
    do
    {
      info = Eigen::LevenbergMarquardt<FunctionErrorFunctor>::lmdif1(error_functor, x, &index);
      error_functor(x, v);
      ROS_INFO_STREAM("x = " << x.transpose() << ": error = " << v.sum() / v.size());
    }
    while (info == 5 and --tries > 0);

    if (info == 0)
      throw std::runtime_error("Improper input parameters.");

    optimized = (info >= 1 and info <= 5);

    if (optimized)
    {
      error_functor.convertResult(x, *distance_function_);
    }

    return optimized;
  }

} /* namespace optimization */
} /* namespace calibration */
#endif /* KINECT_ERROR_OPTIMIZER_H_ */
