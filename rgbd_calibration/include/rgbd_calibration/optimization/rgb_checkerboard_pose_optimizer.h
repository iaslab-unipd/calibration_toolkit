/*
 * rgb_checkerboard_pose_optimizer.h
 *
 *  Created on: Jun 26, 2013
 *      Author: Filippo Basso
 */

#ifndef RGB_CHECKERBOARD_POSE_OPTIMIZER_H_
#define RGB_CHECKERBOARD_POSE_OPTIMIZER_H_

#include <calibration_common/color/view.h>
#include <calibration_common/base/checkerboard.h>
#include <calibration_common/errors/image_reprojection.h>
#include <rgbd_calibration/optimization/rgbd_error_functor.h>

#include <unsupported/Eigen/NonLinearOptimization>

namespace calibration
{
namespace optimization
{

class ImageCheckerboardPoseOptimizer
{
public:

  typedef RGBDErrorFunctorQ RGBDErrorFunctor;

  class ErrorFunctor : public RGBDErrorFunctor
  {

  public:

    /* typedefs */

    // Derived
    typedef typename RGBDErrorFunctor::ValueType ValueType;
    typedef typename RGBDErrorFunctor::InputType InputType;

    /* methods */

    ErrorFunctor(const Checkerboard::Ptr & checkerboard,
                 const ImageReprojectionError::Ptr & error);

    virtual
    ~ErrorFunctor();

    Eigen::VectorXd
    initialize();

    void
    convertResult(const InputType & x);

    int
    operator()(InputType & x,
               ValueType & v) const;

  protected:

    Checkerboard::Ptr checkerboard_;
    ImageReprojectionError::Ptr error_;

  };

  ImageCheckerboardPoseOptimizer(const ColorCameraModel::ConstPtr & camera_model,
                                 const PointSet2d & image_corners,
                                 const Checkerboard::Ptr & checkerboard);

  double
  error() const;

  bool
  optimize() const;

protected:

  Checkerboard::Ptr checkerboard_;
  ImageReprojectionError::Ptr error_;

};

} /* namespace optimization */
} /* namespace calibration */
#endif /* RGB_CHECKERBOARD_POSE_OPTIMIZER_H_ */
