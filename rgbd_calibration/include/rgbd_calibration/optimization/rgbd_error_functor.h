/*
 * rgbd_error_functor.h
 *
 *  Created on: Oct 19, 2012
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_RGBD_ERROR_FUNCTOR_H_
#define CALIBRATION_RGBD_ERROR_FUNCTOR_H_

#include <calibration_common/base/typedefs.h>
#include <rgbd_calibration/optimization/error_functor.h>

namespace calibration
{
namespace optimization
{

class RGBDErrorFunctorQ : public ErrorFunctor<double>
{
protected:

  typedef typename ErrorFunctor<double>::ValueType ValueType;
  typedef typename ErrorFunctor<double>::InputType InputType;

  enum
  {
    TRANSFORM_SIZE = 7,
    TRANSLATION_SIZE = 3,
    ROTATION_SIZE = TRANSFORM_SIZE - TRANSLATION_SIZE
  };

  RGBDErrorFunctorQ()
    : ErrorFunctor<double>()
  {
    // Do nothing
  }

  RGBDErrorFunctorQ(const int & inputs,
                    const int & values)
    : ErrorFunctor<double>(inputs, values)
  {
    // Do nothing
  }

  virtual ~RGBDErrorFunctorQ()
  {
    // Do nothing
  }

  virtual void setTransform(InputType & x,
                            const int & index,
                            const Transform3d & transform) const
  {
    Eigen::Quaterniond rotation(transform.linear());
    x.segment<ROTATION_SIZE>(index) = rotation.coeffs();
    x.segment<TRANSLATION_SIZE>(index + ROTATION_SIZE) = transform.translation();
  }

  virtual Transform3d getTransform(const InputType & x,
                                   const int & index) const
  {
    Eigen::Quaterniond rotation(x.segment<ROTATION_SIZE>(index).normalized());
    Eigen::Translation3d translation(x.segment<TRANSLATION_SIZE>(index + ROTATION_SIZE));
    return translation * rotation;
  }

};

class RGBDErrorFunctorR : public ErrorFunctor<double>
{
protected:

  typedef typename ErrorFunctor<double>::ValueType ValueType;
  typedef typename ErrorFunctor<double>::InputType InputType;

  enum
  {
    TRANSFORM_SIZE = 6,
    TRANSLATION_SIZE = 3,
    ROTATION_SIZE = TRANSFORM_SIZE - TRANSLATION_SIZE
  };

  RGBDErrorFunctorR()
    : ErrorFunctor<double>()
  {
    // Do nothing
  }

  RGBDErrorFunctorR(const int & inputs,
                    const int & values)
    : ErrorFunctor<double>(inputs, values)
  {
    // Do nothing
  }

  virtual ~RGBDErrorFunctorR()
  {
    // Do nothing
  }

  virtual int setTransform(InputType & x,
                           const int & index,
                           const Transform3d & transform) const
  {
    Eigen::AngleAxisd rotation(transform.linear());
    x.segment<ROTATION_SIZE>(index) = rotation.angle() * rotation.axis();
    x.segment<TRANSLATION_SIZE>(index + ROTATION_SIZE) = transform.translation();
  }

  virtual Transform3d getTransform(const InputType & x,
                                   const int & index) const
  {
    Eigen::AngleAxisd rotation(x.segment<ROTATION_SIZE>(index).norm(), x.segment<3>(index).normalized());
    Eigen::Translation3d translation(x.segment<TRANSLATION_SIZE>(index + ROTATION_SIZE));
    return translation * rotation;
  }

};

} /* namespace optimization */
} /* namespace calibration */
#endif /* CALIBRATION_RGBD_ERROR_FUNCTOR_H_ */
