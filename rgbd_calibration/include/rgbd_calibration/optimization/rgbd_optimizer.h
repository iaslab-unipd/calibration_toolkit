/*
 * rgbd_optimizer.h
 *
 *  Created on: Oct 19, 2012
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_RGBD_OPTIMIZER_H_
#define CALIBRATION_RGBD_OPTIMIZER_H_

#include <calibration_common/errors/image_reprojection.h>
#include <calibration_common/color/sensor.h>

#include <rgbd_calibration/optimization/rgbd_error_functor.h>
#include <rgbd_calibration/rgbd/rgbd_checkerboard.h>

#include <kinect/depth/undistortion_map.h>
#include <unsupported/Eigen/NonLinearOptimization>

//#define ALBERTO

namespace calibration
{
namespace optimization
{

template <typename PointT, typename FunctionT>
  class TransformOptimizer
  {
  public:

    typedef boost::shared_ptr<TransformOptimizer> Ptr;
    typedef boost::shared_ptr<const TransformOptimizer> ConstPtr;

    typedef typename FunctionT::Ptr FunctionPtr;

    struct OptimizationData
    {
      typedef boost::shared_ptr<OptimizationData> Ptr;
      typedef boost::shared_ptr<const OptimizationData> ConstPtr;

      OptimizationData(const RGBDCheckerboard<PointT> & rgbd_checkerboard,
                       const ImageReprojectionError::Ptr & error)
        : error_(error), checkerboard_(*rgbd_checkerboard.colorView()->object()),
          plane_points_(rgbd_checkerboard.depthView()->interestPoints()),
          color_checkerboard_(*rgbd_checkerboard.colorCheckerboard())
      {
        // Do nothing
      }

      const ImageReprojectionError::Ptr error_;
      const Checkerboard checkerboard_;
      const PointSet3d plane_points_;

      const Checkerboard color_checkerboard_;
      Plane3d color_plane_;

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    };

    typedef typename RGBDCheckerboard<PointT>::ConstPtr RGBDCheckerboardConstPtr;

    typedef typename OptimizationData::Ptr OptimizationDataPtr;
    typedef std::vector<OptimizationDataPtr> DataVector;

    typedef RGBDErrorFunctorQ RGBDErrorFunctor;

    typedef typename UndistortionMap<double, PointT>::ConstPtr UndistortionMapConstPtr;

    class ErrorFunctor : public RGBDErrorFunctor
    {

    public:

      /* typedefs */

      // Derived
      typedef typename RGBDErrorFunctor::ValueType ValueType;
      typedef typename RGBDErrorFunctor::InputType InputType;

      /* methods */

      ErrorFunctor(const ColorSensor::ConstPtr & color_sensor,
                   const DataVector & data_vec,
                   size_t function_coefficients);

      virtual
      ~ErrorFunctor();

      Eigen::VectorXd
      initialize(const Transform3d & color_sensor_pose,
                 const FunctionT & distance_function);

      void
      convertResult(const InputType & x,
                    Transform3d & color_sensor_pose,
                    std::vector<Transform3d> & cb_poses,
                    FunctionT & distance_function) const;

      int
      operator()(InputType & x,
                 ValueType & v) const;

    protected:

      ColorSensor::ConstPtr color_sensor_;
      DataVector data_vec_;

    };

    class ErrorFunctorInternal : public RGBDErrorFunctor
    {

    public:

      /* typedefs */

      // Derived
      typedef typename RGBDErrorFunctor::ValueType ValueType;
      typedef typename RGBDErrorFunctor::InputType InputType;

      /* methods */

      ErrorFunctorInternal(const OptimizationDataPtr & data);

      virtual
      ~ErrorFunctorInternal();

      Eigen::VectorXd
      initialize();

      Transform3d
      convertResult(const InputType & x);

      int
      operator()(InputType & x,
                 ValueType & v) const;

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    protected:

      OptimizationDataPtr data_;
      Transform3d base_transform_;

    };

    TransformOptimizer(const ColorSensor::Ptr & color_sensor,
                       const UndistortionMapConstPtr & distortion_map,
                       const FunctionPtr & distance_function);

    int
    addData(const RGBDCheckerboardConstPtr & rgbd_checkerboard);

    double
    error() const;

    bool
    optimize();

  protected:

    ColorSensor::Ptr color_sensor_;
    const UndistortionMapConstPtr distortion_map_;
    FunctionPtr distance_function_;

    DataVector data_vec_;

  };

template <typename PointT, typename FunctionT>
  TransformOptimizer<PointT, FunctionT>::ErrorFunctor::ErrorFunctor(const ColorSensor::ConstPtr & color_sensor,
                                                                    const DataVector & data_vec,
                                                                    size_t function_coefficients)
    : RGBDErrorFunctor(), color_sensor_(color_sensor), data_vec_(data_vec)
  {
    inputs_ = RGBDErrorFunctor::TRANSFORM_SIZE /* * (1 + data_vec_.size())*/+ function_coefficients;
    values_ = 0;

    for (size_t i = 0; i < data_vec_.size(); ++i)
#ifdef ALBERTO
      values_ += data_vec_[i]->plane_points_.size();
#else
      values_ += data_vec_[i]->error_->size();
#endif
  }

template <typename PointT, typename FunctionT>
  TransformOptimizer<PointT, FunctionT>::ErrorFunctor::~ErrorFunctor()
  {
    // Do nothing
  }

template <typename PointT, typename FunctionT>
  Eigen::VectorXd TransformOptimizer<PointT, FunctionT>::ErrorFunctor::initialize(const Transform3d & color_sensor_pose,
                                                                                  const FunctionT & distance_function)
  {
    Eigen::VectorXd x(Eigen::VectorXd::Zero(inputs_));
    setTransform(x, 0, color_sensor_pose);

    //for (unsigned int i = 0; i < data_vec_.size(); ++i)
    //  setTransform(x, (i + 1) * TRANSFORM_SIZE, Transform3d::Identity());

    x.tail<FunctionT::SIZE>() = distance_function.coefficients();

    return x;
  }

template <typename PointT, typename FunctionT>
  void TransformOptimizer<PointT, FunctionT>::ErrorFunctor::convertResult(const InputType & x,
                                                                          Transform3d & color_sensor_pose,
                                                                          std::vector<Transform3d> & cb_poses,
                                                                          FunctionT & distance_function) const
  {
    color_sensor_pose = getTransform(x, 0);
    //cb_poses.clear();
    //for (unsigned int i = 0; i < data_vec_.size(); ++i)
    //  cb_poses.push_back(getTransform(x, (i + 1) * TRANSFORM_SIZE));

    distance_function.coefficients() = x.template tail<FunctionT::SIZE>();
  }

template <typename PointT, typename FunctionT>
  int TransformOptimizer<PointT, FunctionT>::ErrorFunctor::operator ()(InputType & x,
                                                                       ValueType & v) const
  {
    std::vector<int> v_index(data_vec_.size());

    v_index[0] = 0;
    for (unsigned int i = 1; i < data_vec_.size(); ++i)
#ifdef ALBERTO
      v_index[i] = v_index[i - 1] + data_vec_[i - 1]->plane_points_.size();
#else
      v_index[i] = v_index[i - 1] + data_vec_[i - 1]->error_->size();
#endif

    if (ROTATION_SIZE == 4)
    {
      x.template head<ROTATION_SIZE>().normalize();
      //for (unsigned int i = 0; i < data_vec_.size(); ++i)
      //  x.template segment<ROTATION_SIZE>((i + 1) * TRANSFORM_SIZE).normalize();
    }

    Transform3d color_sensor_pose;
    std::vector<Transform3d> cb_poses;
    FunctionT function;
    convertResult(x, color_sensor_pose, cb_poses, function);

    Transform3d depth_sensor_pose = color_sensor_pose.inverse();

    double penalty = 1.0;

//    if (ROTATION_SIZE == 4) // Using quaternions
//    {
//      double norm = x.template head<ROTATION_SIZE>().norm() - 1.0;
//      penalty += norm * norm;
//    }

#pragma omp parallel for
    for (unsigned int i = 0; i < data_vec_.size(); ++i)
    {
#ifdef ALBERTO
      int error_size = data_vec_[i]->plane_points_.size();
#else
      int error_size = data_vec_[i]->error_->size();
#endif

      PointSet3d points = data_vec_[i]->plane_points_;

      for (unsigned int p = 0; p < points.size(); ++p)
        points[p] *= function.evaluate(points[p][2]) / points[p][2];

#ifdef ALBERTO

      PointSet3d camera_points(points);
      camera_points.transform(depth_sensor_pose);
      PointSet2d proj_points = color_sensor_->cameraModel()->project3dToPixel(camera_points);
      PointSet3d reproj_points = color_sensor_->cameraModel()->projectPixelTo3dRay(proj_points);

      for (int j = 0; j < data_vec_[i]->plane_points_.size(); ++j)
      {
        Line3d ray(Point3d::Zero(), reproj_points[j]);
        Point3d p = ray.direction() * ray.intersection(data_vec_[i]->color_checkerboard_.plane());
        v[v_index[i] + j] = (p - points[j]).squaredNorm();
      }

#else
      data_vec_[i]->color_plane_ = PlanarObject::fitPlane(points);
      data_vec_[i]->color_plane_.transform(depth_sensor_pose);

      Checkerboard color_cb = data_vec_[i]->color_checkerboard_;
      //color_cb.transform(cb_poses[i]);

      const Checkerboard & cb = data_vec_[i]->checkerboard_;

      Line3d ray0(Point3d::Zero(), color_cb(0, 0).normalized());
      Point3d origin = ray0.direction() * ray0.intersection(data_vec_[i]->color_plane_); // FIXME use intersectionPoint when available
      Line3d ray1(Point3d::Zero(), color_cb(color_cb.rows() - 1, 0).normalized());
      Point3d p1 = ray1.direction() * ray1.intersection(data_vec_[i]->color_plane_);
      Line3d ray2(Point3d::Zero(), color_cb(0, color_cb.cols() - 1).normalized());
      Point3d p2 = ray2.direction() * ray2.intersection(data_vec_[i]->color_plane_);

      Eigen::Vector3d x = (p1 - origin).normalized();
      Eigen::Vector3d y = (p2 - origin).normalized();
      y = (y - x * y.dot(x)).normalized();

      double d = (cb.center() - cb(0, 0)).norm();

      Line3d center_ray(Point3d::Zero(), color_cb.center().normalized());
      Point3d center = center_ray.direction() * center_ray.intersection(data_vec_[i]->color_plane_);

      origin = center + ((origin - center) / (origin - center).norm() * d);

      PointSet3d new_points(cb.cols(), cb.rows());

      for (int r = 0; r < cb.rows(); ++r)
      {
        double n = (cb(r, 0) - cb(0, 0)).norm();
        for (int c = 0; c < cb.cols(); ++c)
        {
          double m = (cb(0, c) - cb(0, 0)).norm();
          new_points(r, c) = x * n + y * m + origin;
        }
      }

      Transform3d t(Eigen::umeyama(cb.points().matrix(), new_points.matrix(), false));
      Checkerboard cb2(data_vec_[i]->checkerboard_, t);

      data_vec_[i]->error_->compute(cb2, v.template segment(v_index[i], error_size));

      double sum = 0.0;
      for (int j = 0; j < color_cb.size(); ++j)
      {
        Line3d ray(Point3d::Zero(), color_cb[j].normalized());
        Point3d pt = ray.direction() * ray.intersection(data_vec_[i]->color_plane_);
        sum += (pt - color_cb[j]).norm() / (color_cb[j][2]);
      }

#pragma omp critical
      {
        penalty *= std::exp(sum / color_cb.size());
        //ROS_WARN_STREAM(std::exp(sum / color_cb.size()));
      }
#endif

      //v.template segment(v_index[i], error_size) /= error_size;

    }

    v *= penalty;

    return 0;
  }

//template <typename PointT, typename FunctionT>
//  int TransformOptimizer<PointT, FunctionT>::ErrorFunctor::operator ()(InputType & x,
//                                                                       ValueType & v) const
//  {
//    std::vector<int> v_index(data_vec_.size());
//
//    v_index[0] = 0;
//    for (unsigned int i = 1; i < data_vec_.size(); ++i)
//      v_index[i] = v_index[i - 1] + data_vec_[i - 1]->error_->size();
//
////    if (ROTATION_SIZE == 4)
////      x.template head<ROTATION_SIZE>().normalize();
//
//    Transform3d color_sensor_pose;
//    FunctionT function;
//    convertResult(x, color_sensor_pose, function);
//
//    Transform3d depth_sensor_pose = color_sensor_pose.inverse();
//
//    double penalty = 1.0;
//
//    if (ROTATION_SIZE == 4) // Using quaternions
//    {
//      double norm = x.template head<ROTATION_SIZE>().norm() - 1.0;
//      penalty += norm * norm;
//    }
//
//#pragma omp parallel for
//    for (unsigned int i = 0; i < data_vec_.size(); ++i)
//    {
//      int error_size = data_vec_[i]->error_->size();
//      PointSet3d points = data_vec_[i]->plane_points_;
//
//      for (unsigned int p = 0; p < points.size(); ++p)
//        points[p] *= function.evaluate(points[p][2]) / points[p][2];
//
//      data_vec_[i]->color_plane_ = PlanarObject::fitPlane(points);
//      data_vec_[i]->color_plane_.transform(depth_sensor_pose);
//
//      ErrorFunctorInternal error_functor_int(data_vec_[i]);
//      Eigen::VectorXd x_int = error_functor_int.initialize();
//
//      Eigen::DenseIndex index;
//
//      int tries = 10;
//      int info;
//      do
//      {
//        info = Eigen::LevenbergMarquardt<ErrorFunctorInternal>::lmdif1(error_functor_int, x_int, &index);
//        Eigen::VectorXd v_int(error_functor_int.values());
//        error_functor_int(x_int, v_int);
//        //ROS_ERROR_STREAM("x = " << x_int.transpose() << ": error = " << v_int.sum());
//      }
//      while (info == 5 and --tries > 0);
//
//      if (info == 0)
//        throw std::runtime_error("Internal improper input parameters.");
//
//      Checkerboard c(data_vec_[i]->checkerboard_, error_functor_int.convertResult(x_int));
//
//      data_vec_[i]->color_checkerboard_ = c;
//
//      data_vec_[i]->error_->compute(c, v.template segment(v_index[i], error_size));
//      v.template segment(v_index[i], error_size) /= error_size;
//
//    }
//
//    v *= penalty;
//
//    return 0;
//  }

template <typename PointT, typename FunctionT>
  TransformOptimizer<PointT, FunctionT>::ErrorFunctorInternal::ErrorFunctorInternal(const OptimizationDataPtr & data)
    : RGBDErrorFunctor(3, data->color_checkerboard_.size()), data_(data)
  {

    const Checkerboard & color_cb = data->color_checkerboard_;
    const Checkerboard & cb = data->checkerboard_;

    Line3d ray0(Point3d::Zero(), color_cb(0, 0).normalized());
    Point3d origin = ray0.direction() * ray0.intersection(data->color_plane_); // FIXME use intersectionPoint when available
    Line3d ray1(Point3d::Zero(), color_cb(color_cb.rows() - 1, 0).normalized());
    Point3d p1 = ray1.direction() * ray1.intersection(data->color_plane_);
    Line3d ray2(Point3d::Zero(), color_cb(0, color_cb.cols() - 1).normalized());
    Point3d p2 = ray2.direction() * ray2.intersection(data->color_plane_);

//    Point3d p0 = plane_.projection(checkerboard(0, 0));
//    Point3d p1 = plane_.projection(checkerboard(checkerboard.rows() - 1, 0));
//    Point3d p2 = plane_.projection(checkerboard(0, checkerboard.cols() - 1));

    Eigen::Vector3d x = (p1 - origin).normalized();
    Eigen::Vector3d y = (p2 - origin).normalized();
    y = (y - x * y.dot(x)).normalized();

//    base_transform_.linear().col(0) = (p1 - origin).normalized();
//    base_transform_.linear().col(1) = ((p2 - origin)
//      - base_transform_.linear().col(0) * (p2 - origin).dot(base_transform_.linear().col(0))).normalized();
//    base_transform_.linear().col(2) = base_transform_.linear().col(0).cross(base_transform_.linear().col(1));
//
//    base_transform_.translation() = origin;

    PointSet3d new_points(cb.cols(), cb.rows());

    for (int r = 0; r < cb.rows(); ++r)
    {
      double n = (cb(r, 0) - cb(0, 0)).norm();
      for (int c = 0; c < cb.cols(); ++c)
      {
        double m = (cb(0, c) - cb(0, 0)).norm();
        new_points(r, c) = x * n + y * m + origin;
      }
    }

    base_transform_ = Eigen::umeyama(cb.points().matrix(), new_points.matrix(), false);

  }

template <typename PointT, typename FunctionT>
  TransformOptimizer<PointT, FunctionT>::ErrorFunctorInternal::~ErrorFunctorInternal()
  {
    // Do nothing
  }

template <typename PointT, typename FunctionT>
  Eigen::VectorXd TransformOptimizer<PointT, FunctionT>::ErrorFunctorInternal::initialize()
  {
    return Eigen::VectorXd::Zero(inputs_);
  }

template <typename PointT, typename FunctionT>
  Transform3d TransformOptimizer<PointT, FunctionT>::ErrorFunctorInternal::convertResult(const InputType & x)
  {
    Eigen::AngleAxisd rotation(x(0), Eigen::Vector3d::UnitZ());
    Eigen::Translation3d translation(x(1), x(2), 0);
    return base_transform_ * (translation * rotation);
  }

template <typename PointT, typename FunctionT>
  int TransformOptimizer<PointT, FunctionT>::ErrorFunctorInternal::operator()(InputType & x,
                                                                              ValueType & v) const
  {
    Eigen::AngleAxisd rotation(x(0), Eigen::Vector3d::UnitZ());
    Eigen::Translation3d translation(x(1), x(2), 0);

    Checkerboard c(data_->checkerboard_, base_transform_ * (translation * rotation));

    data_->error_->compute(c.corners(), v);

    return 0;
  }

template <typename PointT, typename FunctionT>
  TransformOptimizer<PointT, FunctionT>::TransformOptimizer(const ColorSensor::Ptr & color_sensor,
                                                            const UndistortionMapConstPtr & distortion_map,
                                                            const FunctionPtr & distance_function)
    : color_sensor_(color_sensor), distortion_map_(distortion_map), distance_function_(distance_function)
  {
    // Do nothing
  }

template <typename PointT, typename FunctionT>
  int TransformOptimizer<PointT, FunctionT>::addData(const RGBDCheckerboardConstPtr & rgbd_checkerboard)
  {
    ImageReprojectionError::Ptr error(
      new ImageReprojectionError(color_sensor_->cameraModel(), rgbd_checkerboard->colorView()->interestPoints()));

    OptimizationDataPtr data(new OptimizationData(*rgbd_checkerboard, error));
    data_vec_.push_back(data);
    return data_vec_.size();
  }

template <typename PointT, typename FunctionT>
  double TransformOptimizer<PointT, FunctionT>::error() const
  {
    ErrorFunctor error_functor(color_sensor_, data_vec_, distance_function_->size());
    Eigen::VectorXd x = error_functor.initialize(color_sensor_->pose(), *distance_function_);
    Eigen::VectorXd v(error_functor.values());
    error_functor(x, v);
    return v.sum() / v.size();
  }

template <typename PointT, typename FunctionT>
  bool TransformOptimizer<PointT, FunctionT>::optimize()
  {
    bool optimized;

    ErrorFunctor error_functor(color_sensor_, data_vec_, distance_function_->size());
    Eigen::VectorXd x = error_functor.initialize(color_sensor_->pose(), *distance_function_);
    Eigen::VectorXd v(error_functor.values());
    error_functor(x, v);
    Eigen::DenseIndex index;

#ifdef ALBERTO
    ROS_INFO_STREAM("ALBERTO");
#endif
    ROS_INFO_STREAM("x = " << x.transpose() << ": error = " << v.sum() / v.size());

    int tries = 50;
    int info;
    do
    {
      info = Eigen::LevenbergMarquardt<ErrorFunctor>::lmdif1(error_functor, x, &index);
      error_functor(x, v);
      ROS_INFO_STREAM(
        "x = " << x.head<7>().transpose() << " " << x.tail(distance_function_->size()).transpose() << ": error = " << v.sum() / v.size());
      // / data_vec_.size());
    }
    while (info == 5 and --tries > 0);

    if (info == 0)
      throw std::runtime_error("Improper input parameters.");

    optimized = (info >= 1 and info <= 5);

    if (optimized)
    {

      std::vector<Transform3d> cb_tfs;
      Transform3d color_sensor_pose;
      error_functor.convertResult(x, color_sensor_pose, cb_tfs, *distance_function_);
      color_sensor_->setPose(color_sensor_pose);

    }
//    std::cout << dist_map.matrix() << std::endl;

//    cv::Mat im(dist_map.height() * 10, dist_map.width() * 10, CV_8UC3);
//
//    for (int i = 0; i < dist_map.height(); ++i)
//      for (int j = 0; j < dist_map.width(); ++j)
//      {
//        float f = dist_map(i, j)[0];
//        for (int k1 = 0; k1 < 10; ++k1)
//          for (int k2 = 0; k2 < 10; ++k2)
//            im.at<cv::Vec3b>(i * 10 + k1, j * 10 + k2) = cv::Vec3b(f < 1 ? (char)(255 * ((1 - f) / 0.5)) : 255, 255,
//                                                                   f > 1 ? (char)(255 * ((f - 1) / 0.5)) : 255);
//      }
//    cv::imshow("DISTORTION", im);
//    cv::waitKey(10);
//    cv::waitKey(10);

//      if (info >= 1 and info <= 4)
//        break;
//    }

    return optimized;
  }

} /* namespace optimization */
} /* namespace calibration */
#endif /* CALIBRATION_RGBD_OPTIMIZER_H_ */
