/*
 * pointcloud_plane_extractor.h
 *
 *  Created on: Nov 27, 2012
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_PLANE_EXTRACTOR_H_
#define CALIBRATION_PLANE_EXTRACTOR_H_

#include <boost/make_shared.hpp>

#include <pcl/point_cloud.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/sample_consensus/sac_model_normal_plane.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/segmentation/extract_clusters.h>
//#include <pcl/segmentation/region_growing.h>

#include <calibration_common/base/typedefs.h>

namespace pcl_vis = pcl::visualization;
using pcl_vis::PointCloudColorHandlerGenericField;

namespace calibration
{

/* ****************** Class definition ****************** */

template <typename PointT>
  class PlaneExtractor
  {
  public:

    typedef boost::shared_ptr<PlaneExtractor> Ptr;
    typedef boost::shared_ptr<const PlaneExtractor> ConstPtr;

    typedef typename pcl::PointCloud<PointT>::ConstPtr PointCloudConstPtr;

    virtual ~PlaneExtractor()
    {
    }

    virtual void
    setInputCloud(const PointCloudConstPtr & cloud) = 0;

    virtual bool
    extract(pcl::IndicesPtr & plane_indices, // TODO make const
            Plane3d & plane,
            double & std_dev) = 0;

  };

/**
 *
 */
template <typename PointT>
  class PointPlaneExtractor : public PlaneExtractor<PointT>
  {
  public:

    struct PickingPoint
    {
      PickingPoint(PointT point,
                   double radius)
        : point_(point), radius_(radius)
      {
      }

      const PointT point_;
      const double radius_;
    };

    /* typedefs */

    typedef boost::shared_ptr<PointPlaneExtractor> Ptr;
    typedef boost::shared_ptr<const PointPlaneExtractor> ConstPtr;

    typedef pcl::PointCloud<PointT> PointCloud;
    typedef typename PointCloud::ConstPtr PointCloudConstPtr;

    typedef pcl::search::KdTree<PointT> KdTree;
    typedef typename KdTree::Ptr KdTreePtr;
    typedef pcl::NormalEstimationOMP<PointT, pcl::Normal> NormalEstimator;
    typedef pcl::SampleConsensusModelNormalPlane<PointT, pcl::Normal> ModelNormalPlane;

    /* methods */

    PointPlaneExtractor();

    virtual
    ~PointPlaneExtractor();

    void
    setRadius(double radius);

    virtual void
    setInputCloud(const PointCloudConstPtr & cloud);

    virtual void
    setPoint(const PointT & point);

    virtual void
    setPoint(const PickingPoint & picking_point);

    virtual bool
    extract(pcl::IndicesPtr & plane_indices, // TODO make const
            Plane3d & plane,
            double & std_dev);

  protected:

    /* variables */

    PointCloudConstPtr cloud_;
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals_;
    KdTreePtr cloud_tree_;

    PointT point_;

    double radius_;

  };

template <typename PointT>
  class PointPlaneExtractorGUI : public PointPlaneExtractor<PointT>
  {
  public:

    /* typedefs */

    typedef boost::shared_ptr<PointPlaneExtractorGUI> Ptr;
    typedef boost::shared_ptr<const PointPlaneExtractorGUI> ConstPtr;

    typedef pcl::PointCloud<PointT> PointCloud;
    typedef typename PointCloud::ConstPtr PointCloudConstPtr;

    typedef pcl::PointCloud<pcl::PointXYZL> PointCloudLabel;
    typedef typename PointCloudLabel::Ptr PointCloudLabelPtr;

    typedef boost::shared_ptr<pcl_vis::PCLVisualizer> PCLVisualizerPtr;
    typedef PointCloudColorHandlerGenericField<pcl::PointXYZL> ColorHandler;
    typedef boost::shared_ptr<ColorHandler> ColorHandlerPtr;
    typedef boost::signals2::connection Connection;

    typedef PointPlaneExtractor<PointT> Base;

    /* methods */

    /**
     *
     * @param viewer
     */
    PointPlaneExtractorGUI();

    /**
     *
     */
    virtual
    ~PointPlaneExtractorGUI();

//    bool
//    extractPlane(int index,
//                 Eigen::Hyperplane<double, 3> &plane,
//                 std::vector<int> &plane_indices,
//                 double &std_dev);

    virtual bool
    extract(pcl::IndicesPtr & plane_indices, // TODO make const
            Plane3d & plane,
            double & std_dev);

    virtual void
    setInputCloud(const PointCloudConstPtr & cloud);

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  protected:

    void
    pointPickingCallback(const pcl_vis::PointPickingEvent & event,
                         void * param);

    void
    keyboardCallback(const pcl_vis::KeyboardEvent & event,
                     void * param);

    /* variables */

    PCLVisualizerPtr viewer_;
    ColorHandlerPtr color_handler_;

    PointCloudLabelPtr cloud_label_;
    std::string cloud_name_;

    unsigned int current_index_;
    bool next_plane_;

    Plane3d last_plane_;
    pcl::IndicesPtr last_indices_;
    double last_std_dev_;
    Point3d last_clicked_point_;

  };

/* ****************** Methods implementation ****************** */

template <typename PointT>
  PointPlaneExtractor<PointT>::PointPlaneExtractor()
    : cloud_normals_(new pcl::PointCloud<pcl::Normal>()), cloud_tree_(new KdTree()), radius_(0.1)
  {
    // Do nothing
  }

template <typename PointT>
  PointPlaneExtractor<PointT>::~PointPlaneExtractor()
  {
    // Do nothing
  }

template <typename PointT>
  void PointPlaneExtractor<PointT>::setRadius(double radius)
  {
    radius_ = radius;
  }

template <typename PointT>
  void PointPlaneExtractor<PointT>::setInputCloud(const PointCloudConstPtr & cloud)
  {

    cloud_ = cloud;
    cloud_tree_->setInputCloud(cloud_);

    NormalEstimator normal_estimator;
    normal_estimator.setInputCloud(cloud_);
    normal_estimator.setSearchMethod(cloud_tree_);
    normal_estimator.setKSearch(100); // TODO setRadiusSearch
    //normal_estimator.setRadiusSearch(0.05f);
    normal_estimator.compute(*cloud_normals_);

  }

template <typename PointT>
  void PointPlaneExtractor<PointT>::setPoint(const PointT & point)
  {
    point_ = point;
  }

template <typename PointT>
  void PointPlaneExtractor<PointT>::setPoint(const PickingPoint & picking_point)
  {
    setRadius(picking_point.radius_);
    setPoint(picking_point.point_);
  }

template <typename PointT>
  bool PointPlaneExtractor<PointT>::extract(pcl::IndicesPtr & plane_indices,
                                            Plane3d & plane,
                                            double & std_dev)
  {
    if (not plane_indices)
      plane_indices = boost::make_shared<std::vector<int> >();

    pcl::PointIndices init_indices;
    std::vector<float> sqr_distances;
    cloud_tree_->radiusSearch(point_, radius_, init_indices.indices, sqr_distances);

    if (init_indices.indices.size() < 50)
    {
      ROS_ERROR_STREAM("Not enough points found near (" << point_.x << ", " << point_.y << ", " << point_.z << ")");
      return false;
    }

//    Eigen::Vector4f eigen_centroid;
//    pcl::compute3DCentroid(*cloud_, init_indices, eigen_centroid);
//    PointT centroid(eigen_centroid[0], eigen_centroid[1], eigen_centroid[2]);
//
//    std::vector<int> nn_indices(1);
//    std::vector<float> nn_sqr_distances(1);
//    cloud_tree_->nearestKSearch(centroid, 1, nn_indices, nn_sqr_distances);
//
//    int nn_index = nn_indices[0];
//    ROS_INFO_STREAM(
//      "Seed point: (" << cloud_->points[nn_index].x << ", " << cloud_->points[nn_index].y << ", "
//        << cloud_->points[nn_index].z << ")");

    Eigen::VectorXf coefficients(Eigen::VectorXf::Zero(4));
    ModelNormalPlane model(cloud_);
    model.setNormalDistanceWeight(0.2);
    model.setInputNormals(cloud_normals_);
    Eigen::VectorXf new_coefficients(4);
    model.optimizeModelCoefficients(init_indices.indices, coefficients, new_coefficients);
    if (coefficients == new_coefficients)
      return false;
    coefficients = new_coefficients;

    boost::shared_ptr<std::vector<int> > all_cloud_indices = model.getIndices();

    model.setIndices(init_indices.indices);
    std::vector<double> distances;
    model.getDistancesToModel(coefficients, distances);
    Eigen::VectorXd v = Eigen::VectorXd::Map(&distances[0], distances.size());
    std_dev = 0.0;

    if (v.size() > 0)
      std_dev = std::sqrt(v.cwiseAbs2().mean() - std::pow(v.mean(), 2));

    model.setIndices(all_cloud_indices);
    model.selectWithinDistance(coefficients, 5 * std_dev, *plane_indices);

    model.optimizeModelCoefficients(*plane_indices, coefficients, new_coefficients);
    if (coefficients == new_coefficients)
      return false;
    coefficients = new_coefficients;

    model.setNormalDistanceWeight(0.05);
    model.selectWithinDistance(coefficients, 5 * std_dev, *plane_indices);

//    pcl::RegionGrowing<PointT, pcl::Normal> region_grow;
//    region_grow.setMinClusterSize(cloud_->size() / 10);
//    region_grow.setMaxClusterSize(cloud_->size());
//
//    region_grow.setSearchMethod(cloud_tree_);
//    region_grow.setNumberOfNeighbours(64);
//    region_grow.setInputCloud(cloud_);
//    //region_grow.setIndices(plane_indices);
//    region_grow.setInputNormals(cloud_normals_);
//    region_grow.setSmoothnessThreshold(10.0 / 180.0 * M_PI);
//    region_grow.setCurvatureThreshold(0.07);
//
//    pcl::PointIndices cluster;
//    region_grow.getSegmentFromPoint(nn_index, cluster);

    std::vector<pcl::PointIndices> cluster_indices;
    pcl::EuclideanClusterExtraction<PointT> ec;
    ec.setClusterTolerance(radius_ / 8);
    ec.setMinClusterSize(plane_indices->size() / 8);
    ec.setMaxClusterSize(plane_indices->size());
    //ec.setSearchMethod(cloud_tree_);
    ec.setInputCloud(cloud_);
    ec.setIndices(plane_indices);
    ec.extract(cluster_indices);

    if (cluster_indices.empty())
      return false;

    size_t max_index = 0;
    size_t max_size = cluster_indices[0].indices.size();
    for (size_t i = 1; i < cluster_indices.size(); ++i)
    {
      if (cluster_indices[i].indices.size() > max_size)
      {
        max_size = cluster_indices[i].indices.size();
        max_index = i;
      }
    }

    *plane_indices = cluster_indices[max_index].indices;

//    Eigen::VectorXf coefficients(4);
//    ModelNormalPlane model(cloud_);
//    model.setInputNormals(cloud_normals_);
//    Eigen::VectorXf new_coefficients(4);
//    model.optimizeModelCoefficients(cluster.indices, coefficients, new_coefficients);
//    if (coefficients == new_coefficients)
//      return false;
//    coefficients = new_coefficients;
//
//    if (coefficients[3] > 0)
//      coefficients *= -1.0f;
//
//    *plane_indices = cluster.indices;

    //Plane3d plane;
    plane.normal()[0] = coefficients[0];
    plane.normal()[1] = coefficients[1];
    plane.normal()[2] = coefficients[2];
    plane.offset() = coefficients[3];

    //planar_object = boost::make_shared<PointCloudPlanarObject<PointT> >(cloud_, plane_indices, plane);

    return true;
  }

//template <typename PointT>
//  bool SimpleCheckerboardPlaneExtractor<PointT>::extract(const RGBCheckerboard &rgb_checkerboard,
//                                                         pcl::IndicesPtr &plane_indices,
//                                                         Plane3d &plane,
//                                                         double &std_dev)
//  {
//    Point3d center = rgb_to_d_transform_ * rgb_checkerboard.center();
//    PointT p;
//    p.x = center[0];
//    p.y = center[1];
//    p.z = center[2];
//    extractFromPoint(p, plane_indices, plane, std_dev);
//  }

template <typename PointT>
  void PointPlaneExtractorGUI<PointT>::setInputCloud(const PointCloudConstPtr & cloud)
  {
    cloud_label_ = boost::make_shared<pcl::PointCloud<pcl::PointXYZL> >();

    cloud_label_->header = cloud->header;
    cloud_label_->width = cloud->width;
    cloud_label_->height = cloud->height;
    cloud_label_->is_dense = cloud->is_dense;
    cloud_label_->sensor_origin_ = cloud->sensor_origin_;
    cloud_label_->sensor_orientation_ = cloud->sensor_orientation_;

    for (unsigned int i = 0; i < cloud->points.size(); ++i)
    {
      pcl::PointXYZL p;
      p.x = cloud->points[i].x;
      p.y = cloud->points[i].y;
      p.z = cloud->points[i].z;
      p.label = 0;
      cloud_label_->push_back(p);
    }

    Base::setInputCloud(cloud);
    current_index_ = 1;

    color_handler_ = ColorHandlerPtr(new ColorHandler(cloud_label_, "label"));
    if (viewer_)
      if (not viewer_->updatePointCloud<pcl::PointXYZL>(cloud_label_, *color_handler_, cloud_name_))
        viewer_->addPointCloud<pcl::PointXYZL>(cloud_label_, *color_handler_, cloud_name_);

    viewer_->addPointCloudNormals<pcl::PointXYZL, pcl::Normal>(cloud_label_, Base::cloud_normals_, 100, 0.1f,
                                                               "Normals");

  }

template <typename PointT>
  PointPlaneExtractorGUI<PointT>::PointPlaneExtractorGUI()
    : cloud_name_("Cloud"), current_index_(1), next_plane_(false)
  {
    viewer_ = PCLVisualizerPtr(new pcl_vis::PCLVisualizer("Select Checkerboard Planes"));
  }

template <typename PointT>
  PointPlaneExtractorGUI<PointT>::~PointPlaneExtractorGUI()
  {
    if (viewer_ and cloud_label_)
      viewer_->removePointCloud(cloud_name_);
  }

template <typename PointT>
  bool PointPlaneExtractorGUI<PointT>::extract(pcl::IndicesPtr & plane_indices,
                                               Plane3d & plane,
                                               double & std_dev)
  {
    if (not cloud_label_)
      throw std::runtime_error("Need to call setInputCloud(...) method first.");

    next_plane_ = false;

    Connection k_connection = viewer_->registerKeyboardCallback(&PointPlaneExtractorGUI::keyboardCallback, *this);

    Connection pp_connection = viewer_->registerPointPickingCallback(&PointPlaneExtractorGUI::pointPickingCallback,
                                                                     *this);

    while (not viewer_->wasStopped() and not next_plane_)
      viewer_->spinOnce(50);

    viewer_->spinOnce();

    if (next_plane_)
    {
      plane = last_plane_;
      plane_indices = last_indices_;
      std_dev = last_std_dev_;
      current_index_++;
    }

    k_connection.disconnect();
    pp_connection.disconnect();

    return next_plane_;
  }

template <typename PointT>
  void PointPlaneExtractorGUI<PointT>::pointPickingCallback(const pcl_vis::PointPickingEvent & event,
                                                            void * param)
  {
    for (unsigned int i = 0; i < cloud_label_->points.size(); ++i)
    {
      if (cloud_label_->points[i].label == current_index_)
        cloud_label_->points[i].label = 0;
    }

    pcl::PointXYZ p;
    event.getPoint(p.x, p.y, p.z);
    last_clicked_point_ << p.x, p.y, p.z;

    PointPlaneExtractor<PointT>::setPoint(p);
    PointPlaneExtractor<PointT>::extract(last_indices_, last_plane_, last_std_dev_);

    const std::vector<int> &indices = *last_indices_;
    for (unsigned int i = 0; i < indices.size(); ++i)
    {
      if (cloud_label_->points[indices[i]].label == 0)
        cloud_label_->points[indices[i]].label = current_index_;
    }

    //cloud_->points[event.getPointIndex()].label = current_index_;

    viewer_->updatePointCloud<pcl::PointXYZL>(cloud_label_, *color_handler_, cloud_name_);

  }

template <typename PointT>
  inline void PointPlaneExtractorGUI<PointT>::keyboardCallback(const pcl_vis::KeyboardEvent & event,
                                                               void * param)
  {
    if (event.getKeySym() == "space" and event.keyUp())
      next_plane_ = true;
  }

} /* namespace calibration */
#endif /* CALIBRATION_PLANE_EXTRACTOR_H_ */
