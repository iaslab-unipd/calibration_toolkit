/*
 * rgbd_calibration.h
 *
 *  Created on: Apr 2, 2013
 *      Author: Filippo Basso
 */

#ifndef RGBD_CALIBRATION_H_
#define RGBD_CALIBRATION_H_

#include <ros/ros.h>
#include <omp.h>
#include <algorithm>

#include <boost/thread.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/random.hpp>

#include <pcl/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/random_sample.h>

#include <opencv2/opencv.hpp>

#include <geometry_msgs/Pose.h>
#include <eigen_conversions/eigen_msg.h>

#include <calibration_common/base/math.h>
#include <calibration_common/depth/undistortion_map_multifit.h>
#include <calibration_common/algorithms/plane_to_plane_calibration.h>
#include <calibration_common/algorithms/point_on_plane_calibration.h>
#include <calibration_common/base/polynomial_multifit.h>

#include <rgbd_calibration/rgbd/rgbd_data_analyzer.h>
#include <rgbd_calibration/rgbd/rgbd_publisher.h>

#include <rgbd_calibration/optimization/rgb_checkerboard_pose_optimizer.h>
#include <rgbd_calibration/optimization/depth_plane_pose_optimizer.h>

#include <rgbd_calibration/optimization/rgbd_optimizer.h>
#include <rgbd_calibration/optimization/kinect_error_optimizer.h>

using namespace calibration::optimization;

namespace calibration
{

class CheckerboardDistanceConstraint : public Constraint<Checkerboard>
{

public:

  CheckerboardDistanceConstraint(double distance,
                                 const Point3d & from = Point3d::Zero())
    : distance_(distance), from_(from)
  {
    // Do nothing
  }

  virtual bool isValid(const Checkerboard & checkerboard) const
  {
    return (checkerboard.center() - from_).norm() <= distance_;
  }

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

protected:

  double distance_;
  Point3d from_;

};

template <typename PointT>
  struct OrderByDistance
  {
    typedef typename RGBDCheckerboard<PointT>::Ptr RGBDCheckerboardPtr;

    bool operator()(const RGBDCheckerboardPtr & lhs,
                    const RGBDCheckerboardPtr & rhs)
    {
      return lhs->colorCheckerboard()->points().matrix().rowwise().maxCoeff()[2]
        < rhs->colorCheckerboard()->points().matrix().rowwise().maxCoeff()[2];
      //return lhs->colorCheckerboard()->center()[2] < rhs->colorCheckerboard()->center()[2];
    }
  };

template <typename PointT, typename MathFunctionT>
  class RGBDCalibration
  {
  public:

    enum Status
    {
      ESTIMATING_INITIAL_TRANSFORM = 0,
      ESTIMATING_DISTORTION_MAP = 1,
      ESTIMATING_TRANSFORM = 2,
      OPTIMIZING = 4,
      CALIBRATION_ENDED = 8
    };

    /* typedefs */

    typedef boost::shared_ptr<RGBDCalibration> Ptr;
    typedef boost::shared_ptr<const RGBDCalibration> ConstPtr;

    typedef typename RGBDData<PointT>::ConstPtr RGBDConstPtr;
    typedef typename RGBDPublisher<PointT>::Ptr RGBDPublisherPtr;
    typedef typename RGBDCheckerboard<PointT>::Ptr RGBDCheckerboardPtr;
    typedef typename RGBDDataAnalyzer<PointT>::Ptr RGBDDataAnalyzerPtr;

    typedef typename pcl::PointCloud<PointT>::Ptr PointCloudPtr;
    typedef typename pcl::PointCloud<PointT>::ConstPtr PointCloudConstPtr;
    typedef typename DepthSensor<PointT>::Ptr DepthSensorPtr;

    typedef typename UndistortionMapMultifit<PointT>::Ptr UndistortionMapMultifitPtr;

    typedef typename TransformOptimizer<PointT, MathFunctionT>::Ptr OptimizerPtr;
    typedef typename KinectErrorOptimizer<PointT, MathFunctionT>::Ptr FunctionOptimizerPtr;

    /* methods */

    RGBDCalibration(const ColorSensor::Ptr & color_sensor,
                    const DepthSensorPtr & depth_sensor,
                    const std::vector<Checkerboard::ConstPtr> & cb_vec);

    virtual
    ~RGBDCalibration();

    void
    setPublisher(const RGBDPublisherPtr & rgbd_publisher);

    void
    setUndistortionMapMultifit(const UndistortionMapMultifitPtr & multifit);

    void
    setMathFunctionMultifit(const MathFunctionMultifit::Ptr & f_multifit);

    void
    setOptimizer(const OptimizerPtr & optimizer);

    void
    setFunctionOptimizer(const FunctionOptimizerPtr & optimizer);

    bool
    addData(const cv::Mat & image,
            const PointCloudConstPtr & cloud);

    void
    addTestData(const cv::Mat & image,
                const PointCloudConstPtr & cloud);

    void
    estimateInitialTransform(bool force = false);

    void
    estimateTransform(const MathFunctionT & function);

    void
    estimateDepthDistortion();

    void
    optimize();

    void
    publishData() const;

    void
    test(const Checkerboard::Ptr & checkerboard,
         const double & offset,
         int id) const;

  protected:

    void
    addRGBDData(const cv::Mat & image,
                const PointCloudConstPtr & cloud);

    void
    fillRGBDCheckerboardVector();

    /* variables */

    Status status_;

    ColorSensor::Ptr color_sensor_;
    DepthSensorPtr depth_sensor_;

    const std::vector<Checkerboard::ConstPtr> cb_vec_;

    RGBDPublisherPtr rgbd_publisher_;
    RGBDDataAnalyzerPtr data_analyzer_;

    UndistortionMapMultifitPtr dm_multifit_;
    MathFunctionMultifit::Ptr f_multifit_;

    PlaneToPlaneCalibration calibrator_; // Pose calibration

    OptimizerPtr optimizer_;
    FunctionOptimizerPtr f_optimizer_;

    std::vector<RGBDConstPtr> data_vec_;
    std::vector<RGBDConstPtr> test_vec_;

    std::vector<RGBDCheckerboardPtr> rgbd_cb_vec_;
    std::vector<RGBDCheckerboardPtr> und_rgbd_cb_vec_; // XXX Parallel arrays. Pay attention to indices!!!

    int n_data_;

    const size_t MIN_PAIRS;

  };

template <typename PointT, typename MathFunctionT>
  RGBDCalibration<PointT, MathFunctionT>::RGBDCalibration(const ColorSensor::Ptr & color_sensor,
                                                          const DepthSensorPtr & depth_sensor,
                                                          const std::vector<Checkerboard::ConstPtr> & cb_vec)
    : status_(ESTIMATING_INITIAL_TRANSFORM), color_sensor_(color_sensor), depth_sensor_(depth_sensor), cb_vec_(cb_vec),
      data_analyzer_(boost::make_shared<RGBDDataAnalyzer<PointT> >(cb_vec)), n_data_(0), MIN_PAIRS(100u)
  {
    // Do nothing
  }

template <typename PointT, typename MathFunctionT>
  RGBDCalibration<PointT, MathFunctionT>::~RGBDCalibration()
  {
    // Do nothing
  }

template <typename PointT, typename MathFunctionT>
  void RGBDCalibration<PointT, MathFunctionT>::setPublisher(const RGBDPublisherPtr & rgbd_publisher)
  {
    rgbd_publisher_ = rgbd_publisher;
  }

template <typename PointT, typename MathFunctionT>
  void RGBDCalibration<PointT, MathFunctionT>::setUndistortionMapMultifit(const UndistortionMapMultifitPtr & dm_multifit)
  {
    dm_multifit_ = dm_multifit;
  }

template <typename PointT, typename MathFunctionT>
  void RGBDCalibration<PointT, MathFunctionT>::setMathFunctionMultifit(const MathFunctionMultifit::Ptr & f_multifit)
  {
    f_multifit_ = f_multifit;
  }

template <typename PointT, typename MathFunctionT>
  void RGBDCalibration<PointT, MathFunctionT>::setOptimizer(const OptimizerPtr & optimizer)
  {
    optimizer_ = optimizer;
  }

template <typename PointT, typename MathFunctionT>
  void RGBDCalibration<PointT, MathFunctionT>::setFunctionOptimizer(const FunctionOptimizerPtr & f_optimizer)
  {
    f_optimizer_ = f_optimizer;
  }

template <typename PointT, typename MathFunctionT>
  void RGBDCalibration<PointT, MathFunctionT>::publishData() const
  {
    if (not rgbd_publisher_)
      return;

    rgbd_publisher_->publishTF(*depth_sensor_);
    rgbd_publisher_->publishTF(*color_sensor_);

    for (size_t i = 0; i < test_vec_.size(); i += 1)
      rgbd_publisher_->publish(*test_vec_[i]);

    for (size_t i = 0; i < rgbd_cb_vec_.size(); i += 1)
      rgbd_publisher_->publish(*rgbd_cb_vec_[i]);

    for (size_t i = 0; i < und_rgbd_cb_vec_.size(); i += 1)
      rgbd_publisher_->publish(*und_rgbd_cb_vec_[i], "und_");

//    for (unsigned int i = 0; i < rgbd_checkerboard_vec_.size(); ++i)
//      rgbd_publisher_->publish(*rgbd_checkerboard_vec_[i], *distortion_map_);

  }

template <typename PointT, typename MathFunctionT>
  bool RGBDCalibration<PointT, MathFunctionT>::addData(const cv::Mat & image,
                                                       const PointCloudConstPtr & cloud)
  {
    typename pcl::PointCloud<PointT>::Ptr new_cloud(boost::make_shared<pcl::PointCloud<PointT> >());
    std::vector<int> remapping;
    pcl::removeNaNFromPointCloud(*cloud, *new_cloud, remapping);

    pcl::RandomSample<PointT> random_sample;
    random_sample.setInputCloud(new_cloud);
    random_sample.setSample(new_cloud->size() / 4);
    random_sample.setSeed(rand());
    random_sample.filter(*new_cloud);

    RGBDConstPtr data(boost::make_shared<RGBDData<PointT> >(image, new_cloud, color_sensor_, depth_sensor_, ++n_data_));
    data_vec_.push_back(data);
    return true;
  }

template <typename PointT, typename MathFunctionT>
  void RGBDCalibration<PointT, MathFunctionT>::addTestData(const cv::Mat & image,
                                                           const PointCloudConstPtr & cloud)
  {
    typename pcl::PointCloud<PointT>::Ptr new_cloud(boost::make_shared<pcl::PointCloud<PointT> >());
    std::vector<int> remapping;
    pcl::removeNaNFromPointCloud(*cloud, *new_cloud, remapping);

    pcl::RandomSample<PointT> random_sample;
    random_sample.setInputCloud(new_cloud);
    random_sample.setSample(new_cloud->size() / 4);
    random_sample.setSeed(rand());
    random_sample.filter(*new_cloud);

    RGBDConstPtr data(
      boost::make_shared<RGBDData<PointT> >(image.clone(), new_cloud, color_sensor_, depth_sensor_,
                                            test_vec_.size() + 1));
    test_vec_.push_back(data);
  }

template <typename PointT, typename MathFunctionT>
  void RGBDCalibration<PointT, MathFunctionT>::fillRGBDCheckerboardVector()
  {

#pragma omp parallel for
    for (size_t i = 0; i < data_vec_.size(); ++i)
    {
      RGBDConstPtr & data = data_vec_[i];
      AutomaticCheckerboardFinder finder(data->image());

      int id = data->id() * cb_vec_.size();

      int added = 0;
      for (size_t c = 0; c < cb_vec_.size(); ++c)
      {
        const Checkerboard::ConstPtr & cb = cb_vec_[c];
        RGBDCheckerboardPtr rgbd_cb = boost::make_shared<RGBDCheckerboard<PointT> >(data, cb, id);

        PointSet2d image_corners;
        if (not finder.find(*cb, image_corners))
          continue;

        rgbd_cb->setImageCorners(image_corners);

        ImageCheckerboardPoseOptimizer image_optimizer(color_sensor_->cameraModel(), image_corners,
                                                       rgbd_cb->colorCheckerboard());
        image_optimizer.optimize();

#pragma omp critical
        {
          ++added;
          rgbd_cb_vec_.push_back(rgbd_cb);
        }
      }
      if (added == 0)
        ROS_WARN_STREAM("RGBD " << data->id() << ": No checkerboard found!");
    }
  }

template <typename PointT, typename MathFunctionT>
  void RGBDCalibration<PointT, MathFunctionT>::estimateInitialTransform(bool force)
  {
    Transform3d color_sensor_pose;
    CheckerboardDistanceConstraint distance_cst(2.0);

    size_t last_i = 0;

    for (size_t i = 0; i < data_vec_.size() and rgbd_cb_vec_.size() < MIN_PAIRS; ++i, ++last_i)
    {
      if (int added = data_analyzer_->analyze(data_vec_[i], rgbd_cb_vec_, force, distance_cst) > 0)
        ROS_INFO_STREAM("RGBD " << data_vec_[i]->id() << ": " << added << " checkerboard(s) found!");
      else
        ROS_WARN_STREAM("RGBD " << data_vec_[i]->id() << ": No checkerboard found!");
    }

    for (size_t j = 0; j < rgbd_cb_vec_.size(); ++j)
      calibrator_.addPair(rgbd_cb_vec_[j]->colorCheckerboard()->plane(), rgbd_cb_vec_[j]->depthPlane()->plane());
    color_sensor_pose = calibrator_.estimateTransform().inverse();

#pragma omp parallel for
    for (size_t i = last_i; i < data_vec_.size(); ++i)
    {
      std::vector<typename RGBDCheckerboard<PointT>::Ptr> rgbd_cb_vec_local;

      if (int added = data_analyzer_->analyze(data_vec_[i], color_sensor_pose, rgbd_cb_vec_local, force, distance_cst))
      {
        ROS_INFO_STREAM("RGBD " << data_vec_[i]->id() << ": " << added << " checkerboard(s) found!");
      }
      else
      {
        ROS_WARN_STREAM("RGBD " << data_vec_[i]->id() << ": No checkerboard found!");
        continue;
      }

#pragma omp critical
      {
        for (size_t j = 0; j < rgbd_cb_vec_local.size(); ++j)
        {
          rgbd_cb_vec_.push_back(rgbd_cb_vec_local[j]);
          calibrator_.addPair(rgbd_cb_vec_local[j]->colorCheckerboard()->plane(),
                              rgbd_cb_vec_local[j]->depthPlane()->plane());
        }
        color_sensor_pose = calibrator_.estimateTransform().inverse();
      }
    }

    color_sensor_->setPose(color_sensor_pose);

  }

template <typename PointT, typename MathFunctionT>
  void RGBDCalibration<PointT, MathFunctionT>::estimateTransform(const MathFunctionT & function)
  {
    Transform3d color_sensor_pose = color_sensor_->pose();

    PlaneToPlaneCalibration calibrator;
    PointOnPlaneCalibration pop_calibrator;

    for (size_t i = 0; i < und_rgbd_cb_vec_.size(); ++i)
    {
      PointSet3d plane_points = und_rgbd_cb_vec_[i]->depthView()->interestPoints();

      for (unsigned int p = 0; p < plane_points.size(); ++p)
      {
        plane_points[p] *= function.evaluate(plane_points[p][2]) / plane_points[p][2];
      }

      for (unsigned int p = 0; p < 1; ++p)
      {
        int k = rand() % plane_points.size();
        pop_calibrator.addPair(plane_points[k], und_rgbd_cb_vec_[i]->colorCheckerboard()->plane());
      }

      Plane3d plane = PlanarObject::fitPlane(plane_points);

      calibrator.addPair(und_rgbd_cb_vec_[i]->colorCheckerboard()->plane(), plane);
    }

    color_sensor_pose = calibrator.estimateTransform().inverse();
    pop_calibrator.estimateTransform().inverse();
    color_sensor_->setPose(color_sensor_pose);

  }

template <typename PointT, typename MathFunctionT>
  void RGBDCalibration<PointT, MathFunctionT>::estimateDepthDistortion()
  {

    if (rgbd_cb_vec_.empty())
      fillRGBDCheckerboardVector();

    std::sort(rgbd_cb_vec_.begin(), rgbd_cb_vec_.end(), OrderByDistance<PointT>());

//    int max = 0;
//    while (rgbd_cb_vec_[max]->colorCheckerboard()->center()[2] < 1.5)
//      ++max;
//
//    ROS_INFO_STREAM(max);
//
//    for (int i = 0; i < max; ++i)
//    {
//      int j = rand() % max;
//      RGBDCheckerboardPtr tmp = rgbd_cb_vec_[i];
//      rgbd_cb_vec_[i] = rgbd_cb_vec_[j];
//      rgbd_cb_vec_[j] = tmp;
//    }

    data_vec_.clear();

    typedef PolynomialFunction<double, 2, 1> PolynomialFunctionT;
    PolynomialFunctionT::Ptr poly_f(boost::make_shared<PolynomialFunctionT>());
    PolynomialMultifit<PolynomialFunctionT> poly_multifit(poly_f);

    for (size_t i = 0; i < rgbd_cb_vec_.size(); ++i)
    {
      RGBDCheckerboardPtr & rgbd_cb = rgbd_cb_vec_[i];
      const RGBDConstPtr & data = rgbd_cb->data();

      const Point3d color_center = color_sensor_->pose() * rgbd_cb->colorCheckerboard()->center();
      Point3d depth_center = color_center;
      depth_center[2] = poly_f->evaluate(depth_center[2]);

      ROS_INFO_STREAM("RGBD " << data->id() << ": Transformed depth: " << color_center[2] << " -> " << depth_center[2]);

      PointCloudPtr und_cloud(boost::make_shared<pcl::PointCloud<PointT> >(*data->cloud()));
      depth_sensor_->undistort(*und_cloud);

      typename PointPlaneExtractor<PointT>::Ptr plane_extractor = boost::make_shared<PointPlaneExtractor<PointT> >();
      plane_extractor->setInputCloud(und_cloud);
      double r = std::min(rgbd_cb->colorCheckerboard()->width(), rgbd_cb->colorCheckerboard()->height()) / 1.8;
      plane_extractor->setRadius(r);

      PointT p(depth_center[0], depth_center[1], depth_center[2]);
      plane_extractor->setPoint(p);

      double plane_std_dev;
      pcl::IndicesPtr plane_indices = boost::make_shared<std::vector<int> >();
      Plane3d plane;

      bool plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      if (not plane_extracted)
      {
        p.z += r;
        plane_extractor->setPoint(p);
        plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      }
      if (not plane_extracted)
      {
        p.z += r;
        plane_extractor->setPoint(p);
        plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      }
      if (not plane_extracted)
      {
        p.z -= 3 * r;
        plane_extractor->setPoint(p);
        plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      }
      if (not plane_extracted)
      {
        p.z -= r;
        plane_extractor->setPoint(p);
        plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      }

      if (plane_extracted)
      {
        ROS_INFO_STREAM("RGBD " << data->id() << ": Plane extracted!!");

        dm_multifit_->accumulateCloud(data->cloud(), *plane_indices);
        dm_multifit_->addAccumulatedPoints(PlanarObject::fitPlane(PointSet3d(*data->cloud(), *plane_indices)));
        dm_multifit_->update();

        for (int c = 0; c < rgbd_cb->colorCheckerboard()->size(); ++c)
        {
          const Point3d color_center = color_sensor_->pose() * (*rgbd_cb->colorCheckerboard())[c];
          Line3d ray(color_center, Point3d::UnitZ());
          Point3d depth_point = color_center + ray.direction() * ray.intersection(plane); // FIXME use intersectionPoint when available
          f_multifit_->addData(depth_point[2], color_center[2]);
//          std::cerr << "-1, " << depth_point[2] << "," << color_center[2] << ";" << std::endl;
        }

        Line3d ray(color_center, Point3d::UnitZ());
        Point3d depth_point = color_center + ray.direction() * ray.intersection(plane); // FIXME use intersectionPoint when available

        ROS_INFO_STREAM("RGBD " << data->id() << ": Real depth: " << depth_point[2]);

        double angle = 180.0 / M_PI * std::acos(-plane.normal().dot(rgbd_cb->colorCheckerboard()->plane().normal()));
        ROS_INFO_STREAM("RGBD " << data->id() << ": Angle: " << angle);

        for (int c = 0; c < rgbd_cb->colorCheckerboard()->size(); ++c)
        {
          const Point3d color_p = color_sensor_->pose() * (*rgbd_cb->colorCheckerboard())[c];
          Line3d ray(color_p, Point3d::UnitZ());
          Point3d depth_point = color_p + ray.direction() * ray.intersection(plane); // FIXME use intersectionPoint when available
          poly_multifit.addData(color_p[2], depth_point[2]);
        }
        if (i > 20)
          poly_multifit.update();
      }
      else
      {
        ROS_WARN_STREAM("RGBD " << data->id() << ": Plane not extracted!!");
      }
    }

#pragma omp parallel for
    for (size_t i = 0; i < rgbd_cb_vec_.size(); ++i)
    {
      RGBDCheckerboardPtr & rgbd_cb = rgbd_cb_vec_[i];
      const RGBDConstPtr & data = rgbd_cb->data();

      const Point3d color_center = color_sensor_->pose() * rgbd_cb->colorCheckerboard()->center();
      Point3d depth_center = color_center;
      depth_center[2] = poly_f->evaluate(depth_center[2]);

      PointCloudPtr und_cloud(boost::make_shared<pcl::PointCloud<PointT> >(*data->cloud()));

      pcl::RandomSample<PointT> random_sample;
      random_sample.setInputCloud(und_cloud);
      random_sample.setSample(und_cloud->size() / 2);
      random_sample.setSeed(rand());
      random_sample.filter(*und_cloud);
      depth_sensor_->undistort(*und_cloud);

      RGBDConstPtr und_data(
        boost::make_shared<RGBDData<PointT> >(data->image(), und_cloud, color_sensor_, depth_sensor_, data->id()));
      RGBDCheckerboardPtr und_rgbd_cb(
        boost::make_shared<RGBDCheckerboard<PointT> >(und_data, rgbd_cb->checkerboard(), rgbd_cb->id()));
      und_rgbd_cb->setImageCorners(rgbd_cb->colorView()->interestPoints());

      typename PointPlaneExtractor<PointT>::Ptr plane_extractor = boost::make_shared<PointPlaneExtractor<PointT> >();
      plane_extractor->setInputCloud(und_cloud);
      double r = std::min(rgbd_cb->colorCheckerboard()->width(), rgbd_cb->colorCheckerboard()->height()) / 1.8;
      plane_extractor->setRadius(r);

      PointT p(depth_center[0], depth_center[1], depth_center[2]);
      plane_extractor->setPoint(p);

      double plane_std_dev;
      pcl::IndicesPtr plane_indices = boost::make_shared<std::vector<int> >();
      Plane3d plane;

      bool plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      if (not plane_extracted)
      {
        p.z += r;
        plane_extractor->setPoint(p);
        plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      }
      if (not plane_extracted)
      {
        p.z += r;
        plane_extractor->setPoint(p);
        plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      }
      if (not plane_extracted)
      {
        p.z -= 3 * r;
        plane_extractor->setPoint(p);
        plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      }
      if (not plane_extracted)
      {
        p.z -= r;
        plane_extractor->setPoint(p);
        plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      }

      if (plane_extracted)
      {
        und_rgbd_cb->setPlaneInliers(*plane_indices);
#pragma omp critical
        {
          und_rgbd_cb_vec_.push_back(und_rgbd_cb);
        }
      }
      else
      {
        ROS_WARN_STREAM("RGBD " << data->id() << ": Plane not extracted!!");
      }
    }

    f_multifit_->update();
  }

template <typename PointT, typename MathFunctionT>
  void RGBDCalibration<PointT, MathFunctionT>::optimize()
  {
//    int n = 0;
    for (size_t i = 0; i < und_rgbd_cb_vec_.size(); ++i)
    {
//      if (und_rgbd_cb_vec_[i]->colorCheckerboard()->points().matrix().rowwise().maxCoeff()[2] < 2.5)
//      {
//        ++n;
      optimizer_->addData(und_rgbd_cb_vec_[i]);
      //f_optimizer_->addData(und_rgbd_cb_vec_[i]);
//      }
    }
//    ROS_INFO_STREAM("N = " << n);
    //f_optimizer_->optimize();
    optimizer_->optimize();
  }

template <typename PointT, typename MathFunctionT>
  void RGBDCalibration<PointT, MathFunctionT>::test(const Checkerboard::Ptr & checkerboard,
                                                    const double & offset,
                                                    int id) const
  {
    typedef std::map<int, std::pair<int, std::pair<double, double> > > MapT;
    MapT data_bins;

    for (size_t i = 0; i < test_vec_.size(); ++i)
    {
      const RGBDConstPtr & data = test_vec_[i];
      AutomaticCheckerboardFinder finder(data->image());

      int id = data->id() * cb_vec_.size();

      const Checkerboard::ConstPtr & cb = checkerboard;
      RGBDCheckerboardPtr rgbd_cb = boost::make_shared<RGBDCheckerboard<PointT> >(data, cb, id);

      PointSet2d image_corners;
      if (not finder.find(*cb, image_corners))
        continue;

      rgbd_cb->setImageCorners(image_corners);

      ImageCheckerboardPoseOptimizer image_optimizer(color_sensor_->cameraModel(), image_corners,
                                                     rgbd_cb->colorCheckerboard());
      image_optimizer.optimize();

      const Point3d color_center = color_sensor_->pose() * rgbd_cb->colorCheckerboard()->center();
      Point3d depth_center = color_center;

      PointCloudPtr und_cloud(boost::make_shared<pcl::PointCloud<PointT> >(*data->cloud()));

      pcl::RandomSample<PointT> random_sample;
      random_sample.setInputCloud(und_cloud);
      random_sample.setSample(und_cloud->size() / 2);
      random_sample.setSeed(rand());
      random_sample.filter(*und_cloud);
      depth_sensor_->undistort(*und_cloud);

      RGBDConstPtr und_data(
        boost::make_shared<RGBDData<PointT> >(data->image(), und_cloud, color_sensor_, depth_sensor_, data->id()));
      RGBDCheckerboardPtr und_rgbd_cb(
        boost::make_shared<RGBDCheckerboard<PointT> >(und_data, rgbd_cb->checkerboard(), rgbd_cb->id()));
      und_rgbd_cb->setImageCorners(rgbd_cb->colorView()->interestPoints());

      typename PointPlaneExtractor<PointT>::Ptr plane_extractor = boost::make_shared<PointPlaneExtractor<PointT> >();
      plane_extractor->setInputCloud(und_cloud);
      double r = std::min(rgbd_cb->colorCheckerboard()->width(), rgbd_cb->colorCheckerboard()->height()) / 1.8;
      plane_extractor->setRadius(r);

      PointT p(depth_center[0], depth_center[1], depth_center[2]);
      plane_extractor->setPoint(p);

      double plane_std_dev;
      pcl::IndicesPtr plane_indices = boost::make_shared<std::vector<int> >();
      Plane3d plane;

      bool plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      if (not plane_extracted)
      {
        p.z += r;
        plane_extractor->setPoint(p);
        plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      }
      if (not plane_extracted)
      {
        p.z += r;
        plane_extractor->setPoint(p);
        plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      }
      if (not plane_extracted)
      {
        p.z -= 3 * r;
        plane_extractor->setPoint(p);
        plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      }
      if (not plane_extracted)
      {
        p.z -= r;
        plane_extractor->setPoint(p);
        plane_extracted = plane_extractor->extract(plane_indices, plane, plane_std_dev);
      }

      if (plane_extracted)
      {
        und_rgbd_cb->setPlaneInliers(*plane_indices);
#pragma omp critical
        {
          Checkerboard color_cb = *und_rgbd_cb->colorCheckerboard();
          color_cb.transform(color_sensor_->pose());
          const Plane3d & depth_p = und_rgbd_cb->depthPlane()->plane();
          for (int i = 0; i < color_cb.size(); ++i)
          {
            int bin = int(color_cb[i][2] * 50);
            if (data_bins.find(bin) == data_bins.end())
              data_bins[bin] = std::make_pair(1, std::make_pair(color_cb[i][2], depth_p.absDistance(color_cb[i])));
            else
            {
              data_bins[bin].first++;
              data_bins[bin].second.first += color_cb[i][2];
              data_bins[bin].second.second += depth_p.absDistance(color_cb[i]);
            }
          }
        }
      }
    }
//    for (MapT::iterator it = data_bins.begin(); it != data_bins.end(); ++it)
//    {
//      std::cerr << id << ", " << it->second.second.first / it->second.first << ", "
//                << it->second.second.second / it->second.first << ";" << std::endl;
//    }
  }

} /* namespace calibration */
#endif /* RGBD_CALIBRATION_H_ */
