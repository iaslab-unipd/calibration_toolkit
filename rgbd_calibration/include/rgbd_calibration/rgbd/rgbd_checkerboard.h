/*
 * rgbd_checkerboard.h
 *
 *  Created on: Aug 23, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_RGBD_CHECKERBOARD_H_
#define CALIBRATION_RGBD_CHECKERBOARD_H_

#include <rgbd_calibration/rgbd/rgbd_data.h>
#include <calibration_common/color/view.h>
#include <calibration_common/depth/view.h>
#include <calibration_common/base/checkerboard.h>

namespace calibration
{

template <typename PointT>
  class RGBDCheckerboard
  {
  public:

    typedef boost::shared_ptr<RGBDCheckerboard> Ptr;
    typedef boost::shared_ptr<const RGBDCheckerboard> ConstPtr;

    typedef typename RGBDData<PointT>::ConstPtr RGBDDataConstPtr;

    typedef ColorView<Checkerboard> ColorViewT;
    typedef ColorViewT::Ptr ColorViewPtr;

    typedef DepthView<PlanarObject, PointT> DepthViewT;
    typedef typename DepthViewT::Ptr DepthViewPtr;

    RGBDCheckerboard(const typename RGBDData<PointT>::ConstPtr & data,
                     const Checkerboard::ConstPtr & checkerboard,
                     int id);

    virtual
    ~RGBDCheckerboard();

    void
    setImageCorners(const PointSet2d & image_corners);

    void
    setPlaneInliers(const std::vector<int> & plane_inliers);

    const Checkerboard::ConstPtr &
    checkerboard() const;

    const Checkerboard::Ptr &
    colorCheckerboard() const;

    const PlanarObject::Ptr &
    depthPlane() const;

    const ColorViewPtr &
    colorView() const;

    const DepthViewPtr &
    depthView() const;

    const int &
    id() const;

    const RGBDDataConstPtr &
    data() const;

    void
    draw(cv::Mat & image) const;

  protected:

    const int id_;
    const RGBDDataConstPtr data_;
    const Checkerboard::ConstPtr checkerboard_;

    ColorViewPtr color_view_;
    DepthViewPtr depth_view_;

    Checkerboard::Ptr color_checkerboard_;
    PlanarObject::Ptr depth_plane_;

  };

template <typename PointT>
  RGBDCheckerboard<PointT>::RGBDCheckerboard(const RGBDDataConstPtr & data,
                                             const Checkerboard::ConstPtr & checkerboard,
                                             int id)
    : id_(id), data_(data), checkerboard_(checkerboard)
  {
    // Do nothing
  }

template <typename PointT>
  RGBDCheckerboard<PointT>::~RGBDCheckerboard()
  {
    // Do nothing
  }

template <typename PointT>
  void RGBDCheckerboard<PointT>::setImageCorners(const PointSet2d & image_corners)
  {
    color_view_ = boost::make_shared<ColorViewT>(id_, image_corners, data_->colorSensor(), checkerboard_);
    color_checkerboard_ = boost::make_shared<Checkerboard>(*color_view_);
  }

template <typename PointT>
  void RGBDCheckerboard<PointT>::setPlaneInliers(const std::vector<int> & plane_inliers)
  {
    depth_view_ = boost::make_shared<DepthViewT>(id_, *data_->cloud(), plane_inliers, data_->depthSensor(),
                                                 checkerboard_);
    depth_plane_ = boost::make_shared<PlanarObject>(*depth_view_);
  }

template <typename PointT>
  const Checkerboard::ConstPtr & RGBDCheckerboard<PointT>::checkerboard() const
  {
    return checkerboard_;
  }

template <typename PointT>
  const Checkerboard::Ptr & RGBDCheckerboard<PointT>::colorCheckerboard() const
  {
    return color_checkerboard_;
  }

template <typename PointT>
  const PlanarObject::Ptr & RGBDCheckerboard<PointT>::depthPlane() const
  {
    return depth_plane_;
  }

template <typename PointT>
  const typename RGBDCheckerboard<PointT>::ColorViewPtr & RGBDCheckerboard<PointT>::colorView() const
  {
    return color_view_;
  }

template <typename PointT>
  const typename RGBDCheckerboard<PointT>::DepthViewPtr & RGBDCheckerboard<PointT>::depthView() const
  {
    return depth_view_;
  }

template <typename PointT>
  const int & RGBDCheckerboard<PointT>::id() const
  {
    return id_;
  }

template <typename PointT>
  const typename RGBDCheckerboard<PointT>::RGBDDataConstPtr & RGBDCheckerboard<PointT>::data() const
  {
    return data_;
  }

template <typename PointT>
  void RGBDCheckerboard<PointT>::draw(cv::Mat & image) const
  {
    cv::Size pattern_size(color_checkerboard_->rows(), color_checkerboard_->cols());
    std::vector<cv::Point2f> corners;
    for (int i = 0; i < color_view_->interestPoints().size(); ++i)
      corners.push_back(cv::Point2f(color_view_->interestPoints()[i][0], color_view_->interestPoints()[i][1]));

    cv::drawChessboardCorners(image, pattern_size, cv::Mat(corners), true);
  }

} /* namespace calibration */
#endif /* CALIBRATION_RGBD_CHECKERBOARD_H_ */
