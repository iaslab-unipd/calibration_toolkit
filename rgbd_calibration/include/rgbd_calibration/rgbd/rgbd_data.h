/*
 * rgbd.h
 *
 *  Created on: May 28, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_RGBD_H_
#define CALIBRATION_RGBD_H_

#include <ros/ros.h>

#include <omp.h>

#include <pcl/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <opencv2/opencv.hpp>

#include <calibration_common/calibration_common.h>

namespace calibration
{

template <typename PointT>
  class RGBDData
  {
  public:

    typedef boost::shared_ptr<RGBDData> Ptr;
    typedef boost::shared_ptr<const RGBDData> ConstPtr;

    typedef typename pcl::PointCloud<PointT>::Ptr PointCloudPtr;
    typedef typename pcl::PointCloud<PointT>::ConstPtr PointCloudConstPtr;

    typedef typename DepthSensor<PointT>::ConstPtr DepthSensorConstPtr;

    RGBDData(const cv::Mat & image,
             const PointCloudConstPtr & cloud,
             const ColorSensor::ConstPtr & color_sensor,
             const DepthSensorConstPtr & depth_sensor,
             int id);

    RGBDData(const RGBDData<PointT> & other);

    virtual
    ~RGBDData();

    virtual pcl::PointCloud<pcl::PointXYZRGB>::Ptr
    rgbCloud() const;

    const int &
    id() const;

    const cv::Mat &
    image() const;

    const PointCloudPtr &
    cloud() const;

    const ColorSensor::ConstPtr &
    colorSensor() const;

    const DepthSensorConstPtr &
    depthSensor() const;

  protected:

    const int id_;

    const cv::Mat image_;
    const PointCloudPtr cloud_;

    const pcl::PointCloud<pcl::PointXYZRGB>::Ptr rgb_cloud_;

    const ColorSensor::ConstPtr color_sensor_;
    const DepthSensorConstPtr depth_sensor_;

  };

template <typename PointT>
  RGBDData<PointT>::RGBDData(const RGBDData<PointT> & other)
    : image_(other.image().clone()), cloud_(new pcl::PointCloud<PointT>(*other.cloud())),
      rgb_cloud_(new pcl::PointCloud<pcl::PointXYZRGB>(*other.rgbCloud())), color_sensor_(other.colorSensor()),
      id_(other.id())
  {
    // Do nothing
  }

template <typename PointT>
  RGBDData<PointT>::~RGBDData()
  {
    // Do nothing
  }

template <typename PointT>
  RGBDData<PointT>::RGBDData(const cv::Mat & image,
                             const PointCloudConstPtr & cloud,
                             const ColorSensor::ConstPtr & color_sensor,
                             const DepthSensorConstPtr & depth_sensor,
                             int id)
    : image_(image.clone()), cloud_(new pcl::PointCloud<PointT>(*cloud)), color_sensor_(color_sensor),
      depth_sensor_(depth_sensor), id_(id), rgb_cloud_(new pcl::PointCloud<pcl::PointXYZRGB>)
  {
    rgb_cloud_->header.frame_id = cloud_->header.frame_id = depth_sensor_->frameId();

    for (size_t p_index = 0; p_index < cloud_->points.size(); ++p_index)
    {
      const PointT & point_xyz = cloud_->points[p_index];
//      if (point_xyz.x != point_xyz.x) // Remove NaNs
//      {
//        // TODO control if it is necessary to change rgb_cloud_->is_dense value
//        continue;
//      }
//
      pcl::PointXYZRGB point_xyzrgb;
      point_xyzrgb.x = point_xyz.x;
      point_xyzrgb.y = point_xyz.y;
      point_xyzrgb.z = point_xyz.z;
      point_xyzrgb.b = 100;
      point_xyzrgb.g = 100;
      point_xyzrgb.r = 100;

      rgb_cloud_->points.push_back(point_xyzrgb);
    }

  }

template <typename PointT>
  inline const int & RGBDData<PointT>::id() const
  {
    return id_;
  }

template <typename PointT>
  inline const cv::Mat & RGBDData<PointT>::image() const
  {
    return image_;
  }

template <typename PointT>
  inline const typename RGBDData<PointT>::PointCloudPtr & RGBDData<PointT>::cloud() const
  {
    return cloud_;
  }

template <typename PointT>
  inline const ColorSensor::ConstPtr & RGBDData<PointT>::colorSensor() const
  {
    return color_sensor_;
  }

template <typename PointT>
  inline const typename DepthSensor<PointT>::ConstPtr & RGBDData<PointT>::depthSensor() const
  {
    return depth_sensor_;
  }

template <typename PointT>
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr RGBDData<PointT>::rgbCloud() const
  {
    rgb_cloud_->header.stamp = ros::Time::now().toNSec();
    Transform3d t = color_sensor_->pose().inverse();

    pcl::PointCloud<pcl::PointXYZ> cloud_tmp(*cloud_);

    depth_sensor_->undistort(cloud_tmp);

#pragma omp parallel for
    for (unsigned int p_index = 0; p_index < cloud_tmp.points.size(); ++p_index)
    {
      pcl::PointXYZRGB & point_xyzrgb = rgb_cloud_->points[p_index];
      pcl::PointXYZ & point_xyz = cloud_tmp.points[p_index];

      point_xyzrgb.x = point_xyz.x;
      point_xyzrgb.y = point_xyz.y;
      point_xyzrgb.z = point_xyz.z;

      Point3d point_eigen;
      point_eigen << point_xyz.x, point_xyz.y, point_xyz.z;
      point_eigen = t * point_eigen;
      Point2d point_image = color_sensor_->cameraModel()->project3dToPixel(point_eigen);

      int x = static_cast<int>(point_image[0]);
      int y = static_cast<int>(point_image[1]);

      if (x >= 0 and x < image_.cols and y >= 0 and y < image_.rows)
      {
        cv::Point p(x, y);
        const cv::Vec3b & image_data = image_.at<cv::Vec3b>(p);
        point_xyzrgb.b = image_data[0];
        point_xyzrgb.g = image_data[1];
        point_xyzrgb.r = image_data[2];
      }
      else
      {
        point_xyzrgb.b = 100;
        point_xyzrgb.g = 100;
        point_xyzrgb.r = 100;
      }

    }

    return rgb_cloud_;
  }

} /* namespace calibration */
#endif /* CALIBRATION_RGBD_H_ */
