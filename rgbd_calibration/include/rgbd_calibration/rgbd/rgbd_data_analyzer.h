/*
 * rgbd_data_analyzer.h
 *
 *  Created on: Sep 4, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_RGBD_DATA_ANALYZER_H_
#define CALIBRATION_RGBD_DATA_ANALYZER_H_

#include <rgbd_calibration/plane_extractor.h>
#include <rgbd_calibration/interactive_checkerboard_finder.h>

#include <calibration_common/algorithms/automatic_checkerboard_finder.h>

#include <rgbd_calibration/rgbd/rgbd_data.h>
#include <rgbd_calibration/rgbd/rgbd_checkerboard.h>

namespace calibration
{

template <typename PointT>
  class RGBDDataAnalyzer
  {
  public:

    typedef boost::shared_ptr<RGBDDataAnalyzer> Ptr;
    typedef boost::shared_ptr<const RGBDDataAnalyzer> ConstPtr;

    typedef typename RGBDData<PointT>::ConstPtr RGBDConstPtr;
    typedef typename RGBDCheckerboard<PointT>::Ptr RGBDCheckerboardPtr;

    typedef typename pcl::PointCloud<PointT>::ConstPtr PointCloudConstPtr;
    typedef typename DepthSensor<PointT>::Ptr DepthSensorPtr;

    typedef DepthView<PlanarObject, PointT> DepthViewT;
    typedef typename DepthViewT::Ptr DepthViewPtr;

    RGBDDataAnalyzer(const std::vector<Checkerboard::ConstPtr> & cb_vec);

    virtual
    ~RGBDDataAnalyzer();

    int
    analyze(const RGBDConstPtr & data,
            const Transform3d & color_sensor_pose,
            std::vector<typename RGBDCheckerboard<PointT>::Ptr> & rgbd_cb_vec,
            bool force = false,
            const Constraint<Checkerboard> & cb_constraint = NoConstraint<Checkerboard>(),
            const Constraint<PlanarObject> & plane_constraint = NoConstraint<PlanarObject>()) const;

    int
    analyze(const RGBDConstPtr & data,
            std::vector<typename RGBDCheckerboard<PointT>::Ptr> & rgbd_cb_vec,
            bool force = false,
            const Constraint<Checkerboard> & cb_constraint = NoConstraint<Checkerboard>(),
            const Constraint<PlanarObject> & plane_constraint = NoConstraint<PlanarObject>()) const;

    std::vector<DepthViewPtr>
    analyzeDepth(const RGBDConstPtr & data,
                 const Point3d & point_on_plane) const;

    std::vector<Checkerboard::Ptr>
    analyzeImage(const RGBDConstPtr & data) const;

  protected:

    int
    analyze_(const RGBDConstPtr & data,
             const Transform3d & color_sensor_pose,
             std::vector<typename RGBDCheckerboard<PointT>::Ptr> & rgbd_cb_vec,
             bool interactive,
             bool force,
             const Constraint<Checkerboard> & cb_constraint,
             const Constraint<PlanarObject> & plane_constraint) const;

    /* variables */

    const std::vector<Checkerboard::ConstPtr> cb_vec_;

  };

template <typename PointT>
  RGBDDataAnalyzer<PointT>::RGBDDataAnalyzer(const std::vector<Checkerboard::ConstPtr> & cb_vec)
    : cb_vec_(cb_vec)
  {
    // Do nothing
  }

template <typename PointT>
  RGBDDataAnalyzer<PointT>::~RGBDDataAnalyzer()
  {
    // Do nothing
  }

//template <typename PointT>
//  void RGBDDataAnalyzer<PointT>::setData(const RGBDConstPtr & data)
//  {
//    data_ = data;
//  }

template <typename PointT>
  int RGBDDataAnalyzer<PointT>::analyze(const RGBDConstPtr & data,
                                        const Transform3d & color_sensor_pose,
                                        std::vector<typename RGBDCheckerboard<PointT>::Ptr> & rgbd_cb_vec,
                                        bool force,
                                        const Constraint<Checkerboard> & cb_constraint,
                                        const Constraint<PlanarObject> & plane_constraint) const
  {
    return analyze_(data, color_sensor_pose, rgbd_cb_vec, false, force, cb_constraint, plane_constraint);
  }

template <typename PointT>
  int RGBDDataAnalyzer<PointT>::analyze(const RGBDConstPtr & data,
                                        std::vector<typename RGBDCheckerboard<PointT>::Ptr> & rgbd_cb_vec,
                                        bool force,
                                        const Constraint<Checkerboard> & cb_constraint,
                                        const Constraint<PlanarObject> & plane_constraint) const
  {
    return analyze_(data, Transform3d::Identity(), rgbd_cb_vec, true, force, cb_constraint, plane_constraint);
  }

template <typename PointT>
  int RGBDDataAnalyzer<PointT>::analyze_(const RGBDConstPtr & data,
                                         const Transform3d & color_sensor_pose,
                                         std::vector<typename RGBDCheckerboard<PointT>::Ptr> & rgbd_cb_vec,
                                         bool interactive,
                                         bool force,
                                         const Constraint<Checkerboard> & cb_constraint,
                                         const Constraint<PlanarObject> & plane_constraint) const
  {
    int added = 0;

    cv::Mat image = data->image().clone();
    AutomaticCheckerboardFinder finder(image);

    typename PointPlaneExtractor<PointT>::Ptr plane_extractor;

    if (not interactive)
      plane_extractor = boost::make_shared<PointPlaneExtractor<PointT> >();
    else
      plane_extractor = boost::make_shared<PointPlaneExtractorGUI<PointT> >();

    plane_extractor->setInputCloud(data->cloud());

    int base_id = data->id() * cb_vec_.size();

    for (unsigned int c = 0; c < cb_vec_.size(); ++c)
    {
      const Checkerboard::ConstPtr & cb = cb_vec_[c];

      plane_extractor->setRadius(std::min(cb->width(), cb->height()) / 2.0);

      RGBDCheckerboardPtr rgbd_cb = boost::make_shared<RGBDCheckerboard<PointT> >(data, cb, base_id);

      // 1. Extract corners

      PointSet2d image_corners;
      if (not finder.find(*cb, image_corners))
      {
        if (force)
        {
          InteractiveCheckerboardFinder finder2(image);
          if (not finder2.find(*cb, image_corners))
            continue;
        }
        else
          continue;
      }

      rgbd_cb->setImageCorners(image_corners);

      if (not cb_constraint.isValid(*rgbd_cb->colorCheckerboard()))
        continue;

      // 2. Extract plane

      double plane_std_dev;
      pcl::IndicesPtr plane_indices = boost::make_shared<std::vector<int> >();
      Plane3d plane;

      if (interactive)
      {
        rgbd_cb->draw(image);
      }
      else
      {
        Point3d center = color_sensor_pose * rgbd_cb->colorCheckerboard()->center();
        PointT p;
        p.x = center[0];
        p.y = center[1];
        p.z = center[2];
        plane_extractor->setPoint(p);
      }

      if (not plane_extractor->extract(plane_indices, plane, plane_std_dev))
        continue;

      rgbd_cb->setPlaneInliers(*plane_indices);

      if (not plane_constraint.isValid(*rgbd_cb->depthPlane()))
        continue;

      rgbd_cb_vec.push_back(rgbd_cb);
      ++added;
    }

    return added;

  }

template <typename PointT>
  std::vector<Checkerboard::Ptr> RGBDDataAnalyzer<PointT>::analyzeImage(const RGBDConstPtr & data) const
  {
    std::vector<Checkerboard::Ptr> image_cb_vec;
    AutomaticCheckerboardFinder finder(data->image());

    int base_id = data->id() * cb_vec_.size();

    for (unsigned int c = 0; c < cb_vec_.size(); ++c)
    {
      const Checkerboard::ConstPtr & cb = cb_vec_[c];

      PointSet2d image_corners;
      if (not finder.find(*cb, image_corners))
        continue;

      Checkerboard::Ptr color_cb = boost::make_shared<Checkerboard>(
        ColorView<Checkerboard>(base_id, image_corners, data->colorSensor(), cb));

      image_cb_vec.push_back(color_cb);
    }

    return image_cb_vec;
  }

template <typename PointT>
  std::vector<typename RGBDDataAnalyzer<PointT>::DepthViewPtr> RGBDDataAnalyzer<PointT>::analyzeDepth(const RGBDConstPtr & data,
                                                                                                      const Point3d & point_on_plane) const
  {
    std::vector<DepthViewPtr> depth_view_vec;

    typename PointPlaneExtractor<PointT>::Ptr plane_extractor;
    plane_extractor = boost::make_shared<PointPlaneExtractor<PointT> >();
    plane_extractor->setInputCloud(data->cloud());

    for (unsigned int c = 0; c < cb_vec_.size(); ++c)
    {
      const Checkerboard::ConstPtr & cb = cb_vec_[c];

      plane_extractor->setPickingRadius(std::min(cb->width(), cb->height()) / 2.0);

      double plane_std_dev;
      pcl::IndicesPtr plane_indices = boost::make_shared<std::vector<int> >();
      Plane3d plane;

      PointT p;
      p.x = point_on_plane[0];
      p.y = point_on_plane[1];
      p.z = point_on_plane[2];
      plane_extractor->setPoint(p);

      if (not plane_extractor->extract(plane_indices, plane, plane_std_dev))
        continue;
      // TODO save plane_std_dev somewhere

      int id = data->id() * cb_vec_.size() + c;
      DepthViewPtr depth_view = boost::make_shared<DepthViewT>(id, *data->cloud(), plane_indices, *data->depthSensor(),
                                                               cb);

      depth_view_vec.push_back(depth_view);

    }

    return depth_view_vec;
  }

} /* namespace calibration */
#endif /* CALIBRATION_RGBD_DATA_ANALYZER_H_ */
