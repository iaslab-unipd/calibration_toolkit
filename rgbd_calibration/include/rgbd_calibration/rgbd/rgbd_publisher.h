/*
 * rgbd_publisher.h
 *
 *  Created on: May 28, 2013
 *      Author: Filippo Basso
 */

#ifndef CALIBRATION_RGBD_PUBLISHER_H_
#define CALIBRATION_RGBD_PUBLISHER_H_

#include <ros/ros.h>

#include <pcl_ros/point_cloud.h>

#include <visualization_msgs/MarkerArray.h>
#include <eigen_conversions/eigen_msg.h>
#include <tf/transform_broadcaster.h>
#include <cv_bridge/cv_bridge.h>

#include <rgbd_calibration/rgbd/rgbd_data.h>

namespace calibration
{

template <typename PointT>
  class RGBDPublisher
  {

    struct CheckerboardPublisherSet
    {
      ros::Publisher marker_pub_;
    };

    struct DataPublisherSet
    {
      ros::Publisher cloud_pub_;
      ros::Publisher rgbd_pub_;
    };

  public:

    typedef boost::shared_ptr<RGBDPublisher> Ptr;
    typedef boost::shared_ptr<const RGBDPublisher> ConstPtr;

    RGBDPublisher(ros::NodeHandle & node_handle);

    void
    publish(const RGBDCheckerboard<PointT> & rgbd,
            const std::string & prefix = "");

    void
    publish(const RGBDData<PointT> & rgbd);

    void
    publish(const PointSet3d & point_set,
            const BaseObject & frame);

    void
    publishTF(const BaseObject & object);

  protected:

    ros::NodeHandle node_handle_;
    ros::Publisher marker_pub_;
    tf::TransformBroadcaster tf_pub_;

    std::map<int, DataPublisherSet> d_pub_map_;
    std::map<std::string, CheckerboardPublisherSet> c_pub_map_;

  };

template <typename PointT>
  RGBDPublisher<PointT>::RGBDPublisher(ros::NodeHandle & node_handle)
    : node_handle_(node_handle)
  {
    marker_pub_ = node_handle_.advertise<visualization_msgs::Marker>("rgbd_markers", 0);
  }

template <typename PointT>
  void RGBDPublisher<PointT>::publishTF(const BaseObject & object)
  {
    geometry_msgs::TransformStamped transform_msg;
    object.toTF(transform_msg);
    tf_pub_.sendTransform(transform_msg);
  }

//template <typename PointT>
//  void RGBDPublisher<PointT>::publish(const RGBDCheckerboard<PointT> & rgbd_checkerboard,
//                                      const DistortionMap<double, PointT> & map)
//  {
//    publish(rgbd_checkerboard);
//
//    visualization_msgs::Marker cloud_marker;
//    cloud_marker.ns = "undist_cloud";
//
//    PointSet3d points = rgbd_checkerboard.depthView()->interestPoints();
//    map.undistort(points);
//
//    cloud_marker.header.stamp = ros::Time::now();
//    cloud_marker.header.frame_id = rgbd_checkerboard.depthView()->sensor()->frameId();
//    points.toMarker(cloud_marker);
//    cloud_marker.color.r = 0.0;
//    cloud_marker.color.g = 1.0;
//    cloud_marker.color.b = 0.0;
//
//    CheckerboardPublisherSet & pub_set = c_pub_map_[rgbd_checkerboard.id()];
//    pub_set.marker_pub_.publish(cloud_marker);
//  }

template <typename PointT>
  void RGBDPublisher<PointT>::publish(const RGBDCheckerboard<PointT> & rgbd_checkerboard,
                                      const std::string & prefix)
  {
    std::stringstream ss;
    ss << prefix << "checkerboard_" << rgbd_checkerboard.id();
    std::string ns = ss.str();

    if (c_pub_map_.find(ns) == c_pub_map_.end())
    {
      CheckerboardPublisherSet pub_set;

      std::stringstream sss;
      sss << ns << "/markers";
      pub_set.marker_pub_ = node_handle_.advertise<visualization_msgs::Marker>(sss.str(), 0);

      c_pub_map_[ns] = pub_set;
    }

    visualization_msgs::Marker checkerboard_marker;
    visualization_msgs::Marker cloud_marker;
    visualization_msgs::Marker d_plane_marker;

    checkerboard_marker.ns = "checkerboard";
    cloud_marker.ns = "cloud";
    d_plane_marker.ns = "d_plane";

    checkerboard_marker.id = cloud_marker.id = d_plane_marker.id = 0;

    CheckerboardPublisherSet & pub_set = c_pub_map_[ns];

    if (rgbd_checkerboard.colorCheckerboard())
    {
      rgbd_checkerboard.colorCheckerboard()->toMarker(checkerboard_marker);
      pub_set.marker_pub_.publish(checkerboard_marker);
    }

    if (rgbd_checkerboard.depthPlane())
    {
      rgbd_checkerboard.depthPlane()->toMarker(d_plane_marker);
      pub_set.marker_pub_.publish(d_plane_marker);
    }

    if (rgbd_checkerboard.depthView())
    {
      rgbd_checkerboard.depthView()->toMarker(cloud_marker);
      pub_set.marker_pub_.publish(cloud_marker);
    }
  }

template <typename PointT>
  void RGBDPublisher<PointT>::publish(const RGBDData<PointT> & rgbd)
  {
    std::stringstream ss;
    ss << "rgbd_" << rgbd.id();
    std::string ns = ss.str();

    if (d_pub_map_.find(rgbd.id()) == d_pub_map_.end())
    {
      DataPublisherSet pub_set;

      ss.str("");
      ss << ns << "/cloud";
      pub_set.cloud_pub_ = node_handle_.advertise<pcl::PointCloud<PointT> >(ss.str(), 0);

//        ss.str("");
//        ss << ns << "/image";
//        pub_set.image_pub_ = node_handle_.advertise<sensor_msgs::Image>(ss.str(), 0);

      ss.str("");
      ss << ns << "/rgbd";
      pub_set.rgbd_pub_ = node_handle_.advertise<pcl::PointCloud<pcl::PointXYZRGB> >(ss.str(), 0);

      d_pub_map_[rgbd.id()] = pub_set;
    }

    DataPublisherSet & pub_set = d_pub_map_[rgbd.id()];

    rgbd.cloud()->header.stamp = 0;
    pub_set.cloud_pub_.publish(*rgbd.cloud());
    pub_set.rgbd_pub_.publish(*rgbd.rgbCloud());

  }

} /* namespace calibration */
#endif /* CALIBRATION_RGBD_PUBLISHER_H_ */
