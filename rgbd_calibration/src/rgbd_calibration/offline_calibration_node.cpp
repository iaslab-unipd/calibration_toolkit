/*
 * offline_calibration_node.cpp
 *
 *  Created on: Nov 28, 2012
 *      Author: Filippo Basso
 */

#include <ros/ros.h>

#include <pcl/io/pcd_io.h>

#include <calibration_common/base/base.h>
#include <kinect/depth/depth.h>

#include <rgbd_calibration/rgbd/rgbd_calibration.h>
#include <rgbd_calibration/rgbd/rgbd_publisher.h>

#include <rgbd_calibration/optimization/rgbd_optimizer.h>
#include <rgbd_calibration/optimization/kinect_error_optimizer.h>

#include <omnicamera/omnicamera_info_manager.h>
#include <omnicamera/pose_estimator.h>

#include <calibration_common/pinhole/pose_estimator.h>
#include <camera_info_manager/camera_info_manager.h>

#include <calibration_msgs/CheckerboardMsg.h>
#include <calibration_msgs/CheckerboardArray.h>

using namespace camera_info_manager;
using namespace calibration_msgs;

static const int POLY_DEGREE = 1;
static const int POLY_MIN_DEGREE = 0;

static const int UM_POLY_DEGREE = 2;
static const int UM_POLY_MIN_DEGREE = 0;

namespace calibration
{

typedef PolynomialFunction<double, POLY_DEGREE, POLY_MIN_DEGREE> PolynomialFunctionT;
typedef PolynomialMultifit<PolynomialFunctionT> PolynomialMultifitT;
typedef KinectDepthDistortionModel<pcl::PointXYZ, PolynomialFunctionT> DistortionModelT;

typedef PolynomialFunction<double, UM_POLY_DEGREE, UM_POLY_MIN_DEGREE> UMPolynomialFunctionT;
typedef KinectPolynomialUndistortionMap<double, pcl::PointXYZ, UMPolynomialFunctionT> UndistortionMapT;
typedef KinectPolynomialUndistortionMapMultifit<double, pcl::PointXYZ, UMPolynomialFunctionT> UMMultifitT;

typedef TransformOptimizer<pcl::PointXYZ, PolynomialFunctionT> OptimizerT;
typedef KinectErrorOptimizer<pcl::PointXYZ, PolynomialFunctionT> FunctionOptimizerT;

class OfflineCalibrationNode
{
public:

  /* methods */
  OfflineCalibrationNode(ros::NodeHandle & node_handle);

  virtual
  ~OfflineCalibrationNode();

  bool
  initialize();

  void
  spin();

  static Checkerboard::Ptr
  createCheckerboard(const CheckerboardMsg::ConstPtr & msg,
                     int id);

  static Checkerboard::Ptr
  createCheckerboard(const CheckerboardMsg & msg,
                     int id);

protected:

  void
  checkerboardArrayCallback(const CheckerboardArray::ConstPtr & msg);

  bool
  waitForMessages() const;

  /* variables */

  ros::NodeHandle node_handle_;

  // TODO find another way to get checkerboards
  ros::Subscriber checkerboards_sub_;
  CheckerboardArray::ConstPtr checkerboard_array_msg_;

  ColorSensor::Type camera_type_;
  std::string camera_calib_url_;
  std::string camera_name_;

  int instances_;
  int starting_index_;

  std::string path_;
  std::string image_extension_;
  std::string image_filename_;
  std::string cloud_filename_;

  int max_threads_;

  BaseObject::ConstPtr world_;
  ColorSensor::Ptr color_sensor_;
  DepthSensor<pcl::PointXYZ>::Ptr depth_sensor_;

  bool estimate_depth_distortion_;
  int distortion_cols_;
  int distortion_rows_;

  DepthSensor<pcl::PointXYZ>::Type laser_type_;

  RGBDCalibration<pcl::PointXYZ, PolynomialFunctionT>::Ptr calibration_;
  RGBDPublisher<pcl::PointXYZ>::Ptr rgbd_publisher_;

  UndistortionMapT::Ptr k_distortion_map_;
  PolynomialFunctionT::Ptr k_distance_function_;

  OptimizerT::Ptr optimizer_;
};

OfflineCalibrationNode::OfflineCalibrationNode(ros::NodeHandle & node_handle)
  : node_handle_(node_handle), rgbd_publisher_(new RGBDPublisher<pcl::PointXYZ>(node_handle))
{
  checkerboards_sub_ = node_handle_.subscribe("checkerboard_array", 1,
                                              &OfflineCalibrationNode::checkerboardArrayCallback, this);

  if (not node_handle_.getParam("camera_calib_url", camera_calib_url_))
    ROS_FATAL("Missing \"camera_calib_url\" parameter!!");

  node_handle_.param("camera_name", camera_name_, std::string("camera"));

  std::string camera_type_string;
  node_handle_.param("camera_type", camera_type_string, std::string("pinhole"));

  if (camera_type_string == "pinhole")
    camera_type_ = ColorSensor::PINHOLE;
  else if (camera_type_string == "omnidirectional")
    camera_type_ = ColorSensor::OMNIDIRECTIONAL;
  else
    throw std::runtime_error("Unknown camera type. Use \"pinhole\" or \"omnidirectional\"");

  node_handle_.param("estimate_depth_distortion", estimate_depth_distortion_, true); // TODO default = false
  node_handle_.param("distortion/cols", distortion_cols_, 640);
  node_handle_.param("distortion/rows", distortion_rows_, 480);

  std::string laser_type_string;
  node_handle_.param("laser_type", laser_type_string, std::string("kinect"));

  if (laser_type_string == "kinect")
    laser_type_ = DepthSensor<pcl::PointXYZ>::KINECT;
  else if (laser_type_string == "laser")
    laser_type_ = DepthSensor<pcl::PointXYZ>::LASER;
  else
    throw std::runtime_error("Unknown laser type. Use \"kinect\" or \"laser\"");

  if (not node_handle_.getParam("path", path_))
    ROS_FATAL("Missing \"path\" parameter!!");

  if (path_[path_.size() - 1] != '/')
    path_.append("/");

  if (not node_handle_.getParam("instances", instances_))
    ROS_FATAL("Missing \"instances\" parameter!!");

  node_handle_.param("image_extension", image_extension_, std::string("png"));
  node_handle_.param("starting_index", starting_index_, 1);
  node_handle_.param("image_filename", image_filename_, std::string("image_"));
  node_handle_.param("cloud_filename", cloud_filename_, std::string("cloud_"));

  node_handle_.param("max_threads", max_threads_, 8);

}

OfflineCalibrationNode::~OfflineCalibrationNode()
{
  // Do nothing
}

bool OfflineCalibrationNode::initialize()
{
  if (not waitForMessages())
    return false;

  std::vector<Checkerboard::ConstPtr> cb_vec;
  for (unsigned int i = 0; i < checkerboard_array_msg_->checkerboards.size(); ++i)
    cb_vec.push_back(createCheckerboard(checkerboard_array_msg_->checkerboards[i], i));

  ColorCameraModel::ConstPtr camera_model;
  PoseEstimator::ConstPtr pose_estimator;

  if (camera_type_ == ColorSensor::OMNIDIRECTIONAL)
  {
    OmnicameraInfoManager manager(node_handle_, camera_name_, camera_calib_url_);
    OmnicameraModel::ConstPtr omnicamera_model(boost::make_shared<OmnicameraModel>(manager.getOmnicameraInfo()));
    pose_estimator = PoseEstimator::ConstPtr(boost::make_shared<OmnicameraPoseEstimator>(omnicamera_model));
    camera_model = omnicamera_model;
  }
  else
  {
    CameraInfoManager manager(node_handle_, camera_name_, camera_calib_url_);
    PinholeCameraModel::ConstPtr pinhole_model(boost::make_shared<PinholeCameraModel>(manager.getCameraInfo()));
    pose_estimator = PoseEstimator::ConstPtr(boost::make_shared<PinholePoseEstimator>(pinhole_model));
    camera_model = pinhole_model;
  }

//  UndistortionMap<double, pcl::PointXYZ>::Ptr distortion_map =
//    boost::make_shared<NoUndistortionMap<double, pcl::PointXYZ> >();
//
//  if (laser_type_ == DepthSensor<pcl::PointXYZ>::KINECT and estimate_depth_distortion_)
//  {

  k_distortion_map_ = boost::make_shared<UndistortionMapT>(distortion_rows_, distortion_cols_);
  k_distance_function_ = boost::make_shared<PolynomialFunctionT>();
  DistortionModelT::Ptr k_distortion_model = boost::make_shared<DistortionModelT>(k_distortion_map_, k_distance_function_);

//    distortion_map = k_distortion_map;
//  }

  world_ = boost::make_shared<BaseObject>("/world");
  depth_sensor_ = boost::make_shared<DepthSensor<pcl::PointXYZ> >(laser_type_, k_distortion_model, "/depth_sensor",
                                                                  world_);
  color_sensor_ = boost::make_shared<ColorSensor>(camera_type_, camera_model, pose_estimator, "/color_sensor",
                                                  depth_sensor_);

  Transform3d pose(Eigen::Translation3d(0.1, 0.1, 0.1));
  color_sensor_->setPose(pose);

  UMMultifitT::Ptr dm_multifit = boost::make_shared<UMMultifitT>(k_distortion_map_);
  PolynomialMultifitT::Ptr poly_multifit = boost::make_shared<PolynomialMultifitT>(k_distance_function_);

  calibration_ = boost::make_shared<RGBDCalibration<pcl::PointXYZ, PolynomialFunctionT> >(color_sensor_, depth_sensor_, cb_vec);
  calibration_->setPublisher(rgbd_publisher_);
  calibration_->setUndistortionMapMultifit(dm_multifit);
  calibration_->setMathFunctionMultifit(poly_multifit);

  optimizer_ = boost::make_shared<OptimizerT>(color_sensor_, k_distortion_map_, k_distance_function_);
  calibration_->setOptimizer(optimizer_);

  FunctionOptimizerT::Ptr f_optimizer = boost::make_shared<FunctionOptimizerT>(color_sensor_, k_distance_function_);
  calibration_->setFunctionOptimizer(f_optimizer);

  return true;
}

void OfflineCalibrationNode::checkerboardArrayCallback(const CheckerboardArray::ConstPtr & msg)
{
  checkerboard_array_msg_ = msg;
}

bool OfflineCalibrationNode::waitForMessages() const
{
  ros::Rate rate(1.0);
  ros::spinOnce();
  while (ros::ok() and (not checkerboard_array_msg_))
  {
    ROS_WARN("Not all messages received!");
    rate.sleep();
    ros::spinOnce();
  }
  return checkerboard_array_msg_;
}

void OfflineCalibrationNode::spin()
{
  ros::Rate rate(10.0);

  int added = 0;

  ROS_INFO("Getting data...");
  for (int i = starting_index_; ros::ok() and i < starting_index_ + instances_; ++i)
  {
    std::stringstream image_file;
    image_file << path_ << image_filename_ << i << "." << image_extension_;

    std::stringstream cloud_file;
    cloud_file << path_ << cloud_filename_ << i << ".pcd";

    cv::Mat image = cv::imread(image_file.str());

    if (not image.data)
      continue;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PCDReader pcd_reader;

    if (pcd_reader.read(cloud_file.str(), *cloud) < 0)
      continue;

    if (calibration_->addData(image, cloud))
      ++added;

    //if (i % 10 == 0)
      calibration_->addTestData(image, cloud);

    //rate.sleep();
  }
  ROS_INFO_STREAM("Read " << added << " RGBD files.");

  //calibration_->test(createCheckerboard(checkerboard_array_msg_->checkerboards[0], 0), 0.0, 1);

  ROS_INFO("Estimating initial transform...");
  if (laser_type_ != DepthSensor<pcl::PointXYZ>::KINECT)
    calibration_->estimateInitialTransform();

  geometry_msgs::Pose pose_msg;
  tf::poseEigenToMsg(color_sensor_->pose(), pose_msg);
  ROS_INFO_STREAM("Estimated transform:\n" << pose_msg);

  ROS_INFO("Estimating depth distortion...");
  calibration_->estimateDepthDistortion();
  ROS_INFO("Done");

  calibration_->publishData();

  //calibration_->test(createCheckerboard(checkerboard_array_msg_->checkerboards[0], 0), 0.0, 2);

  calibration_->estimateTransform(*k_distance_function_);
  //calibration_->publishData();

  tf::poseEigenToMsg(color_sensor_->pose(), pose_msg);
  ROS_INFO_STREAM("Re-estimated transform:\n" << pose_msg);

  //calibration_->test(createCheckerboard(checkerboard_array_msg_->checkerboards[0], 0), 0.0, 2);

  calibration_->optimize();

  tf::poseEigenToMsg(color_sensor_->pose(), pose_msg);
  ROS_INFO_STREAM("Optimized transform:\n" << pose_msg);

  calibration_->estimateTransform(*k_distance_function_);
  optimizer_->optimize();

  tf::poseEigenToMsg(color_sensor_->pose(), pose_msg);
  ROS_INFO_STREAM("Re-optimized transform:\n" << pose_msg);


  //calibration_->test(createCheckerboard(checkerboard_array_msg_->checkerboards[0], 0), 0.0, 3);

  //std::cerr << "-------------------" << std::endl;
  //std::cerr << *k_distortion_map_ << std::endl;

  rate = ros::Rate(1.0);

  while (ros::ok())
  {
    calibration_->publishData();
    rate.sleep();
  }

}

Checkerboard::Ptr OfflineCalibrationNode::createCheckerboard(const CheckerboardMsg::ConstPtr & msg,
                                                             int id)
{
  return createCheckerboard(*msg, id);
}

Checkerboard::Ptr OfflineCalibrationNode::createCheckerboard(const CheckerboardMsg & msg,
                                                             int id)
{
  std::stringstream ss;
  ss << "/checkerboard_" << id;
  return boost::make_shared<Checkerboard>(msg.rows, msg.cols, msg.cell_width, msg.cell_height, ss.str());
}

} /* namespace calibration */

using namespace calibration;

int main(int argc,
         char ** argv)
{
  ros::init(argc, argv, "rgbd_offline_calibration");
  ros::NodeHandle node_handle("~");

  try
  {
    OfflineCalibrationNode calib_node(node_handle);
    if (not calib_node.initialize())
      return 0;
    calib_node.spin();

//    ROS_INFO("Calibration completed. Press ctrl-c to exit.");
    ros::spin();
  }
  catch (std::runtime_error & error)
  {
    ROS_FATAL("Calibration error: %s", error.what());
    return 1;
  }

  return 0;
}

