/*
 * calibration_node.cpp
 *
 *  Created on: Nov 28, 2012
 *      Author: Filippo Basso
 */

#include <ros/ros.h>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <std_msgs/String.h>

#include <calibration/rgbd/rgbd_calibration.h>
#include <calibration/rgbd/rgbd_publisher.h>

#include <calibration_common/objects/kinect_multifit_distorsion_map.h>

#include <omnicamera/omnicamera_info_manager.h>
#include <omnicamera/omnicamera_pose_estimator.h>

#include <calibration_common/algorithms/pinhole_pose_estimator.h>
#include <camera_info_manager/camera_info_manager.h>

#include <calibration/CheckerboardArray.h>
#include <calibration/CheckerboardMsg.h>

using namespace camera_info_manager;
using namespace calibration::rgbd;
using namespace omnicamera;
using namespace calibration_common;

namespace calibration
{

class OnlineCalibrationNode
{
public:

  /* methods */
  OnlineCalibrationNode(ros::NodeHandle & node_handle);

  virtual
  ~OnlineCalibrationNode();

  bool
  initialize();

  void
  spin();

  static Checkerboard::Ptr
  createCheckerboard(const CheckerboardMsg::ConstPtr & msg,
                     int id);

  static Checkerboard::Ptr
  createCheckerboard(const CheckerboardMsg & msg,
                     int id);

protected:

  void
  save(const cv::Mat & image,
       const pcl::PointCloud<pcl::PointXYZ>::ConstPtr & cloud);

  void
  pointCloudCallback(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr & msg);

  void
  imageCallback(const sensor_msgs::Image::ConstPtr & msg);

  void
  cameraInfoCallback(const sensor_msgs::CameraInfo::ConstPtr & msg);

  void
  omnicameraInfoCallback(const omnicamera::OmnicameraInfo::ConstPtr & msg);

  void
  actionCallback(const std_msgs::String::ConstPtr & msg);

  void
  checkerboardArrayCallback(const CheckerboardArray::ConstPtr & msg);

  bool
  waitForMessages() const;

  /* variables */

  ros::NodeHandle node_handle_;
  image_transport::ImageTransport image_transport_;

  ros::Subscriber cloud_sub_;
  image_transport::Subscriber image_sub_;
  ros::Subscriber camera_info_sub_;
  ros::Subscriber omnicamera_info_sub_;
  ros::Subscriber action_sub_;

  pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud_msg_;
  sensor_msgs::Image::ConstPtr image_msg_;
  sensor_msgs::CameraInfo::ConstPtr camera_info_msg_;
  omnicamera::OmnicameraInfo::ConstPtr omnicamera_info_msg_;

  // TODO find another way to get checkerboards
  ros::Subscriber checkerboards_sub_;
  CheckerboardArray::ConstPtr checkerboard_array_msg_;

  ColorSensor::Type camera_type_;

  int max_threads_;

  BaseObject::ConstPtr world_;
  ColorSensor::Ptr color_sensor_;
  DepthSensor<pcl::PointXYZ>::Ptr depth_sensor_;

  bool estimate_depth_distortion_;
  int distortion_cols_;
  int distortion_rows_;

  DepthSensor<pcl::PointXYZ>::Type laser_type_;

  RGBDCalibration<pcl::PointXYZ>::Ptr calibration_;
  RGBDPublisher<pcl::PointXYZ>::Ptr rgbd_publisher_;

  int starting_index_;
  std::string save_folder_;
  int file_index_;

};

OnlineCalibrationNode::OnlineCalibrationNode(ros::NodeHandle & node_handle)
  : node_handle_(node_handle), image_transport_(node_handle),
    rgbd_publisher_(new RGBDPublisher<pcl::PointXYZ>(node_handle))
{
  cloud_sub_ = node_handle.subscribe("point_cloud", 1, &OnlineCalibrationNode::pointCloudCallback, this);
  image_sub_ = image_transport_.subscribe("image", 1, &OnlineCalibrationNode::imageCallback, this);
  camera_info_sub_ = node_handle.subscribe("camera_info", 1, &OnlineCalibrationNode::cameraInfoCallback, this);
  omnicamera_info_sub_ = node_handle.subscribe("omnicamera_info", 1, &OnlineCalibrationNode::omnicameraInfoCallback,
                                               this);
  checkerboards_sub_ = node_handle_.subscribe("checkerboard_array", 1,
                                              &OnlineCalibrationNode::checkerboardArrayCallback, this);
  action_sub_ = node_handle.subscribe("action", 1, &OnlineCalibrationNode::actionCallback, this);

  std::string camera_type_string;
  node_handle_.param("camera_type", camera_type_string, std::string("pinhole"));

  if (camera_type_string == "pinhole")
    camera_type_ = ColorSensor::PINHOLE;
  else if (camera_type_string == "omnidirectional")
    camera_type_ = ColorSensor::OMNIDIRECTIONAL;
  else
    throw std::runtime_error("Unknown camera type. Use \"pinhole\" or \"omnidirectional\"");

  node_handle_.param("estimate_depth_distortion", estimate_depth_distortion_, true); // TODO default = false
  node_handle_.param("distortion/cols", distortion_cols_, 64);
  node_handle_.param("distortion/rows", distortion_rows_, 64);

  std::string laser_type_string;
  node_handle_.param("laser_type", laser_type_string, std::string("kinect"));

  if (laser_type_string == "kinect")
    laser_type_ = DepthSensor<pcl::PointXYZ>::KINECT;
  else if (laser_type_string == "laser")
    laser_type_ = DepthSensor<pcl::PointXYZ>::LASER;
  else
    throw std::runtime_error("Unknown laser type. Use \"kinect\" or \"laser\"");

  node_handle_.param("starting_index", starting_index_, 1);
  if (not node_handle_.getParam("save_folder", save_folder_))
    ROS_FATAL("Missing folder!!");

  if (save_folder_.at(save_folder_.size() - 1) != '/')
    save_folder_.append("/");

  file_index_ = starting_index_;

  node_handle_.param("max_threads", max_threads_, 8);

}

OnlineCalibrationNode::~OnlineCalibrationNode()
{
  // Do nothing
}

bool OnlineCalibrationNode::initialize()
{
  if (not waitForMessages())
    return false;

  std::vector<Checkerboard::ConstPtr> cb_vec;
  for (unsigned int i = 0; i < checkerboard_array_msg_->checkerboards.size(); ++i)
    cb_vec.push_back(createCheckerboard(checkerboard_array_msg_->checkerboards[i], i));

  CameraModel::ConstPtr camera_model;
  PoseEstimator::ConstPtr pose_estimator;

  if (camera_type_ == ColorSensor::OMNIDIRECTIONAL)
  {
    OmnicameraModel::ConstPtr omnicamera_model(new OmnicameraModel(*omnicamera_info_msg_));
    ROS_INFO_STREAM("OMNICAMERA:\n " << *omnicamera_info_msg_);
    pose_estimator = PoseEstimator::ConstPtr(new OmnicameraPoseEstimator(omnicamera_model));
    camera_model = omnicamera_model;
  }
  else
  {
    PinholeCameraModel::ConstPtr pinhole_model(new PinholeCameraModel(*camera_info_msg_));
    ROS_INFO_STREAM("PINHOLE:\n" << *camera_info_msg_);
    pose_estimator = PoseEstimator::ConstPtr(new PinholePoseEstimator(pinhole_model));
    camera_model = pinhole_model;
  }

//  DistortionMap<double, pcl::PointXYZ>::Ptr distortion_map =
//    boost::make_shared<NoDistortionMap<double, pcl::PointXYZ> >();
//
//  if (laser_type_ == DepthSensor<pcl::PointXYZ>::KINECT and estimate_depth_distortion_)
//  {
  KinectMultifitDistortionMap<double, pcl::PointXYZ, 3>::Ptr k_distortion_map = boost::make_shared<
    KinectMultifitDistortionMap<double, pcl::PointXYZ, 3> >(distortion_rows_, distortion_cols_);
//    distortion_map = k_distortion_map;
//  }

  world_ = boost::make_shared<BaseObject>("/world");
  depth_sensor_ = boost::make_shared<DepthSensor<pcl::PointXYZ> >(laser_type_, k_distortion_map, "/depth_sensor",
                                                                  world_);
  color_sensor_ = boost::make_shared<ColorSensor>(camera_type_, camera_model, pose_estimator, "/color_sensor",
                                                  depth_sensor_);

  calibration_ = boost::make_shared<RGBDCalibration<pcl::PointXYZ> >(color_sensor_, depth_sensor_, cb_vec,
                                                                     k_distortion_map, max_threads_);
  calibration_->setPublisher(rgbd_publisher_);

//  if (find_depth_intrinsics_)
//    depth_calibration_.reset(new DepthCalibration<pcl::PointXYZ>());

  // TODO change!!!!
  if (camera_type_ == ColorSensor::OMNIDIRECTIONAL)
    calibration_->force(true);
  else
    calibration_->setInitialTransform(Transform3d::Identity());
  // ---------------

  return true;
}

void OnlineCalibrationNode::pointCloudCallback(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr & msg)
{
  cloud_msg_ = pcl::PointCloud<pcl::PointXYZ>::ConstPtr(new pcl::PointCloud<pcl::PointXYZ>(*msg));
}

void OnlineCalibrationNode::imageCallback(const sensor_msgs::Image::ConstPtr & msg)
{
  image_msg_ = msg;
}

void OnlineCalibrationNode::cameraInfoCallback(const sensor_msgs::CameraInfo::ConstPtr & msg)
{
  camera_info_msg_ = msg;
}

void OnlineCalibrationNode::omnicameraInfoCallback(const omnicamera::OmnicameraInfo::ConstPtr & msg)
{
  omnicamera_info_msg_ = msg;
}

void OnlineCalibrationNode::actionCallback(const std_msgs::String::ConstPtr & msg)
{
  if (msg->data == "stop")
    calibration_->stop();
}

void OnlineCalibrationNode::checkerboardArrayCallback(const CheckerboardArray::ConstPtr & msg)
{
  checkerboard_array_msg_ = msg;
}

bool OnlineCalibrationNode::waitForMessages() const
{
  ros::Rate rate(1.0);
  ros::spinOnce();
  while (ros::ok() and (not checkerboard_array_msg_ or not image_msg_ or not cloud_msg_))
  {
    ROS_WARN("Not all messages received!");
    rate.sleep();
    ros::spinOnce();
  }
  return checkerboard_array_msg_;
}

void OnlineCalibrationNode::spin()
{
  ros::Rate rate(5.0);
  int n = 1;
  cv::Mat image(300, 300, CV_8UC3, cv::Scalar(0, 0, 0));
  while (ros::ok())
  {
    ros::spinOnce();

    if (n == 0)
    {
      try
      {
        cv_bridge::CvImage::Ptr image_ptr = cv_bridge::toCvCopy(image_msg_, sensor_msgs::image_encodings::BGR8);
        //      calibration_->addTestData(image_ptr->image, cloud_msg_);
        bool has_pattern = calibration_->addData(image_ptr->image, cloud_msg_);
        if (has_pattern)
          save(image_ptr->image, cloud_msg_);
      }
      catch (cv_bridge::Exception & ex)
      {
        ROS_ERROR("cv_bridge exception: %s", ex.what());
        return;
      }
    }
    n = (n + 1) % 2;
    cv::Mat image_clone = image.clone();
    std::stringstream ss;
    ss << n;
    cv::putText(image_clone, ss.str(), cv::Point(50, 250), CV_FONT_HERSHEY_COMPLEX, 10, cv::Scalar(255, 255, 255), 2);
    cv::imshow("TIMER", image_clone);
    cv::waitKey(10);
    rate.sleep();
  }
}

void OnlineCalibrationNode::save(const cv::Mat & image,
                                 const pcl::PointCloud<pcl::PointXYZ>::ConstPtr & cloud)
{
  pcl::PCDWriter pcd_writer;

  std::stringstream cloud_file_name;
  cloud_file_name << save_folder_ << "cloud_" << file_index_ << ".pcd";
  pcd_writer.writeBinary(cloud_file_name.str(), *cloud);

  std::stringstream image_file_name;
  image_file_name << save_folder_ << "image_" << file_index_ << ".png";
  cv::imwrite(image_file_name.str(), image);

  file_index_++;
}

Checkerboard::Ptr OnlineCalibrationNode::createCheckerboard(const CheckerboardMsg::ConstPtr & msg,
                                                            int id)
{
  return createCheckerboard(*msg, id);
}

Checkerboard::Ptr OnlineCalibrationNode::createCheckerboard(const CheckerboardMsg & msg,
                                                            int id)
{
  std::stringstream ss;
  ss << "/checkerboard_" << id;
  return Checkerboard::Ptr(new Checkerboard(msg.rows, msg.cols, msg.cell_width, msg.cell_height, ss.str()));
}

} /* namespace calibration */

using namespace calibration;

int main(int argc,
         char** argv)
{
  ros::init(argc, argv, "rgbd_online_calibration");
  ros::NodeHandle node_handle("~");

  try
  {
    OnlineCalibrationNode calib_node(node_handle);
    if (not calib_node.initialize())
      return 0;
    calib_node.spin();
  }
  catch (std::runtime_error & error)
  {
    ROS_FATAL("Calibration error: %s", error.what());
    return 1;
  }

  return 0;
}
