/*
 * rgb_checkerboard_pose_optimizer.cpp
 *
 *  Created on: Jun 26, 2013
 *      Author: Filippo Basso
 */

#include <stdexcept>
#include <rgbd_calibration/optimization/rgb_checkerboard_pose_optimizer.h>

namespace calibration
{
namespace optimization
{

ImageCheckerboardPoseOptimizer::ErrorFunctor::ErrorFunctor(const Checkerboard::Ptr & checkerboard,
                                                           const ImageReprojectionError::Ptr & error)
  : RGBDErrorFunctor(RGBDErrorFunctor::TRANSFORM_SIZE, checkerboard->size()), checkerboard_(checkerboard), error_(error)
{
  // Do nothing
}

ImageCheckerboardPoseOptimizer::ErrorFunctor::~ErrorFunctor()
{
  // Do nothing
}

Eigen::VectorXd ImageCheckerboardPoseOptimizer::ErrorFunctor::initialize()
{
  Eigen::VectorXd x(inputs());
  setTransform(x, 0, Transform3d::Identity());
  return x;
}

void ImageCheckerboardPoseOptimizer::ErrorFunctor::convertResult(const InputType & x)
{
  checkerboard_->transform(getTransform(x, 0));
}

int ImageCheckerboardPoseOptimizer::ErrorFunctor::operator ()(InputType & x,
                                                             ValueType & v) const
{
  Checkerboard checkerboard(*checkerboard_, getTransform(x, 0));
  error_->compute(checkerboard.corners(), v);
  return 0;
}

ImageCheckerboardPoseOptimizer::ImageCheckerboardPoseOptimizer(const ColorCameraModel::ConstPtr & camera_model,
                                                               const PointSet2d & image_corners,
                                                               const Checkerboard::Ptr & checkerboard)
  : checkerboard_(checkerboard), error_(new ImageReprojectionError(camera_model, image_corners))
{
  // Do nothing
}

double ImageCheckerboardPoseOptimizer::error() const
{
  ErrorFunctor error_functor(checkerboard_, error_);
  Eigen::VectorXd x = error_functor.initialize();
  Eigen::VectorXd v(error_functor.values());
  error_functor(x, v);
  return v.sum();
}

bool ImageCheckerboardPoseOptimizer::optimize() const
{
  ErrorFunctor error_functor(checkerboard_, error_);
  Eigen::VectorXd x = error_functor.initialize();

  Eigen::DenseIndex index;

  int tries = 3;
  int info;
  do
  {
    info = Eigen::LevenbergMarquardt<ErrorFunctor>::lmdif1(error_functor, x, &index);
  }
  while (info == 5 and --tries > 0);

  if (info == 0)
    throw std::runtime_error("[ImageCheckerboardPoseOptimizer] Improper input parameters.");

  bool optimized = (info >= 1 and info <= 5);

  if (optimized)
    error_functor.convertResult(x);

  return optimized;
}

} /* namespace optimization */
} /* namespace calibration */
